#pragma once

#include <math.h>
#include <kame/base.h>

struct alignas(16) Vector2
{
    float X, Y;
    inline       float & operator[](size_t n)       { return ((float*)this)[n]; }
    inline const float & operator[](size_t n) const { return ((float*)this)[n]; }
};
struct alignas(16) Vector3
{
    float X, Y, Z;
    inline       float & operator[](size_t n)       { return ((float*)this)[n]; }
    inline const float & operator[](size_t n) const { return ((float*)this)[n]; }
};
struct alignas(16) Vector4
{
    float X, Y, Z, W;
    inline       float & operator[](size_t n)       { return ((float*)this)[n]; }
    inline const float & operator[](size_t n) const { return ((float*)this)[n]; }
};

#pragma pack(push, 1)
struct PVector2
{
    float X, Y;
    inline       float & operator[](size_t n)       { return ((float*)this)[n]; }
    inline const float & operator[](size_t n) const { return ((float*)this)[n]; }
};
struct PVector3
{
    float X, Y, Z;
    inline       float & operator[](size_t n)       { return ((float*)this)[n]; }
    inline const float & operator[](size_t n) const { return ((float*)this)[n]; }
};
struct PVector4
{
    float X, Y, Z, W;
    inline       float & operator[](size_t n)       { return ((float*)this)[n]; }
    inline const float & operator[](size_t n) const { return ((float*)this)[n]; }
};
#pragma pack(pop)

struct alignas(16) Matrix
{
    Vector4 Rows[4];
    inline       Vector4 & operator[](size_t n)       { return Rows[n]; }
    inline const Vector4 & operator[](size_t n) const { return Rows[n]; }
};

struct Rectangle2
{
    float Width, Height;
    float X, Y;
};

template <class T> inline T Min(T a, T b) { return a < b ? a : b; }
template <class T> inline T Max(T a, T b) { return a > b ? a : b; }

#define Clamp(v, min, max) ((v) < (min) ? (min) : ((v) > (max) ? (max) : (v)))

void IdentityMatrix(Out Matrix *m);
void RotateMatrix(Vector3 r, Out Matrix *m);
void TranslateMatrix(Vector3 p, Out Matrix *m);
void ScaleMatrix(Vector3 s, Out Matrix *m);
void ModelMatrix(Vector3 position, Vector3 scale, Vector3 rotation, Out Matrix *m);
void LookAtMatrix(Vector3 eye, Vector3 center, Vector3 up, Out Matrix *m);
void PerspectiveFovMatrix(float fovy, float aspect, float zNear, float zFar, Out Matrix *m);

Vector4 Transform(Vector4 vec, const Matrix *transform);
void Inverse(const Matrix *v, Out Matrix *m);

int IntersectBounds(Vector3 aMin, Vector3 aMax, Vector3 bMin, Vector3 bMax);
int IntersectSegmentBounds(Vector3 segmentA, Vector3 segmentB, Vector3 boundsMin, Vector3 boundsMax);

Matrix   operator* (const Matrix &lhs, const Matrix &rhs);
Matrix & operator*=(      Matrix &lhs, const Matrix &rhs);

inline float Lerp(float a, float b, float t)
{
    return (1.f - t) * a + t * b;
}

#define SCALAR_TYPE float
#define VECTOR_TYPE Vector2
#define VECTOR_SIZE 2
#include "math_vector.tpl.inl"

#define SCALAR_TYPE float
#define VECTOR_TYPE Vector3
#define VECTOR_SIZE 3
#include "math_vector.tpl.inl"

#define SCALAR_TYPE float
#define VECTOR_TYPE Vector4
#define VECTOR_SIZE 4
#include "math_vector.tpl.inl"

#define SCALAR_TYPE float
#define VECTOR_TYPE PVector2
#define VECTOR_SIZE 2
#include "math_vector.tpl.inl"

#define SCALAR_TYPE float
#define VECTOR_TYPE PVector3
#define VECTOR_SIZE 3
#include "math_vector.tpl.inl"

#define SCALAR_TYPE float
#define VECTOR_TYPE PVector4
#define VECTOR_SIZE 4
#include "math_vector.tpl.inl"