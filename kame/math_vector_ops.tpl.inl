#ifndef OP
#error Missing required definition: OP
#endif

#ifndef VECTOR_TYPE
#error Missing required definition: VECTOR_TYPE
#endif

#ifndef SCALAR_TYPE
#error Missing required definition: SCALAR_TYPE
#endif

#ifndef VECTOR_SIZE
#error Missing required definition: VECTOR_SIZE
#endif

inline VECTOR_TYPE operator OP(VECTOR_TYPE lhs, VECTOR_TYPE rhs)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        lhs[x] = lhs[x] OP rhs[x];
    return lhs;
}

inline VECTOR_TYPE & operator PPCat(OP,=)(VECTOR_TYPE &lhs, VECTOR_TYPE rhs)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        lhs[x] PPCat(OP,=) rhs[x];
    return lhs;
}

inline VECTOR_TYPE operator OP(VECTOR_TYPE lhs, SCALAR_TYPE rhs)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        lhs[x] = lhs[x] OP rhs;
    return lhs;
}

inline VECTOR_TYPE operator OP(SCALAR_TYPE lhs, VECTOR_TYPE rhs)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        rhs[x] = lhs OP rhs[x];
    return rhs;
}

inline VECTOR_TYPE & operator PPCat(OP,=)(VECTOR_TYPE &lhs, SCALAR_TYPE rhs)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        lhs[x] PPCat(OP,=) rhs;
    return lhs;
}

#undef OP