#pragma once

#include <xcb/xcb.h>
#include <kame/engine/platform.h>
#include <kame/engine/keysyms.h>

#define XcbChecked(expr) \
    do { if (xcb_request_check(window->Conn, expr)) Log(LogFatal, "x11", #expr); } while(0)

struct X11KeyInfo
{
    xcb_keysym_t  X11Keysym;
    KeySym        Keysym;
    const char   *Name;
};

struct X11Window
{
    xcb_connection_t *Conn;

    xcb_window_t      Window;
    xcb_screen_t     *Screen;
    xcb_visualid_t    VisualId;

    xcb_atom_t
        WmName,
        WmClass,
        WmProtocols,
        WmDeleteWindow,
        WmState,
        WmStateAbove;

    Handle        KeyboardMappingMemory;
    int           KeycodeCount, KeysymCount;
    int           KeysymCountPerKeycode;
    id_t          KeycodeMin;
    xcb_keysym_t *Keysyms;

    int           Mapped;
};