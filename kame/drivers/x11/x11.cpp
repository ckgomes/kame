#include <string.h>
#include <kame/engine/keysyms.h>
#include <kame/drivers/x11/x11.h>

static xcb_atom_t internAtom(X11Window *window, const char *name)
{
    return xcb_intern_atom_reply(
        window->Conn,
        xcb_intern_atom(window->Conn, 0, strlen(name), name),
        NULL)->atom;
}

static xcb_atom_t setWindowString(X11Window *window, const char *name, const char *str)
{
    xcb_atom_t atom;
    xcb_change_property(
        window->Conn,
        XCB_PROP_MODE_REPLACE,
        window->Window,
        atom = internAtom(window, name),
        XCB_ATOM_STRING,
        8,
        strlen(str),
        str);
    return atom;
}

static xcb_atom_t setWindowAtom(X11Window *window, xcb_atom_t key, const char *name)
{
    xcb_atom_t atom = internAtom(window, name);
    xcb_change_property(
        window->Conn,
        XCB_PROP_MODE_REPLACE,
        window->Window,
        key,
        XCB_ATOM_ATOM,
        32,
        1,
        &atom);
    return atom;
}

static const X11KeyInfo * getKeysymInfo(xcb_keysym_t keysym)
{
    // NOTE: this array has to be sorted (by keysym ascending)
    static const X11KeyInfo keyInfos[] =
    {
        { 32,    KeySymSpace,     "SPACE"     },
        { 33,    KeySym1,         "1"         },
        { 35,    KeySym3,         "3"         },
        { 36,    KeySym4,         "4"         },
        { 49,    KeySym1,         "1"         },
        { 50,    KeySym2,         "2"         },
        { 51,    KeySym3,         "3"         },
        { 52,    KeySym4,         "4"         },
        { 64,    KeySym2,         "2"         },
        { 65,    KeySymA,         "A"         },
        { 67,    KeySymC,         "C"         },
        { 68,    KeySymD,         "D"         },
        { 69,    KeySymE,         "E"         },
        { 80,    KeySymP,         "P"         },
        { 81,    KeySymQ,         "Q"         },
        { 82,    KeySymR,         "R"         },
        { 83,    KeySymS,         "S"         },
        { 87,    KeySymW,         "W"         },
        { 97,    KeySymA,         "A"         },
        { 99,    KeySymC,         "C"         },
        { 100,   KeySymD,         "D"         },
        { 101,   KeySymE,         "E"         },
        { 112,   KeySymP,         "P"         },
        { 113,   KeySymQ,         "Q"         },
        { 114,   KeySymR,         "R"         },
        { 115,   KeySymS,         "S"         },
        { 119,   KeySymW,         "W"         },
        { 161,   KeySym1,         "1"         },
        { 162,   KeySymC,         "C"         },
        { 163,   KeySym4,         "4"         },
        { 164,   KeySym4,         "4"         },
        { 167,   KeySymS,         "S"         },
        { 169,   KeySymC,         "C"         },
        { 174,   KeySymR,         "R"         },
        { 178,   KeySym2,         "2"         },
        { 179,   KeySym3,         "3"         },
        { 185,   KeySym1,         "1"         },
        { 193,   KeySymA,         "A"         },
        { 196,   KeySymQ,         "Q"         },
        { 197,   KeySymW,         "W"         },
        { 201,   KeySymE,         "E"         },
        { 208,   KeySymD,         "D"         },
        { 214,   KeySymP,         "P"         },
        { 223,   KeySymS,         "S"         },
        { 225,   KeySymA,         "A"         },
        { 228,   KeySymQ,         "Q"         },
        { 229,   KeySymW,         "W"         },
        { 233,   KeySymE,         "E"         },
        { 240,   KeySymD,         "D"         },
        { 246,   KeySymP,         "P"         },
        { 65027, KeySymRAlt,      "RALT"      },
        { 65056, KeySymTab,       "TAB"       },
        { 65288, KeySymBackspace, "BACKSPACE" },
        { 65289, KeySymTab,       "TAB"       },
        { 65293, KeySymReturn,    "RETURN"    },
        { 65307, KeySymEscape,    "ESCAPE"    },
        { 65361, KeySymLeft,      "LEFT"      },
        { 65362, KeySymUp,        "UP"        },
        { 65363, KeySymRight,     "RIGHT"     },
        { 65364, KeySymDown,      "DOWN"      },
        { 65470, KeySymF1,        "F1"        },
        { 65471, KeySymF2,        "F2"        },
        { 65472, KeySymF3,        "F3"        },
        { 65473, KeySymF4,        "F4"        },
        { 65474, KeySymF5,        "F5"        },
        { 65475, KeySymF6,        "F6"        },
        { 65476, KeySymF7,        "F7"        },
        { 65477, KeySymF8,        "F8"        },
        { 65478, KeySymF9,        "F9"        },
        { 65479, KeySymF10,       "F10"       },
        { 65480, KeySymF11,       "F11"       },
        { 65481, KeySymF12,       "F12"       },
        { 65505, KeySymLShift,    "LSHIFT"    },
        { 65506, KeySymRShift,    "RSHIFT"    },
        { 65507, KeySymLCtrl,     "LCTRL"     },
        { 65508, KeySymRCtrl,     "RCTRL"     },
        { 65511, KeySymLAlt,      "LALT"      },
        { 65512, KeySymRAlt,      "RALT"      },
        { 65513, KeySymLAlt,      "LALT"      },
        { 65514, KeySymRAlt,      "RALT"      },
    };

    int32_t index;
    int32_t minBound = 0;
    int32_t maxBound = ArraySize(keyInfos) - 1;

    while (1)
    {
        index = (maxBound + minBound) / 2;

        if (keyInfos[index].X11Keysym > keysym)
            maxBound = index - 1;
        else if (keyInfos[index].X11Keysym < keysym)
            minBound = index + 1;
        else break;

        if (minBound > maxBound)
            return nullptr;
    }

    return keyInfos + index;
}

static int loadKeyboardMapping(X11Window *window)
{
    const xcb_setup_t *setup = xcb_get_setup(window->Conn);

    xcb_get_keyboard_mapping_reply_t* keyboardMapping = xcb_get_keyboard_mapping_reply(
        window->Conn,
        xcb_get_keyboard_mapping(
            window->Conn,
            setup->min_keycode,
            setup->max_keycode - setup->min_keycode + 1),
        nullptr);
    window->KeyboardMappingMemory = keyboardMapping;
    window->KeycodeCount          = (int) (keyboardMapping->length / keyboardMapping->keysyms_per_keycode);
    window->KeysymCount           = (int) keyboardMapping->length;
    window->KeysymCountPerKeycode = (int) keyboardMapping->keysyms_per_keycode;
    window->KeycodeMin            = (int) setup->min_keycode;
    window->Keysyms               = xcb_get_keyboard_mapping_keysyms(keyboardMapping);

    return ErrNone;
}

static int processKeyEvent(
    Platform                    *p,
    X11Window                   *window,
    const xcb_key_press_event_t *e,
    int                          state)
{
    for (int i = 0; i < window->KeysymCountPerKeycode; ++i)
    {
        xcb_keysym_t keysym = window->Keysyms[i + (e->detail - window->KeycodeMin) * window->KeysymCountPerKeycode];
        if (keysym == 0)
            break;

        const X11KeyInfo *keyInfo = getKeysymInfo(keysym);

        if (keyInfo && keyInfo->Keysym < ArraySize(p->KeyboardState))
            p->KeyboardState[keyInfo->Keysym] = state;
        else if (!keyInfo)
            Log(LogWarn, "x11", "keycode %u > keysym %u has no mapping", (uint32_t) e->detail, (uint32_t) keysym);
    }

    return ErrNotImplemented;
}

static int load(Platform *p)
{
    Log(LogInfo, "x11", "loading");

    X11Window *window = (X11Window *) p->WindowDriver.Data;

    window->Conn = xcb_connect(nullptr, nullptr);
    CheckedFatal(
        xcb_connection_has_error(window->Conn),
        "x11",
        "xcb_connect: error state");

    Checked(loadKeyboardMapping(window));

    window->Screen = xcb_setup_roots_iterator(xcb_get_setup(window->Conn)).data;
    window->VisualId = window->Screen->root_visual;

    xcb_get_geometry_reply_t *geometry = xcb_get_geometry_reply(
        window->Conn,
        xcb_get_geometry(window->Conn, window->Screen->root),
        nullptr);

    const uint32_t eventMask =
        XCB_EVENT_MASK_VISIBILITY_CHANGE |
        XCB_EVENT_MASK_KEY_RELEASE |
        XCB_EVENT_MASK_KEY_PRESS |
        XCB_EVENT_MASK_STRUCTURE_NOTIFY |
        XCB_EVENT_MASK_POINTER_MOTION |
        XCB_EVENT_MASK_BUTTON_PRESS |
        XCB_EVENT_MASK_BUTTON_RELEASE;

    const uint32_t windowValues[] = { window->Screen->black_pixel, eventMask };
    const uint16_t w = 800, h = 800;

    window->Window = xcb_generate_id(window->Conn);
    XcbChecked(xcb_create_window_checked(
        window->Conn,
        XCB_COPY_FROM_PARENT,
        window->Window,
        window->Screen->root,
        (geometry->width - w) / 2, (geometry->height - h) / 2,
        w, h,
        0,
        XCB_WINDOW_CLASS_INPUT_OUTPUT,
        window->VisualId,
        XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK,
        windowValues));

    window->WmProtocols    = internAtom(window, "WM_PROTOCOLS");
    window->WmState        = internAtom(window, "_NET_WM_STATE");
    window->WmName         = setWindowString(window, "WM_NAME", "");
    window->WmClass        = setWindowString(window, "WM_CLASS", "KAMI_ENGINE");
    window->WmDeleteWindow = setWindowAtom(window, window->WmProtocols, "WM_DELETE_WINDOW");
    window->WmStateAbove   = setWindowAtom(window, window->WmState, "_NET_WM_STATE_ABOVE");

    return ErrNone;
}

static int loadRenderer(Platform *p)
{
    LibInit initVulkan;
    Handle vulkanHandle = nullptr;
    Checked(OpenLibrary("kame_vulkan", &vulkanHandle));
    Checked(LoadLibrarySymbol("Init", vulkanHandle, (Handle *) &initVulkan));
    Checked(initVulkan(p));
    return p->RendererDriver.Load(p);
}

static int setTitle(Platform *p, const char *title)
{
    X11Window *window = (X11Window *) p->WindowDriver.Data;
    XcbChecked(xcb_change_property_checked(
        window->Conn,
        XCB_PROP_MODE_REPLACE,
        window->Window,
        window->WmName,
        XCB_ATOM_STRING,
        8,
        strlen(title),
        title));
    return ErrNone;
}

static int present(Platform *p)
{
    if (strcmp("vulkan", p->RendererDriver.Name) == 0)
        return ErrNone;

    return ErrNotImplemented;
}

static int processEvents(Platform *p)
{
    X11Window *window = (X11Window *) p->WindowDriver.Data;

    if (!window->Mapped)
    {
        XcbChecked(xcb_map_window_checked(window->Conn, window->Window));
        window->Mapped = 1;
    }

    xcb_generic_event_t *ge;
    while ((ge = xcb_poll_for_event(window->Conn)))
    {
        uint8_t eventType = ge->response_type & 0x7f;
        switch (eventType)
        {
            case XCB_CLIENT_MESSAGE:
            {
                xcb_client_message_event_t *e = (xcb_client_message_event_t *) ge;
                if (e->format == 32)
                {
                    xcb_atom_t atom = (xcb_atom_t) e->data.data32[0];
                    if (atom == window->WmDeleteWindow)
                        p->Running = false;
                }
            } break;

            case XCB_KEY_RELEASE:
            {
                processKeyEvent(p, window, (xcb_key_press_event_t *) ge, 0);
            } break;

            case XCB_KEY_PRESS:
            {
                processKeyEvent(p, window, (xcb_key_press_event_t *) ge, 1);
            } break;
        }
    }

    return ErrNone;
}

KameAPI int Init(Platform *p)
{
    Log(LogInfo, "x11", "init");

    p->WindowDriver.Load          = load;
    p->WindowDriver.LoadRenderer  = loadRenderer;
    p->WindowDriver.SetTitle      = setTitle;
    p->WindowDriver.Present       = present;
    p->WindowDriver.ProcessEvents = processEvents;

    p->WindowDriver.Data = NewObjectZ(X11Window);
    p->WindowDriver.Name = "X11";

    return ErrNone;
}