set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}")

add_subdirectory(x11)
add_subdirectory(vulkan)