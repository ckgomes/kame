#pragma once

#include <vulkan/vulkan.h>
#include <kame/math.h>
#include <kame/engine/platform.h>
#include <kame/engine/material.h>

#define VkChecked(expr) do {\
    VkResult _r = expr;\
    if (_r != VK_SUCCESS)\
        Log(LogFatal, "vulkan", "result=%s %s", stringifyResult(_r), #expr);\
} while(0)

struct VulkanVertex
{
    PVector3 Position;
    PVector3 Normal;
    PVector2 TextureCoordinate;
    PVector3 Color;
};
static const VkVertexInputAttributeDescription VulkanVertexAttributes[] =
{
    { 0, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(VulkanVertex, Position          ) },
    { 1, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(VulkanVertex, Normal            ) },
    { 2, 0, VK_FORMAT_R32G32_SFLOAT   , offsetof(VulkanVertex, TextureCoordinate ) },
    { 3, 0, VK_FORMAT_R32G32B32_SFLOAT, offsetof(VulkanVertex, Color             ) },
};

struct VulkanRenderPassVertexUniformBlock
{
    Matrix ViewProjection;
    float  Near, Far;
};

struct VulkanMeshPart
{
    VkDeviceSize Offset, Count;
};

struct VulkanVertexBuffer
{
    uint32_t        VertexCount, VertexCapacity;
    VkBuffer        VertexBuffer;
    VkDeviceMemory  VertexBufferMemory;
    VkDeviceSize    VertexBufferLength;
};

struct VulkanMesh : VulkanVertexBuffer
{
    VkIndexType     IndexType;
    uint32_t        IndexCount, IndexCapacity;
    VkBuffer        IndexBuffer;
    VkDeviceMemory  IndexMemory;
    VkDeviceSize    IndexMemoryLength;

    VkDeviceSize    PartCount;
    VulkanMeshPart *Parts;

    Mesh           *BaseMesh;
};

struct VulkanTextureAtlasNode : VkRect2D
{
    VulkanTextureAtlasNode *Children[2];
};

struct VulkanTextureAtlas : VulkanTextureAtlasNode
{
    VkImage         Image;
    VkImageView     ImageView;
    VkDeviceMemory  ImageMemory;
    VkFormat        Format;
    VkSampler       Sampler;
    VkImageLayout   CurrentLayout;
    VkBuffer        StageBuffer;
    VkDeviceMemory  StageBufferMemory;
    VkDeviceSize    StageBufferLength;
    VkCommandBuffer CommandBuffer;
    VkFence         CommandBufferFence;
};

struct VulkanTexture
{
    VulkanTextureAtlas *Atlas;
    Texture            *BaseTexture;
    VkRect2D            Region;

    struct { PVector2 Extent, Offset; } TextureCoordinate;
};

struct VulkanRenderTexture
{
    VkExtent2D      Extent;
    VkFramebuffer   Framebuffer;
    VkImage         ColorImage, DepthStencilImage;
    VkImageView     ColorImageView, DepthStencilImageView;
    VkDeviceMemory  ColorMemory, DepthStencilMemory;
    uint64_t        BarrierFrameNumber;
    RenderTexture  *BaseRenderTexture;
};

struct VulkanMaterial
{
    VkPipeline      Pipeline;
    VkDescriptorSet DescriptorSet;
    uint32_t        DataLength;
};

struct VulkanCommandBufferState
{
    VkPipeline  ActivePipeline;
    VkBuffer    ActiveIndexBuffer;
    VkIndexType ActiveIndexType;
    VkBuffer    ActiveVertexBuffers[8];
};

struct VulkanDestroyBuffer
{
    VkBuffer       Buffer;
    VkDeviceMemory Memory;
};

struct VulkanUniformBuffer
{
    VkBuffer        Buffer;
    VkDeviceSize    Length;
    VkDeviceMemory  Memory;
    uint8_t        *Data;
    uint32_t        Position;
};

struct VulkanRenderer
{
    VkInstance                Instance;
    VkDevice                  Device;

    VkQueue                   GraphicsQueue, PresentQueue;
    VkCommandPool             CommandPool;
    VkDescriptorPool          DescriptorPool;
    VkDescriptorSetLayout     DescriptorSetLayout;

    VkSurfaceKHR              Surface;
    VkSurfaceFormatKHR        SurfaceFormat;
    VkSurfaceCapabilitiesKHR  SurfaceCapabilities;
    VkSwapchainKHR            Swapchain;
    uint32_t                  SwapchainImageCount;
    VkImage                   SwapchainImages[3];

    VkPipelineLayout          PipelineLayout;

    VulkanUniformBuffer       UniformBuffers[4];

    uint32_t                  DrawCallCapacity, DrawCallCount;
    uint32_t                  RenderPassCapacity, RenderPassCurrentCount;

    VulkanTextureAtlas        Atlas[8];
    VkSampler                 SamplerNearest, SamplerLinear;

    VkRenderPass              RenderTexturePass;

    RenderTexture             RenderTexture;

    int UpdateSwapchain;
    uint64_t FrameNumber;

    VulkanVertexBuffer  GeometryVertexBuffer;
    void               *GeometryVertexBufferMappedMemory;
    VkDeviceSize        GeometryVertexBufferCapacity;

    VulkanTexture      *GridTexture;
    Material           *MissingTextureMaterial;

    Handle PipelineResolver;

    VulkanDestroyBuffer *DestroyBuffers;
    uint32_t             DestroyBufferCount, DestroyBufferCapacity;

    uint32_t                         GraphicsQueueFamily, PresentQueueFamily;
    VkPhysicalDevice                 PhysicalDevice;
    VkPhysicalDeviceProperties       PhysicalDeviceProperties;
    VkPhysicalDeviceMemoryProperties PhysicalDeviceMemoryProperties;

    MeshNode   *MeshNodeBuffer[8 * 1024];
    CameraNode *CameraNodeBuffer[128];

    VkLayerProperties      *Layers;
    uint32_t                InstanceExtensionCount,          DeviceExtensionCount, LayerCount;
    VkExtensionProperties  *InstanceExtensions,             *DeviceExtensions;
    uint32_t                EnabledInstanceExtensionCount,   EnabledDeviceExtensionCount, EnabledLayerCount;
    const char            **EnabledInstancedExtensions,    **EnabledDeviceExtensions, **EnabledLayers;
};

const char *stringifyResult(VkResult result);

VkResult addInstanceExtension(VulkanRenderer *renderer, const char *ext);
VkResult addDeviceExtension(VulkanRenderer *renderer, const char *ext);

VkResult addInstanceExtensionsXcb(VulkanRenderer *renderer);
VkResult createSurfaceXcb(Platform *p, VulkanRenderer *renderer);
int isQueueFamilyPresentableXcb(Platform *p, VulkanRenderer *renderer, uint32_t queueFamIndex, int *presentable);

int createGraphicsPipeline(
    VulkanRenderer                          *r,
    VkRenderPass                             renderPass,
    VkShaderModule                           vertexShader,
    VkShaderModule                           fragmentShader,
    MeshTopology                             meshTopology,
    CullMode                                 cullMode,
    DrawMode                                 drawMode,
    BlendMode                                blendMode,
    VkBool32                                 enableDepthBuffering,
    uint32_t                                 vertexStride,
    uint32_t                                 attributeCount,
    const VkVertexInputAttributeDescription *attributes,
    Out VkPipeline                          *pipeline);

int getMaterialPipeline(
    VulkanRenderer                          *r,
    Material                                *material,
    VkRenderPass                             renderPass,
    uint32_t                                 vertexStride,
    uint32_t                                 attributeCount,
    const VkVertexInputAttributeDescription *attributes,
    VkPipeline                              *pipeline);