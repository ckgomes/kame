#include <kame/drivers/vulkan/vk.h>

static const VkPrimitiveTopology meshTopologyMap[] =
{
    VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, // MeshTopologyTriangles
    VK_PRIMITIVE_TOPOLOGY_LINE_LIST,     // MeshTopologyLines
};

static const VkCullModeFlags cullModeMap[] =
{
    VK_CULL_MODE_NONE,      // CullModeNever
    VK_CULL_MODE_BACK_BIT,  // CullModeBack
    VK_CULL_MODE_FRONT_BIT, // CullModeFront
};

static const VkPolygonMode drawModeMap[] =
{
    VK_POLYGON_MODE_FILL, // DrawModeFill
    VK_POLYGON_MODE_LINE, // DrawModeWireframe
};

int createGraphicsPipeline(
    VulkanRenderer                          *r,
    VkRenderPass                             renderPass,
    VkShaderModule                           vertexShader,
    VkShaderModule                           fragmentShader,
    MeshTopology                             meshTopology,
    CullMode                                 cullMode,
    DrawMode                                 drawMode,
    BlendMode                                blendMode,
    VkBool32                                 enableDepthBuffering,
    uint32_t                                 vertexStride,
    uint32_t                                 attributeCount,
    const VkVertexInputAttributeDescription *attributes,
    VkPipeline                              *pipeline)
{
    Assert(
        meshTopology < ArraySize(meshTopologyMap) &&
        cullMode < ArraySize(cullModeMap) &&
        drawMode < ArraySize(drawModeMap));

    if (blendMode != BlendModeNone)
        return ErrNotImplemented;

    VkVertexInputBindingDescription bindings = {};
    bindings.stride    = vertexStride;
    bindings.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    VkPipelineVertexInputStateCreateInfo pvisci = {};
    pvisci.sType                           = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    pvisci.vertexBindingDescriptionCount   = 1;
    pvisci.pVertexBindingDescriptions      = &bindings;
    pvisci.vertexAttributeDescriptionCount = attributeCount;
    pvisci.pVertexAttributeDescriptions    = attributes;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    VkPipelineInputAssemblyStateCreateInfo piasci = {};
    piasci.sType                  = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    piasci.primitiveRestartEnable = VK_FALSE;
    piasci.topology               = meshTopologyMap[meshTopology];

    VkPipelineRasterizationStateCreateInfo prsci = {};
    prsci.sType       = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    prsci.frontFace   = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    prsci.cullMode    = cullModeMap[cullMode];
    prsci.polygonMode = drawModeMap[drawMode];
    prsci.lineWidth   = 1.0f;

    VkPipelineColorBlendAttachmentState blendState = {};
    blendState.colorWriteMask =
        VK_COLOR_COMPONENT_R_BIT |
        VK_COLOR_COMPONENT_G_BIT |
        VK_COLOR_COMPONENT_B_BIT |
        VK_COLOR_COMPONENT_A_BIT;

    VkPipelineColorBlendStateCreateInfo pcbsci = {};
    pcbsci.sType           = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    pcbsci.attachmentCount = 1;
    pcbsci.pAttachments    = &blendState;
    pcbsci.logicOpEnable   = VK_FALSE;
    pcbsci.logicOp         = VK_LOGIC_OP_NO_OP;
    pcbsci.blendConstants[0] = pcbsci.blendConstants[1] = pcbsci.blendConstants[2] = pcbsci.blendConstants[3] = 1.f;

    // NOTE: Vulkan requires these parameters to be present here
    // However, they're not actually used now, as they will be set by the dynamic state
    VkRect2D scissor = {};
    VkViewport viewport = {};

    VkPipelineViewportStateCreateInfo pvsci = {};
    pvsci.sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    pvsci.viewportCount = 1;
    pvsci.scissorCount  = 1;
    pvsci.pViewports    = &viewport;
    pvsci.pScissors     = &scissor;

    VkPipelineMultisampleStateCreateInfo pmsci = {};
    pmsci.sType                = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    pmsci.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    VkPipelineShaderStageCreateInfo pssci[2] = {};
    pssci[0].sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pssci[0].pName  = "main";
    pssci[0].stage  = VK_SHADER_STAGE_VERTEX_BIT;
    pssci[0].module = vertexShader;
    pssci[1].sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pssci[1].pName  = "main";
    pssci[1].stage  = VK_SHADER_STAGE_FRAGMENT_BIT;
    pssci[1].module = fragmentShader;

    VkDynamicState dynamicStates[] =
    {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR,
    };
    VkPipelineDynamicStateCreateInfo pdsci = {};
    pdsci.sType             = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    pdsci.dynamicStateCount = ArraySize(dynamicStates);
    pdsci.pDynamicStates    = dynamicStates;

    VkPipelineDepthStencilStateCreateInfo pdssci = {};
    pdssci.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    if (enableDepthBuffering)
    {
        pdssci.depthTestEnable  = VK_TRUE;
        pdssci.depthWriteEnable = VK_TRUE;
        pdssci.depthCompareOp   = VK_COMPARE_OP_LESS;
    }

    VkGraphicsPipelineCreateInfo gpci = {};
    gpci.sType               = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    gpci.layout              = r->PipelineLayout;
    gpci.pVertexInputState   = &pvisci;
    gpci.pInputAssemblyState = &piasci;
    gpci.pRasterizationState = &prsci;
    gpci.pColorBlendState    = &pcbsci;
    gpci.pMultisampleState   = &pmsci;
    gpci.pViewportState      = &pvsci;
    gpci.pDynamicState       = &pdsci;
    gpci.pDepthStencilState  = &pdssci;
    gpci.pStages             = pssci;
    gpci.stageCount          = 2;
    gpci.renderPass          = renderPass;
    gpci.subpass             = 0;

    VkChecked(vkCreateGraphicsPipelines(r->Device, nullptr, 1, &gpci, nullptr, pipeline));
    return ErrNone;
}