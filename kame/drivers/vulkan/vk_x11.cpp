#define VK_USE_PLATFORM_XCB_KHR
#include <kame/drivers/vulkan/vk.h>
#include <kame/drivers/x11/x11.h>

VkResult addInstanceExtensionsXcb(VulkanRenderer *renderer)
{
    return addInstanceExtension(renderer, VK_KHR_XCB_SURFACE_EXTENSION_NAME);
}

VkResult createSurfaceXcb(
    Platform   *p,
    VulkanRenderer *renderer)
{
    X11Window *window = (X11Window *) p->WindowDriver.Data;

    VkXcbSurfaceCreateInfoKHR ci = {};
    ci.sType      = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR;
    ci.connection = window->Conn;
    ci.window     = window->Window;

    VkChecked(vkCreateXcbSurfaceKHR(renderer->Instance, &ci, nullptr, &renderer->Surface));
    return VK_SUCCESS;
}

int isQueueFamilyPresentableXcb(
    Platform       *p,
    VulkanRenderer *renderer,
    uint32_t        queueFamIndex,
    int            *presentable)
{
    X11Window *window = (X11Window *) p->WindowDriver.Data;

    VkBool32 presentationSupported, surfaceSupported;

    presentationSupported = vkGetPhysicalDeviceXcbPresentationSupportKHR(
        renderer->PhysicalDevice,
        queueFamIndex,
        window->Conn,
        window->VisualId);

    VkChecked(vkGetPhysicalDeviceSurfaceSupportKHR(
        renderer->PhysicalDevice,
        queueFamIndex,
        renderer->Surface,
        &surfaceSupported));

    *presentable =
        presentationSupported == VK_TRUE &&
        surfaceSupported == VK_TRUE;

    return ErrNone;
}