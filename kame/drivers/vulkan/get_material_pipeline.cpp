#include <string.h>
#include <stdio.h>

#include <kame/drivers/vulkan/vk.h>

struct VulkanShaderModule
{
    char            Name[64];
    VkShaderModule  VertexShader;
    VkShaderModule  FragmentShader;
};

// shaders
// cull mode
// blend mode
// draw mode
// vertex topology

struct VulkanPipelineNode
{
    uint32_t   ReferenceCount;
    VkPipeline Pipeline;
};

struct VulkanMaterialParametersNode : VulkanPipelineNode
{
    MeshTopology MeshTopology;
    BlendMode    BlendMode;
    DrawMode     DrawMode;
    CullMode     CullMode;
};

struct VulkanShaderNode
{
    uint32_t ShaderIndex;
    uint32_t ChildCount, ChildCapacity;
    VulkanMaterialParametersNode *Children;
};

struct VulkanInputLayoutNode
{
    uint32_t                          VertexStride;
    uint32_t                          AtributeCount;
    VkVertexInputAttributeDescription Attributes[16];

    uint32_t ChildCount, ChildCapacity;
    VulkanShaderNode *Children;
};

struct VulkanRenderPassNode
{
    VkRenderPass RenderPass;

    uint32_t ChildCount, ChildCapacity;
    VulkanInputLayoutNode *Children;
};

struct VulkanPipelineResolver
{
    uint32_t ChildCount, ChildCapacity;
    VulkanRenderPassNode *Children;

    uint32_t ShaderCount, ShaderCapacity;
    VulkanShaderModule *Shaders;
};

static int compareInputLayout(
    uint32_t                                 lhsVertexStride,
    uint32_t                                 lhsAttributeCount,
    const VkVertexInputAttributeDescription *lhsAttributes,
    uint32_t                                 rhsVertexStride,
    uint32_t                                 rhsAttributeCount,
    const VkVertexInputAttributeDescription *rhsAttributes)
{
    if (lhsVertexStride != rhsVertexStride || lhsAttributeCount != rhsAttributeCount)
        return 0;

    for (uint32_t i = 0; i < lhsAttributeCount; ++i)
    {
        int equal =
            lhsAttributes[i].binding  != rhsAttributes[i].binding ||
            lhsAttributes[i].format   != rhsAttributes[i].format ||
            lhsAttributes[i].location != rhsAttributes[i].location ||
            lhsAttributes[i].offset   != rhsAttributes[i].offset;
        if (!equal)
            return 0;
    }

    return 1;
}

static int createShaderFromFile(VulkanRenderer *r, const char *name, const char *type, VkShaderModule *shaderModule)
{
    char path[256];
    snprintf(path, sizeof(path), "data/shaders/%s.%s.spirv", name, type);

    MemoryMappedFile spirv;
    Checked(OpenMemoryMappedFile(path, &spirv));

    VkShaderModuleCreateInfo smci = {};
    smci.sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    smci.codeSize = (size_t) spirv.End - (size_t) spirv.Start;
    smci.pCode    = (const uint32_t *) spirv.Start;

    VkResult result = vkCreateShaderModule(r->Device, &smci, nullptr, shaderModule);
    Checked(CloseMemoryMappedFile(&spirv));
    CheckedError(result != VK_SUCCESS, ErrDefault, "vulkan: CreateShaderModule: %s", stringifyResult(result));
    return ErrNone;
}

static int getShaderModuleIndex(
    VulkanRenderer         *r,
    VulkanPipelineResolver *pipelineResolver,
    const char             *name,
    uint32_t               *shaderIndex)
{
    for (uint32_t i = 0; i < pipelineResolver->ShaderCount; ++i)
    {
        if (strcmp(pipelineResolver->Shaders[i].Name, name) == 0)
        {
            *shaderIndex = i;
            return ErrNone;
        }
    }

    Log(LogInfo, "vulkan", "loading shader [%s]", name);

    VulkanShaderModule *module = ListPush(
        &pipelineResolver->Shaders,
        &pipelineResolver->ShaderCount,
        &pipelineResolver->ShaderCapacity);
    strncpy(module->Name, name, sizeof(module->Name));
    Checked(createShaderFromFile(r, name, "vertex", &module->VertexShader));
    Checked(createShaderFromFile(r, name, "fragment", &module->FragmentShader));

    *shaderIndex = module - pipelineResolver->Shaders;
    return ErrNone;
}

int getMaterialPipeline(
    VulkanRenderer                          *r,
    Material                                *material,
    VkRenderPass                             renderPass,
    uint32_t                                 vertexStride,
    uint32_t                                 attributeCount,
    const VkVertexInputAttributeDescription *attributes,
    VkPipeline                              *pipeline)
{
    VulkanPipelineResolver *materialResolver = ( VulkanPipelineResolver *) r->PipelineResolver;
    if (!materialResolver)
        r->PipelineResolver = materialResolver = NewObjectZ(VulkanPipelineResolver);

    VulkanRenderPassNode *renderPassNode = nullptr;
    for (uint32_t i = 0; i < materialResolver->ChildCount; ++i)
    {
        if (materialResolver->Children[i].RenderPass == renderPass)
        {
            renderPassNode = materialResolver->Children + i;
            break;
        }
    }
    if (!renderPassNode)
    {
        renderPassNode = ListPush(
            &materialResolver->Children,
            &materialResolver->ChildCount,
            &materialResolver->ChildCapacity);
        renderPassNode->RenderPass = renderPass;
    }

    VulkanInputLayoutNode *inputLayerNode = nullptr;
    for (uint32_t i = 0; i < renderPassNode->ChildCount; ++i)
    {
        int equal = compareInputLayout(
            vertexStride,
            attributeCount,
            attributes,
            renderPassNode->Children[i].VertexStride,
            renderPassNode->Children[i].AtributeCount,
            renderPassNode->Children[i].Attributes);
        if (equal)
        {
            inputLayerNode = renderPassNode->Children + i;
            break;
        }
    }
    if (!inputLayerNode)
    {
        inputLayerNode = ListPush(
            &renderPassNode->Children,
            &renderPassNode->ChildCount,
            &renderPassNode->ChildCapacity);
        inputLayerNode->VertexStride = vertexStride;
        inputLayerNode->AtributeCount = attributeCount;
        for (uint32_t i = 0; i < attributeCount; ++i)
            inputLayerNode->Attributes[i] = attributes[i];
    }

    VulkanShaderNode *shaderNode = nullptr;
    for (uint32_t i = 0; i < inputLayerNode->ChildCount; ++i)
    {
        VulkanShaderModule *shaderModule = materialResolver->Shaders + inputLayerNode->Children[i].ShaderIndex;
        if (strcmp(shaderModule->Name, material->Shader) == 0)
        {
            shaderNode = inputLayerNode->Children + i;
            break;
        }
    }
    if (!shaderNode)
    {
        Assert(strlen(material->Shader) < sizeof(VulkanShaderModule::Name));

        shaderNode = ListPush(
            &inputLayerNode->Children,
            &inputLayerNode->ChildCount,
            &inputLayerNode->ChildCapacity);
        Checked(getShaderModuleIndex(r, materialResolver, material->Shader, &shaderNode->ShaderIndex));
    }

    VulkanMaterialParametersNode *materialParametersNode = nullptr;
    for (uint32_t i = 0; i < shaderNode->ChildCount; ++i)
    {
        int equal =
            shaderNode->Children[i].CullMode     == material->CullMode &&
            shaderNode->Children[i].BlendMode    == material->BlendMode &&
            shaderNode->Children[i].DrawMode     == material->DrawMode &&
            shaderNode->Children[i].MeshTopology == material->MeshTopology;
        if (equal)
        {
            materialParametersNode = shaderNode->Children + i;
            break;
        }
    }
    if (!materialParametersNode)
    {
        materialParametersNode = ListPush(
            &shaderNode->Children,
            &shaderNode->ChildCount,
            &shaderNode->ChildCapacity);
        materialParametersNode->MeshTopology = material->MeshTopology;
        materialParametersNode->CullMode     = material->CullMode;
        materialParametersNode->BlendMode    = material->BlendMode;
        materialParametersNode->DrawMode     = material->DrawMode;

        VulkanShaderModule *shaderModule = materialResolver->Shaders + shaderNode->ShaderIndex;

        Checked(createGraphicsPipeline(
            r,
            renderPass,
            shaderModule->VertexShader,
            shaderModule->FragmentShader,
            material->MeshTopology,
            material->CullMode,
            material->DrawMode,
            material->BlendMode,
            1,
            sizeof(VulkanVertex),
            ArraySize(VulkanVertexAttributes),
            VulkanVertexAttributes,
            &materialParametersNode->Pipeline));
    }

    *pipeline = materialParametersNode->Pipeline;
    return ErrNone;
}