#include <string.h>
#include <stdio.h>
#include <kame/drivers/vulkan/vk.h>

const char *stringifyResult(VkResult result)
{
    #define Case(code) case VK_##code: return #code
    switch (result)
    {
        Case(SUCCESS);
        Case(NOT_READY);
        Case(TIMEOUT);
        Case(EVENT_SET);
        Case(EVENT_RESET);
        Case(INCOMPLETE);
        Case(ERROR_OUT_OF_HOST_MEMORY);
        Case(ERROR_OUT_OF_DEVICE_MEMORY);
        Case(ERROR_INITIALIZATION_FAILED);
        Case(ERROR_DEVICE_LOST);
        Case(ERROR_MEMORY_MAP_FAILED);
        Case(ERROR_LAYER_NOT_PRESENT);
        Case(ERROR_EXTENSION_NOT_PRESENT);
        Case(ERROR_FEATURE_NOT_PRESENT);
        Case(ERROR_INCOMPATIBLE_DRIVER);
        Case(ERROR_TOO_MANY_OBJECTS);
        Case(ERROR_FORMAT_NOT_SUPPORTED);
        Case(ERROR_FRAGMENTED_POOL);
        Case(ERROR_UNKNOWN);
        Case(ERROR_OUT_OF_POOL_MEMORY);
        Case(ERROR_INVALID_EXTERNAL_HANDLE);
        Case(ERROR_FRAGMENTATION);
        Case(ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS);
        Case(PIPELINE_COMPILE_REQUIRED);
        Case(ERROR_SURFACE_LOST_KHR);
        Case(ERROR_NATIVE_WINDOW_IN_USE_KHR);
        Case(SUBOPTIMAL_KHR);
        Case(ERROR_OUT_OF_DATE_KHR);
        Case(ERROR_INCOMPATIBLE_DISPLAY_KHR);
        Case(ERROR_VALIDATION_FAILED_EXT);
        Case(ERROR_INVALID_SHADER_NV);
        Case(ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT);
        Case(ERROR_NOT_PERMITTED_KHR);
        Case(ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT);
        Case(THREAD_IDLE_KHR);
        Case(THREAD_DONE_KHR);
        Case(OPERATION_DEFERRED_KHR);
        Case(OPERATION_NOT_DEFERRED_KHR);
        Case(ERROR_COMPRESSION_EXHAUSTED_EXT);
        default: return "UNKNOWN_ERROR";
    }
    #undef Case
}

VkResult addInstanceLayer(VulkanRenderer *r, const char *layer)
{
    for (int i = 0; i < r->LayerCount; ++i)
    {
        if (strcmp(layer, r->Layers[i].layerName) == 0)
        {
            r->EnabledLayers[r->EnabledLayerCount++] = layer;
            Log(LogInfo, "vulkan", "loading layer [%s]", layer);
            return VK_SUCCESS;
        }
    }

    Log(LogWarn, "vulkan", "layer [%s] not available", layer);
    return VK_ERROR_LAYER_NOT_PRESENT;
}

VkResult addInstanceExtension(VulkanRenderer *r, const char *extension)
{
    for (int i = 0; i < r->InstanceExtensionCount; ++i)
    {
        if (strcmp(extension, r->InstanceExtensions[i].extensionName) == 0)
        {
            r->EnabledInstancedExtensions[r->EnabledInstanceExtensionCount++] = extension;
            Log(LogInfo, "vulkan", "loading instance extension [%s]", extension);
            return VK_SUCCESS;
        }
    }

    Log(LogWarn, "vulkan", "instance extension [%s] not supported", extension);
    return VK_ERROR_EXTENSION_NOT_PRESENT;
}

VkResult addDeviceExtension(VulkanRenderer *r, const char *extension)
{
    for (int i = 0; i < r->DeviceExtensionCount; ++i)
    {
        if (strcmp(extension, r->DeviceExtensions[i].extensionName) == 0)
        {
            r->EnabledDeviceExtensions[r->EnabledDeviceExtensionCount++] = extension;
            Log(LogInfo, "vulkan", "loading device extension [%s]", extension);
            return VK_SUCCESS;
        }
    }

    Log(LogWarn, "vulkan", "device extension [%s] not supported", extension);
    return VK_ERROR_EXTENSION_NOT_PRESENT;
}

static VkResult loadInstanceLayers(VulkanRenderer *r)
{
    vkEnumerateInstanceLayerProperties(&r->LayerCount, nullptr);
    r->Layers = NewObjects(VkLayerProperties, r->LayerCount);
    vkEnumerateInstanceLayerProperties(&r->LayerCount, r->Layers);

    r->EnabledLayers = NewObjects(const char *, r->LayerCount);

    const char *enableValidation = getenv("KAME_VULKAN_ENABLE_VALIDATION");
    const char *enableRenderdocCapture = getenv("KAME_VULKAN_ENABLE_RENDERDOC");

    if (enableValidation && strcmp(enableValidation, "1") == 0)
        addInstanceLayer(r, "VK_LAYER_KHRONOS_validation");
    if (enableRenderdocCapture && strcmp(enableRenderdocCapture, "1") == 0)
        addInstanceLayer(r, "VK_LAYER_RENDERDOC_Capture");

    return VK_SUCCESS;
}

static VkResult loadInstanceExtensions(VulkanRenderer *r)
{
    VkChecked(vkEnumerateInstanceExtensionProperties(nullptr, &r->InstanceExtensionCount, nullptr));
    r->InstanceExtensions = NewObjects(VkExtensionProperties, r->InstanceExtensionCount);
    VkChecked(vkEnumerateInstanceExtensionProperties(nullptr, &r->InstanceExtensionCount, r->InstanceExtensions));

    r->EnabledInstancedExtensions = NewObjects(const char *, r->InstanceExtensionCount);
    VkChecked(addInstanceExtension(r, VK_KHR_SURFACE_EXTENSION_NAME));
    VkChecked(addInstanceExtensionsXcb(r));

    return VK_SUCCESS;
}

static int loadDeviceExtensions(VulkanRenderer *r)
{
    VkChecked(vkEnumerateDeviceExtensionProperties(r->PhysicalDevice, nullptr, &r->DeviceExtensionCount, nullptr));
    r->DeviceExtensions = NewObjects(VkExtensionProperties, r->DeviceExtensionCount);
    VkChecked(vkEnumerateDeviceExtensionProperties(
        r->PhysicalDevice,
        nullptr,
        &r->DeviceExtensionCount,
        r->DeviceExtensions));

    r->EnabledDeviceExtensions = NewObjects(const char *, r->DeviceExtensionCount);
    Checked(addDeviceExtension(r, VK_KHR_SWAPCHAIN_EXTENSION_NAME));

    return ErrNone;
}

static void queueDestroyBuffer(VulkanRenderer *r, VkBuffer buffer, VkDeviceMemory memory)
{
    if (r->DestroyBufferCapacity <= r->DestroyBufferCount)
    {
        if (!r->DestroyBuffers)
        {
            r->DestroyBufferCapacity = 32;
            r->DestroyBuffers = NewObjects(VulkanDestroyBuffer, r->DestroyBufferCapacity);
        }
        else
        {
            r->DestroyBufferCapacity = 3 * (r->DestroyBufferCapacity + 1) / 2;
            VulkanDestroyBuffer *newDestroyBuffers = NewObjects(VulkanDestroyBuffer, r->DestroyBufferCapacity);
            memcpy(newDestroyBuffers, r->DestroyBuffers, r->DestroyBufferCount * sizeof(VulkanDestroyBuffer));
            FreeMemory(r->DestroyBuffers);
            r->DestroyBuffers = newDestroyBuffers;
        }
    }

    r->DestroyBuffers[r->DestroyBufferCount++] = { buffer, memory };
}

static void destroyBuffers(VulkanRenderer *r)
{
    for (uint32_t i = 0; i < r->DestroyBufferCount; ++i)
    {
        vkDestroyBuffer(r->Device, r->DestroyBuffers[i].Buffer, nullptr);
        vkFreeMemory(r->Device, r->DestroyBuffers[i].Memory, nullptr);
    }
    r->DestroyBufferCount = 0;
}

static VkResult allocateDeviceMemory(
    VulkanRenderer            *r,
    VkMemoryRequirements   memoryRequirements,
    VkMemoryPropertyFlags  memoryFlags,
    VkDeviceMemory        *memory)
{
    uint32_t typeIndex = UINT32_MAX;
    uint32_t typeBits = memoryRequirements.memoryTypeBits;

    // NOTE: could also be implemented with a bitscan intrinsic
    for (int i = 0; i < r->PhysicalDeviceMemoryProperties.memoryTypeCount; ++i, typeBits >>= 1)
    {
        if (typeBits & 1)
        {
            if ((r->PhysicalDeviceMemoryProperties.memoryTypes[i].propertyFlags & memoryFlags) == memoryFlags)
            {
                typeIndex = i;
                break;
            }
        }
    }

    if (typeIndex == UINT32_MAX)
        return VK_ERROR_OUT_OF_DEVICE_MEMORY;

    VkMemoryAllocateInfo mai = {};
    mai.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    mai.allocationSize = memoryRequirements.size;
    mai.memoryTypeIndex = typeIndex;

    return vkAllocateMemory(r->Device, &mai, nullptr, memory);
}

static int setDeviceMemory(
    VulkanRenderer *r,
    VkDeviceMemory  memory,
    VkDeviceSize    length,
    const void     *data,
    VkDeviceSize    offset)
{
    void *mappedMemory;
    VkChecked(vkMapMemory(r->Device, memory, offset, length, 0, &mappedMemory));
    memcpy(mappedMemory, data, length);
    vkUnmapMemory(r->Device, memory);
    return ErrNone;
}

static int createShaderFromFile(VulkanRenderer *r, const char *path, VkShaderModule *shaderModule)
{
    MemoryMappedFile spirv;
    Checked(OpenMemoryMappedFile(path, &spirv));

    VkShaderModuleCreateInfo smci = {};
    smci.sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    smci.codeSize = (size_t) spirv.End - (size_t) spirv.Start;
    smci.pCode    = (const uint32_t *) spirv.Start;

    VkResult result = vkCreateShaderModule(r->Device, &smci, nullptr, shaderModule);
    Checked(CloseMemoryMappedFile(&spirv));
    CheckedError(result != VK_SUCCESS, ErrDefault, "vulkan: CreateShaderModule: %s", stringifyResult(result));
    return ErrNone;
}

static int createBuffer(
    VulkanRenderer        *r,
    VkBufferUsageFlags     usage,
    VkMemoryPropertyFlags  memoryFlags,
    VkDeviceSize           length,
    VkBuffer              *buffer,
    VkDeviceMemory        *memory)
{
    VkBufferCreateInfo bci = {};
    bci.sType       = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bci.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bci.usage       = usage;
    bci.size        = length;
    VkChecked(vkCreateBuffer(r->Device, &bci, nullptr, buffer));
    VkMemoryRequirements memoryRequirements;
    vkGetBufferMemoryRequirements(r->Device, *buffer, &memoryRequirements);
    VkChecked(allocateDeviceMemory(r, memoryRequirements, memoryFlags, memory));
    VkChecked(vkBindBufferMemory(r->Device, *buffer, *memory, 0));
    return ErrNone;
}

static int createSampler(VulkanRenderer *r, VkFilter filter, VkSampler *sampler)
{
    VkSamplerCreateInfo sci = {};
    sci.sType                   = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sci.magFilter               = filter;
    sci.minFilter               = filter;
    sci.borderColor             = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    sci.mipmapMode              =
        filter == VK_FILTER_LINEAR ? VK_SAMPLER_MIPMAP_MODE_LINEAR : VK_SAMPLER_MIPMAP_MODE_NEAREST;
    VkChecked(vkCreateSampler(r->Device, &sci, nullptr, sampler));

    return ErrNone;
}

static int createImage(
    VulkanRenderer    *r,
    uint32_t           width,
    uint32_t           height,
    VkFormat           format,
    VkImageUsageFlags  usage,
    VkImageAspectFlags aspect,
    VkImageLayout      initialLayout,
    VkImage           *image,
    VkDeviceMemory    *imageMemory,
    VkImageView       *imageView)
{
    Assert(width > 0 && height > 0);

    VkImageCreateInfo ici = {};
    ici.sType                 = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    ici.imageType             = VK_IMAGE_TYPE_2D;
    ici.samples               = VK_SAMPLE_COUNT_1_BIT;
    ici.usage                 = usage;
    ici.format                = format;
    ici.extent.width          = width;
    ici.extent.height         = height;
    ici.initialLayout         = initialLayout;
    ici.extent.depth          = 1;
    ici.arrayLayers           = 1;
    ici.mipLevels             = 1;
    VkChecked(vkCreateImage(r->Device, &ici, nullptr, image));

    VkMemoryRequirements memoryRequirements;
    vkGetImageMemoryRequirements(r->Device, *image, &memoryRequirements);
    VkChecked(allocateDeviceMemory(
        r,
        memoryRequirements,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        imageMemory));

    VkChecked(vkBindImageMemory(r->Device, *image, *imageMemory, 0));

    VkImageViewCreateInfo ivci = {};
    ivci.sType                       = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    ivci.viewType                    = VK_IMAGE_VIEW_TYPE_2D;
    ivci.format                      = format;
    ivci.subresourceRange.aspectMask = aspect;
    ivci.subresourceRange.levelCount = 1;
    ivci.subresourceRange.layerCount = 1;
    ivci.image                       = *image;
    VkChecked(vkCreateImageView(r->Device, &ivci, nullptr, imageView));

    return ErrNone;
}

static int bookTexture(VulkanTextureAtlasNode *node, VkRect2D *rect)
{
    if (rect->extent.width > node->extent.width || rect->extent.height > node->extent.height)
        return ErrOutOfResources;

    if (node->Children[0] != nullptr)
    {
        int result;
        result = bookTexture(node->Children[0], rect);
        if (result == ErrOutOfResources)
            result = bookTexture(node->Children[1], rect);
        return result;
    }
    else
    {
        node->Children[0] = NewObjects(VulkanTextureAtlasNode, 2);
        node->Children[1] = node->Children[0] + 1;

        *node->Children[0] = {};
        *node->Children[1] = {};

        //  offset,offset    offset+w,offset
        // x----------------x------------------+
        // |                |                  |
        // |       r        |                  |
        // x----------------+                  |
        // |offset,offset+h |         1        |
        // |                |                  |
        // |       0        |                  |
        // +----------------+------------------+

        node->Children[0]->offset = { node->offset.x, node->offset.y + (int32_t) rect->extent.height };
        node->Children[0]->extent = { rect->extent.width, node->extent.height - rect->extent.height };

        node->Children[1]->offset = { node->offset.x + (int32_t) rect->extent.width, node->offset.y };
        node->Children[1]->extent = { node->extent.width - rect->extent.width, node->extent.height };

        rect->offset = node->offset;
    }

    return ErrNone;
}

int updateTextureAtlas(VulkanRenderer *r, VulkanTexture *deviceTexture, Bitmap *bitmap)
{
    VulkanTextureAtlas *atlas = deviceTexture->Atlas;

    int needBuffer = atlas->StageBuffer == VK_NULL_HANDLE;
    if (atlas->StageBuffer && atlas->StageBufferLength < (VkDeviceSize) bitmap->Length)
    {
        vkDestroyBuffer(r->Device, atlas->StageBuffer, nullptr);
        vkFreeMemory(r->Device, atlas->StageBufferMemory, nullptr);
        needBuffer = 1;
    }
    if (needBuffer)
    {
        const size_t stageBufferLengthStep = 1024 * 1024;
        atlas->StageBufferLength = (VkDeviceSize) Align((size_t) bitmap->Length, stageBufferLengthStep);

        Checked(createBuffer(
            r,
            VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            atlas->StageBufferLength,
            &atlas->StageBuffer,
            &atlas->StageBufferMemory));
    }

    Checked(setDeviceMemory(r, atlas->StageBufferMemory, bitmap->Length, bitmap->Data, 0));

    if (!atlas->CommandBuffer)
    {
        VkCommandBufferAllocateInfo cbai = {};
        cbai.sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        cbai.level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        cbai.commandPool        = r->CommandPool;
        cbai.commandBufferCount = 1;
        VkChecked(vkAllocateCommandBuffers(r->Device, &cbai, &atlas->CommandBuffer));
    }

    VkCommandBufferBeginInfo cbbi = {};
    cbbi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    cbbi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    VkChecked(vkBeginCommandBuffer(atlas->CommandBuffer, &cbbi));

    VkImageMemoryBarrier imb = {};
    imb.sType                       = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imb.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imb.subresourceRange.levelCount = 1;
    imb.subresourceRange.layerCount = 1;
    imb.image                       = atlas->Image;

    imb.oldLayout                   = atlas->CurrentLayout;
    imb.newLayout                   = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    imb.srcAccessMask               = 0;
    imb.dstAccessMask               = VK_ACCESS_TRANSFER_WRITE_BIT;

    vkCmdPipelineBarrier(
        atlas->CommandBuffer,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        0,
        0, NULL,
        0, NULL,
        1, &imb);

    VkBufferImageCopy bic = {};
    bic.imageSubresource.layerCount = 1;
    bic.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    bic.imageExtent                 = { deviceTexture->Region.extent.width, deviceTexture->Region.extent.height, 1 };
    bic.imageOffset                 = { deviceTexture->Region.offset.x, deviceTexture->Region.offset.y, 0 };

    vkCmdCopyBufferToImage(
        atlas->CommandBuffer,
        atlas->StageBuffer,
        atlas->Image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &bic);

    imb.oldLayout     = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    imb.newLayout     = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imb.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    imb.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

    vkCmdPipelineBarrier(
        atlas->CommandBuffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        0,
        0, NULL,
        0, NULL,
        1, &imb);

    vkEndCommandBuffer(atlas->CommandBuffer);

    if (!atlas->CommandBufferFence)
    {
        VkFenceCreateInfo fci = {};
        fci.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        VkChecked(vkCreateFence(r->Device, &fci, nullptr, &atlas->CommandBufferFence));
    }
    else
        VkChecked(vkResetFences(r->Device, 1, &atlas->CommandBufferFence));

    VkSubmitInfo si = {};
    si.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    si.commandBufferCount = 1;
    si.pCommandBuffers    = &atlas->CommandBuffer;

    VkChecked(vkQueueSubmit(r->GraphicsQueue, 1, &si, atlas->CommandBufferFence));
    VkChecked(vkWaitForFences(r->Device, 1, &atlas->CommandBufferFence, VK_TRUE, UINT64_MAX));

    atlas->CurrentLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    return ErrNone;
}

int createOrUpdateDeviceTexture(VulkanRenderer *r, Texture *texture)
{
    Assert(texture != nullptr && !(texture->Flags & TextureFlagFramebuffer));

    int update = texture->RendererData != nullptr;

    VulkanTexture *deviceTexture;
    if (update)
    {
        Log(LogTrace, "vulkan", "update device texture [%s]", texture->Name);
        deviceTexture = (VulkanTexture *) texture->RendererData;
    }
    else
    {
        Log(LogTrace, "vulkan", "create device texture [%s]", texture->Name);
        texture->RendererData = deviceTexture = NewObjectZ(VulkanTexture);

        deviceTexture->Atlas         = r->Atlas + 0;
        deviceTexture->BaseTexture   = texture;
        deviceTexture->Region.extent = { texture->Bitmap.Width, texture->Bitmap.Height };
        CheckedError(
            bookTexture(deviceTexture->Atlas, &deviceTexture->Region),
            ErrOutOfResources,
            "vulkan",
            "could not book texture [%s]", texture->Name);

        deviceTexture->TextureCoordinate.Extent =
        {
            (float) texture->Bitmap.Width  / (float) deviceTexture->Atlas->extent.width,
            (float) texture->Bitmap.Height / (float) deviceTexture->Atlas->extent.height,
        };

        deviceTexture->TextureCoordinate.Offset =
        {
            (float) deviceTexture->Region.offset.x / (float) deviceTexture->Atlas->extent.width,
            (float) deviceTexture->Region.offset.y / (float) deviceTexture->Atlas->extent.height,
        };
    }

    Checked(updateTextureAtlas(r, deviceTexture, &texture->Bitmap));

    texture->Flags = (TextureFlags) (texture->Flags & ~TextureFlagDirty);
    return ErrNone;
}

static int createRenderPass(
    VulkanRenderer *r,
    VkFormat        colorFormat,
    VkFormat        depthStencilFormat,
    VkImageLayout   finalLayout,
    VkRenderPass   *renderPass)
{
    VkAttachmentDescription attachments[2] = {};
    uint32_t attachmentCount = 1;

    VkAttachmentReference colorAttachmentReference = {};
    colorAttachmentReference.layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    colorAttachmentReference.attachment = 0;

    VkAttachmentReference depthStencilAttachmentReference = {};
    depthStencilAttachmentReference.layout     = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    depthStencilAttachmentReference.attachment = 1;

    VkSubpassDescription subpassDescription = {};
    subpassDescription.pipelineBindPoint       = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpassDescription.colorAttachmentCount    = 1;
    subpassDescription.pColorAttachments       = &colorAttachmentReference;

    VkSubpassDependency subpassDependency = {};
    subpassDependency.srcSubpass    = VK_SUBPASS_EXTERNAL;
    subpassDependency.dstStageMask  = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    subpassDependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    attachments[0].format         = colorFormat;
    attachments[0].samples        = VK_SAMPLE_COUNT_1_BIT;
    attachments[0].loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[0].storeOp        = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[0].stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[0].initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED;
    attachments[0].finalLayout    = finalLayout;

    if (depthStencilFormat != VK_FORMAT_UNDEFINED)
    {
        attachments[1].format         = depthStencilFormat;
        attachments[1].samples        = VK_SAMPLE_COUNT_1_BIT;
        attachments[1].loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR;
        attachments[1].storeOp        = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        attachments[1].initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED;
        attachments[1].finalLayout    = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

        subpassDependency.srcStageMask  |= VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        subpassDependency.dstStageMask  |= VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
        subpassDependency.dstAccessMask |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        subpassDescription.pDepthStencilAttachment = &depthStencilAttachmentReference;
        attachmentCount += 1;
    }
    VkRenderPassCreateInfo rpci = {};
    rpci.sType           = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    rpci.attachmentCount = attachmentCount;
    rpci.pAttachments    = attachments;
    rpci.dependencyCount = 1;
    rpci.pDependencies   = &subpassDependency;
    rpci.subpassCount    = 1;
    rpci.pSubpasses      = &subpassDescription;

    VkChecked(vkCreateRenderPass(r->Device, &rpci, nullptr, renderPass));
    return ErrNone;
}

static int createOrUpdateRenderTexture(
    VulkanRenderer *r,
    VkFormat        colorFormat,
    VkFormat        depthStencilFormat,
    RenderTexture  *texture)
{
    Assert(texture != nullptr && (texture->Flags & TextureFlagFramebuffer));
    Assert(texture->Bitmap.Width > 0 && texture->Bitmap.Height > 0);
    Assert(colorFormat != VK_FORMAT_UNDEFINED && depthStencilFormat != VK_FORMAT_UNDEFINED);

    Log(LogTrace, "vulkan", "updating render texture [%s]", texture->Name);

    uint32_t attachmentCount = 0;
    VkImageView attachments[2];

    VulkanRenderTexture *renderTexture = (VulkanRenderTexture *) texture->RendererData;
    if (!renderTexture)
    {
        texture->RendererData = renderTexture = NewObjectZ(VulkanRenderTexture);
        renderTexture->BaseRenderTexture = texture;
    }

    if (renderTexture->Framebuffer)
    {
        vkDestroyFramebuffer(r->Device, renderTexture->Framebuffer, nullptr);
        vkDestroyImageView(r->Device, renderTexture->ColorImageView, nullptr);
        vkDestroyImageView(r->Device, renderTexture->DepthStencilImageView, nullptr);
        vkFreeMemory(r->Device, renderTexture->ColorMemory, nullptr);
        vkFreeMemory(r->Device, renderTexture->DepthStencilMemory, nullptr);
        vkDestroyImage(r->Device, renderTexture->ColorImage, nullptr);
        vkDestroyImage(r->Device, renderTexture->DepthStencilImage, nullptr);
    }

    renderTexture->Extent = { texture->Bitmap.Width, texture->Bitmap.Height };

    Checked(createImage(
        r,
        texture->Bitmap.Width,
        texture->Bitmap.Height,
        colorFormat,
        VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_IMAGE_LAYOUT_UNDEFINED,
        &renderTexture->ColorImage,
        &renderTexture->ColorMemory,
        &renderTexture->ColorImageView));
    attachments[attachmentCount++] = renderTexture->ColorImageView;

    if (depthStencilFormat != VK_FORMAT_UNDEFINED)
    {
        Checked(createImage(
            r,
            texture->Bitmap.Width,
            texture->Bitmap.Height,
            depthStencilFormat,
            VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
            VK_IMAGE_ASPECT_DEPTH_BIT,
            VK_IMAGE_LAYOUT_UNDEFINED,
            &renderTexture->DepthStencilImage,
            &renderTexture->DepthStencilMemory,
            &renderTexture->DepthStencilImageView));
        attachments[attachmentCount++] = renderTexture->DepthStencilImageView;
    }

    VkFramebufferCreateInfo fci = {};
    fci.sType           = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    fci.renderPass      = r->RenderTexturePass;
    fci.width           = texture->Bitmap.Width;
    fci.height          = texture->Bitmap.Height;
    fci.layers          = 1;
    fci.attachmentCount = attachmentCount;
    fci.pAttachments    = attachments;
    VkChecked(vkCreateFramebuffer(r->Device, &fci, nullptr, &renderTexture->Framebuffer));

    Log(
        LogTrace,
        "vulkan",
        "created %ux%u renderTexture [%s]",
        texture->Bitmap.Width,
        texture->Bitmap.Height,
        renderTexture->BaseRenderTexture->Name);

    ClearFlags(texture->Flags, TextureFlagDirty);
    return ErrNone;
}

static int createOrUpdateDescriptorSet(
    VulkanRenderer  *r,
    VkSampler        sampler,
    Texture         *textures[8],
    VkDescriptorSet *descriptorSet)
{
    Assert(sampler != VK_NULL_HANDLE);

    if (!*descriptorSet)
    {
        VkDescriptorSetAllocateInfo dsai = {};
        dsai.sType              = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        dsai.descriptorPool     = r->DescriptorPool;
        dsai.descriptorSetCount = 1;
        dsai.pSetLayouts        = &r->DescriptorSetLayout;
        VkChecked(vkAllocateDescriptorSets(r->Device, &dsai, descriptorSet));
    }

    VkDescriptorImageInfo dii[ArraySize(Material::Textures)] = {};

    VkWriteDescriptorSet wds[ArraySize(r->UniformBuffers) + ArraySize(dii)]  = {};
    VkDescriptorBufferInfo ubos[ArraySize(r->UniformBuffers)] = {};
    uint32_t writeDescriptorCount = 0;

    for (uint32_t i = 0; i < ArraySize(r->UniformBuffers); ++i)
    {
        ubos[i].range          = sizeof(VulkanRenderPassVertexUniformBlock);
        ubos[i].buffer         = r->UniformBuffers[i].Buffer;
        wds[i].sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        wds[i].dstSet          = *descriptorSet;
        wds[i].descriptorCount = 1;
        wds[i].descriptorType  = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        wds[i].pBufferInfo     = ubos + i;
        wds[i].dstBinding      = i;

        writeDescriptorCount += 1;
    }

    for (uint32_t samplerIndex = 0; samplerIndex < ArraySize(dii); ++samplerIndex)
    {
        if (!textures[samplerIndex])
            continue;

        if (textures[samplerIndex]->Flags & TextureFlagFramebuffer)
        {
            RenderTexture *renderTexture = (RenderTexture *) textures[samplerIndex];
            if (!renderTexture->RendererData || (textures[samplerIndex]->Flags & TextureFlagDirty))
                Checked(createOrUpdateRenderTexture(r, VK_FORMAT_R8G8B8A8_SRGB, VK_FORMAT_D24_UNORM_S8_UINT, (RenderTexture *) textures[samplerIndex]));
            VulkanRenderTexture *vulkanRenderTexture = (VulkanRenderTexture *) textures[samplerIndex]->RendererData;
            dii[samplerIndex].imageView = vulkanRenderTexture->ColorImageView;
        }
        else
        {
            if (!textures[samplerIndex]->RendererData || (textures[samplerIndex]->Flags & TextureFlagDirty))
                Checked(createOrUpdateDeviceTexture(r, textures[samplerIndex]));
            VulkanTexture *deviceTexture = (VulkanTexture *) textures[samplerIndex]->RendererData;
            dii[samplerIndex].imageView = deviceTexture->Atlas->ImageView;
        }

        // The last [wds] index is reserved for a sampler
        dii[samplerIndex].imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        dii[samplerIndex].sampler     = sampler;

        wds[writeDescriptorCount].sType           = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        wds[writeDescriptorCount].dstSet          = *descriptorSet;
        wds[writeDescriptorCount].descriptorCount = 1;
        wds[writeDescriptorCount].descriptorType  = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        wds[writeDescriptorCount].pImageInfo      = dii + samplerIndex;
        wds[writeDescriptorCount].dstBinding      = ArraySize(dii) + samplerIndex;

        writeDescriptorCount += 1;
    }

    vkUpdateDescriptorSets(r->Device, writeDescriptorCount, wds, 0, nullptr);
    return ErrNone;
}

static int createOrUpdateMaterial(VulkanRenderer *r, Material *material)
{
    if (!material->Shader)
        return ErrNone;

    if (!material->RendererData)
    {
        material->RendererData = NewObjectZ(VulkanMaterial);
    }
    VulkanMaterial *vulkanMaterial = (VulkanMaterial *) material->RendererData;
    vulkanMaterial->DataLength = sizeof(VulkanVertex);

    Checked(getMaterialPipeline(
        r,
        material,
        r->RenderTexturePass,
        sizeof(VulkanVertex),
        ArraySize(VulkanVertexAttributes),
        VulkanVertexAttributes,
        &vulkanMaterial->Pipeline));

    Checked(createOrUpdateDescriptorSet(
        r,
        r->SamplerNearest,
        material->Textures,
        &vulkanMaterial->DescriptorSet));

    ClearFlags(material->Flags, MaterialFlagDirty);

    return ErrNone;
}

static int createSurface(Platform *p, VulkanRenderer *r)
{
    if (strcmp("X11", p->WindowDriver.Name) == 0)
        return createSurfaceXcb(p, r);

    Log(LogError, "vulkan", "could not create surface for window driver [%s]", p->WindowDriver.Name);
    return ErrNotSupported;
}

static int pickPhysicalDevice(VulkanRenderer *r)
{
    uint32_t physicalDeviceCount;
    VkChecked(vkEnumeratePhysicalDevices(r->Instance, &physicalDeviceCount, nullptr));
    VkPhysicalDevice *physicalDevices = NewObjects(VkPhysicalDevice, physicalDeviceCount);
    VkChecked(vkEnumeratePhysicalDevices(r->Instance, &physicalDeviceCount, physicalDevices));

    r->PhysicalDevice = VK_NULL_HANDLE;

    uint32_t physicalDeviceIndex = -1;
    uint32_t wantedPhysicalDeviceIndex = -1;
    const char *wantedPhysicalDeviceName = getenv("KAME_VULKAN_PHYSICAL_DEVICE");

    for (uint32_t i = 0; i < physicalDeviceCount; ++i)
    {
        vkGetPhysicalDeviceProperties(physicalDevices[i], &r->PhysicalDeviceProperties);
        vkGetPhysicalDeviceMemoryProperties(physicalDevices[i], &r->PhysicalDeviceMemoryProperties);
        r->PhysicalDevice = physicalDevices[i];

        Log(LogInfo, "vulkan", "physical device %u: [%s]", i, r->PhysicalDeviceProperties.deviceName);

        if (wantedPhysicalDeviceName && strcmp(wantedPhysicalDeviceName, r->PhysicalDeviceProperties.deviceName) == 0)
            wantedPhysicalDeviceIndex = i;
        if (r->PhysicalDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
            physicalDeviceIndex = i;
    }

    // If an explicitly chosen device is found, use it over whatever was selected
    if (wantedPhysicalDeviceIndex < physicalDeviceCount)
        physicalDeviceIndex = wantedPhysicalDeviceIndex;

    if (physicalDeviceIndex < physicalDeviceCount)
    {
        vkGetPhysicalDeviceProperties(physicalDevices[physicalDeviceIndex], &r->PhysicalDeviceProperties);
        vkGetPhysicalDeviceMemoryProperties(physicalDevices[physicalDeviceIndex], &r->PhysicalDeviceMemoryProperties);
        r->PhysicalDevice = physicalDevices[physicalDeviceIndex];
    }

    FreeMemory(physicalDevices);

    CheckedError(r->PhysicalDevice == VK_NULL_HANDLE, ErrDefault, "vulkan", "no physical device available");
    return ErrNone;
}

static int pickQueueFamily(Platform *p, VulkanRenderer *r)
{
    r->GraphicsQueueFamily = r->PresentQueueFamily = UINT32_MAX;

    uint32_t queueFamilyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(r->PhysicalDevice, &queueFamilyCount, nullptr);
    VkQueueFamilyProperties *queueFamProps = NewObjects(VkQueueFamilyProperties, queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(r->PhysicalDevice, &queueFamilyCount, queueFamProps);

    int err = ErrNone;

    if (strcmp("X11", p->WindowDriver.Name) == 0)
    {
        for (uint32_t queueFamIndex = 0; queueFamIndex < queueFamilyCount; ++queueFamIndex)
        {
            int presentable;
            Checked(isQueueFamilyPresentableXcb(p, r, queueFamIndex, &presentable));
            if (presentable)
                r->PresentQueueFamily = queueFamIndex;
            if (queueFamProps[queueFamIndex].queueFlags & VK_QUEUE_GRAPHICS_BIT)
                r->GraphicsQueueFamily = queueFamIndex;
            if (r->PresentQueueFamily != UINT32_MAX && r->GraphicsQueueFamily != UINT32_MAX)
                break;
        }
        if (r->PresentQueueFamily == UINT32_MAX || r->GraphicsQueueFamily == UINT32_MAX)
        {
            Log(LogError, "vulkan", "could not find any supported queue family");
            err = ErrNotSupported;
        }
    }
    else
    {
        Log(LogError, "vulkan", "window driver [%s] not supported", p->WindowDriver.Name);
        err = ErrNotSupported;
    }

    FreeMemory(queueFamProps);
    return err;
}

static int createDevice(Platform *p, VulkanRenderer *r)
{
    VkChecked(loadInstanceLayers(r));
    VkChecked(loadInstanceExtensions(r));

    VkApplicationInfo ai = {};
    ai.sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    ai.apiVersion         = VK_MAKE_VERSION(1, 2, 0);
    ai.engineVersion      = VK_MAKE_VERSION(0, 0, 1);
    ai.applicationVersion = VK_MAKE_VERSION(0, 0, 1);
    ai.pEngineName        = "kame engine";
    ai.pApplicationName   = "kame application";

    VkInstanceCreateInfo ci = {};
    ci.sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    ci.pApplicationInfo        = &ai;
    ci.ppEnabledExtensionNames = r->EnabledInstancedExtensions;
    ci.ppEnabledLayerNames     = r->EnabledLayers;
    ci.enabledExtensionCount   = r->EnabledInstanceExtensionCount;
    ci.enabledLayerCount       = r->EnabledLayerCount;

    VkChecked(vkCreateInstance(&ci, nullptr, &r->Instance));
    Checked(createSurface(p, r));
    Checked(pickPhysicalDevice(r));
    Checked(pickQueueFamily(p, r));

    Checked(loadDeviceExtensions(r));

    const float queuePriority = 0.f;
    VkDeviceQueueCreateInfo dqci[2] = {};
    VkDeviceCreateInfo dci = {};
    dci.sType              = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

    // One queue for each queue family, but if they are a part of the same family, only one queue is needed
    dci.queueCreateInfoCount    = r->GraphicsQueueFamily == r->PresentQueueFamily ? 1 : 2;
    dci.pQueueCreateInfos       = dqci;
    dci.enabledExtensionCount   = r->EnabledDeviceExtensionCount;
    dci.ppEnabledExtensionNames = r->EnabledDeviceExtensions;

    dqci[0].sType            = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    dqci[0].pQueuePriorities = &queuePriority;
    dqci[0].queueFamilyIndex = r->GraphicsQueueFamily;
    dqci[0].queueCount       = 1;

    if (dci.queueCreateInfoCount > 1)
    {
        dqci[1] = dqci[0];
        dqci[1].queueFamilyIndex = r->PresentQueueFamily;
    }

    VkChecked(vkCreateDevice(r->PhysicalDevice, &dci, nullptr, &r->Device));
    Log(LogInfo, "vulkan", "created device using [%s]", r->PhysicalDeviceProperties.deviceName);
    LogDetail("maxMemoryAllocationCount              = %u", r->PhysicalDeviceProperties.limits.maxMemoryAllocationCount);
    LogDetail("maxPushConstantsSize                  = %u", r->PhysicalDeviceProperties.limits.maxPushConstantsSize);
    LogDetail("maxUniformBufferRange                 = %u", r->PhysicalDeviceProperties.limits.maxUniformBufferRange);
    LogDetail("maxDescriptorSetUniformBuffersDynamic = %u", r->PhysicalDeviceProperties.limits.maxDescriptorSetUniformBuffersDynamic);

    vkGetDeviceQueue(r->Device, r->GraphicsQueueFamily, 0, &r->GraphicsQueue);
    vkGetDeviceQueue(r->Device, r->PresentQueueFamily, 0, &r->PresentQueue);

    VkCommandPoolCreateInfo cpci = {};
    cpci.sType            = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cpci.queueFamilyIndex = r->GraphicsQueueFamily;
    cpci.flags            = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    VkChecked(vkCreateCommandPool(r->Device, &cpci, nullptr, &r->CommandPool));

    CheckedFatal(r->PhysicalDeviceProperties.limits.maxDescriptorSetUniformBuffersDynamic < 3, "vulkan", "limits");
    VkDescriptorPoolSize descriptorPoolSizes[2] = {};
    descriptorPoolSizes[0].type            = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    descriptorPoolSizes[0].descriptorCount = 3;
    descriptorPoolSizes[1].type            = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorPoolSizes[1].descriptorCount = 1;
    VkDescriptorPoolCreateInfo dpci = {};
    dpci.sType         = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    dpci.maxSets       = 128;
    dpci.poolSizeCount = ArraySize(descriptorPoolSizes);
    dpci.pPoolSizes    = descriptorPoolSizes;
    VkChecked(vkCreateDescriptorPool(r->Device, &dpci, nullptr, &r->DescriptorPool));

    const uint32_t samplerCount = 8;
    VkDescriptorSetLayoutBinding layoutBindings[ArraySize(r->UniformBuffers) + samplerCount] = {};
    layoutBindings[0].binding         = 0;
    layoutBindings[0].descriptorCount = 1;
    layoutBindings[0].descriptorType  = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    layoutBindings[0].stageFlags      = VK_SHADER_STAGE_VERTEX_BIT;
    layoutBindings[1].binding         = 1;
    layoutBindings[1].descriptorCount = 1;
    layoutBindings[1].descriptorType  = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    layoutBindings[1].stageFlags      = VK_SHADER_STAGE_VERTEX_BIT;
    layoutBindings[2].binding         = 2;
    layoutBindings[2].descriptorCount = 1;
    layoutBindings[2].descriptorType  = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    layoutBindings[2].stageFlags      = VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBindings[3].binding         = 3;
    layoutBindings[3].descriptorCount = 1;
    layoutBindings[3].descriptorType  = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    layoutBindings[3].stageFlags      = VK_SHADER_STAGE_FRAGMENT_BIT;

    for (uint32_t i = 0; i < samplerCount; ++i)
    {
        layoutBindings[ArraySize(r->UniformBuffers) + i].binding         = 8 + i;
        layoutBindings[ArraySize(r->UniformBuffers) + i].descriptorCount = 1;
        layoutBindings[ArraySize(r->UniformBuffers) + i].descriptorType  = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        layoutBindings[ArraySize(r->UniformBuffers) + i].stageFlags      = VK_SHADER_STAGE_FRAGMENT_BIT;
    }

    VkDescriptorSetLayoutCreateInfo dslc = {};
    dslc.sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    dslc.bindingCount = ArraySize(layoutBindings);
    dslc.pBindings    = layoutBindings;

    VkChecked(vkCreateDescriptorSetLayout(r->Device, &dslc, nullptr, &r->DescriptorSetLayout));

    VkPipelineLayoutCreateInfo plci = {};
    plci.sType          = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    plci.setLayoutCount = 1;
    plci.pSetLayouts    = &r->DescriptorSetLayout;
    VkChecked(vkCreatePipelineLayout(r->Device, &plci, nullptr, &r->PipelineLayout));

    return ErrNone;
}

static int createOrUpdateSwapchain(Platform *p, VulkanRenderer *r)
{
    uint32_t surfaceFormatCount;
    VkChecked(vkGetPhysicalDeviceSurfaceFormatsKHR(r->PhysicalDevice, r->Surface, &surfaceFormatCount, nullptr));
    VkSurfaceFormatKHR *surfaceFormats = NewObjects(VkSurfaceFormatKHR, surfaceFormatCount);
    VkChecked(vkGetPhysicalDeviceSurfaceFormatsKHR(r->PhysicalDevice, r->Surface, &surfaceFormatCount, surfaceFormats));
    VkChecked(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(r->PhysicalDevice, r->Surface, &r->SurfaceCapabilities));

    // Pick the closest format to SRGB, otherwise go with the first format
    r->SurfaceFormat = surfaceFormats[0];
    for (uint32_t i = 0; i < surfaceFormatCount; ++i)
    {
        if (VK_FORMAT_B8G8R8A8_SRGB == surfaceFormats[i].format)
        {
            r->SurfaceFormat = surfaceFormats[i];
            if (VK_COLOR_SPACE_SRGB_NONLINEAR_KHR == surfaceFormats[i].colorSpace)
                break;
        }
    }

    VkSwapchainCreateInfoKHR sci = {};
    sci.sType            = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    sci.surface          = r->Surface;
    sci.compositeAlpha   = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    sci.imageArrayLayers = 1;
    sci.presentMode      = VK_PRESENT_MODE_FIFO_KHR; // VSYNC
    sci.clipped          = VK_TRUE;
    sci.imageFormat      = r->SurfaceFormat.format;
    sci.imageColorSpace  = r->SurfaceFormat.colorSpace;
    sci.imageUsage       = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    sci.minImageCount    = r->SurfaceCapabilities.minImageCount;
    sci.imageExtent      = r->SurfaceCapabilities.currentExtent;
    sci.preTransform     = r->SurfaceCapabilities.currentTransform;
    sci.oldSwapchain     = r->Swapchain;

    uint32_t queueFamilies[] = { r->GraphicsQueueFamily, r->PresentQueueFamily };
    sci.pQueueFamilyIndices = queueFamilies;

    if (queueFamilies[0] == queueFamilies[1])
    {
        sci.queueFamilyIndexCount = 1;
        sci.imageSharingMode      = VK_SHARING_MODE_EXCLUSIVE;
    }
    else
    {
        sci.queueFamilyIndexCount = 2;
        sci.imageSharingMode      = VK_SHARING_MODE_CONCURRENT;
    }

    VkChecked(vkCreateSwapchainKHR(r->Device, &sci, nullptr, &r->Swapchain));

    if (sci.oldSwapchain != VK_NULL_HANDLE)
    {
        vkDestroySwapchainKHR(r->Device, sci.oldSwapchain, nullptr);
    }

    VkChecked(vkGetSwapchainImagesKHR(r->Device, r->Swapchain, &r->SwapchainImageCount, nullptr));
    CheckedFatal(
        r->SwapchainImageCount > ArraySize(r->SwapchainImages),
        "vulkan",
        "swapchain image count %u out of range",
        (uint32_t) ArraySize(r->SwapchainImages));
    VkChecked(vkGetSwapchainImagesKHR(r->Device, r->Swapchain, &r->SwapchainImageCount, r->SwapchainImages));

    if (!r->RenderTexture.RendererData)
    {
        r->RenderTexture.Name = "swapchain-staging";
        SetFlags(r->RenderTexture.Flags, TextureFlagFramebuffer);
    }

    r->RenderTexture.Bitmap.Width  = r->SurfaceCapabilities.currentExtent.width;
    r->RenderTexture.Bitmap.Height = r->SurfaceCapabilities.currentExtent.height;
    SetFlags(r->RenderTexture.Flags, TextureFlagDirty);
    Checked(createOrUpdateRenderTexture(
        r,
        VK_FORMAT_R8G8B8A8_SRGB,
        VK_FORMAT_D24_UNORM_S8_UINT,
        &r->RenderTexture));

    return ErrNone;
}

static VkDeviceSize calculateIndexBufferLength(Mesh *mesh)
{
    if (mesh->IndexCount <= UINT16_MAX)
        return mesh->IndexCount * sizeof(uint16_t);
    return mesh->IndexCount * sizeof(uint32_t);
}

static int createOrUpdateDeviceMesh(VulkanRenderer *r, Mesh *mesh)
{
    int update = mesh->RendererData != nullptr;

    VkDeviceSize requiredIndexBufferLength = calculateIndexBufferLength(mesh);

    VulkanMesh *deviceMesh;
    if (update)
    {
        Log(LogTrace, "vulkan", "update device mesh: [%s] with %u vertices, %u indices", mesh->Name, mesh->VertexCount, mesh->IndexCount);
        deviceMesh = (VulkanMesh *) mesh->RendererData;
        if (deviceMesh->VertexBuffer && deviceMesh->VertexCapacity < mesh->VertexCount)
        {
            queueDestroyBuffer(r, deviceMesh->VertexBuffer, deviceMesh->VertexBufferMemory);
            deviceMesh->VertexBuffer       = VK_NULL_HANDLE;
            deviceMesh->VertexBufferMemory = VK_NULL_HANDLE;
            deviceMesh->VertexBufferLength = 0;
            deviceMesh->VertexCapacity     = 0;
        }
        if (deviceMesh->IndexBuffer && deviceMesh->IndexMemoryLength < requiredIndexBufferLength)
        {
            queueDestroyBuffer(r, deviceMesh->IndexBuffer, deviceMesh->IndexMemory);
            deviceMesh->IndexBuffer       = VK_NULL_HANDLE;
            deviceMesh->IndexMemory       = VK_NULL_HANDLE;
            deviceMesh->IndexMemoryLength = 0;
        }
    }
    else
    {
        Log(LogTrace, "vulkan", "create device mesh: [%s] with %u vertices, %u indices", mesh->Name, mesh->VertexCount, mesh->IndexCount);
        mesh->RendererData = deviceMesh = NewObjectZ(VulkanMesh);
        deviceMesh->BaseMesh = mesh;
    }

    deviceMesh->VertexCount = (VkDeviceSize) mesh->VertexCount;
    deviceMesh->IndexCount  = (VkDeviceSize) mesh->IndexCount;

    if (mesh->VertexCount == 0)
        goto clearAndReturn;

    if (mesh->PartCount > 0)
        return ErrNotImplemented;

    if (!deviceMesh->VertexBuffer)
    {
        deviceMesh->VertexCapacity     = (VkDeviceSize) mesh->VertexCount;
        deviceMesh->VertexBufferLength = sizeof(VulkanVertex) * deviceMesh->VertexCapacity;

        Checked(createBuffer(
            r,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
            deviceMesh->VertexBufferLength,
            &deviceMesh->VertexBuffer,
            &deviceMesh->VertexBufferMemory));
    }

    VulkanVertex *vertices;
    VkChecked(vkMapMemory(
        r->Device,
        deviceMesh->VertexBufferMemory,
        0,
        (VkDeviceSize) sizeof(VulkanVertex) * deviceMesh->VertexCount,
        0,
        (void **) &vertices));

    if (mesh->Positions)
    {
        for (uint32_t i = 0; i < mesh->VertexCount; ++i)
            vertices[i].Position = mesh->Positions[i];
    }
    if (mesh->Colors)
    {
        for (uint32_t i = 0; i < mesh->VertexCount; ++i)
        {
            vertices[i].Color[0] = mesh->Colors[i][0];
            vertices[i].Color[1] = mesh->Colors[i][1];
            vertices[i].Color[2] = mesh->Colors[i][2];
        }
    }
    if (mesh->Normals)
    {
        for (uint32_t i = 0; i < mesh->VertexCount; ++i)
            vertices[i].Normal = mesh->Normals[i];
    }
    if (mesh->TextureCoordinates)
    {
        for (uint32_t i = 0; i < mesh->VertexCount; ++i)
            vertices[i].TextureCoordinate = mesh->TextureCoordinates[i];
    }

    vkUnmapMemory(r->Device, deviceMesh->VertexBufferMemory);

    if (!deviceMesh->Parts)
    {
        deviceMesh->PartCount = 1;
        deviceMesh->Parts     = NewObjects(VulkanMeshPart, 1);
    }

    deviceMesh->Parts[0]       = {};
    deviceMesh->Parts[0].Count = deviceMesh->VertexCount;

    if (deviceMesh->IndexCount > 0)
    {
        deviceMesh->IndexType = mesh->IndexCount <= UINT16_MAX ? VK_INDEX_TYPE_UINT16 : VK_INDEX_TYPE_UINT32;

        if (!deviceMesh->IndexBuffer)
        {
            deviceMesh->IndexMemoryLength = requiredIndexBufferLength;
            Checked(createBuffer(
                r,
                VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                deviceMesh->IndexMemoryLength,
                &deviceMesh->IndexBuffer,
                &deviceMesh->IndexMemory));
        }

        uint32_t *indices32;
        VkChecked(vkMapMemory(
           r->Device,
           deviceMesh->IndexMemory,
           0,
           requiredIndexBufferLength,
           0,
           (void **) &indices32));

        for (uint32_t i = 0; i < mesh->IndexCount; ++i)
        {
            if (mesh->Indices[i] >= mesh->VertexCount)
                Log(LogFatal, "vulkan", "invalid mesh [%s], indices out of bounds", mesh->Name);
        }

        if (deviceMesh->IndexType == VK_INDEX_TYPE_UINT16)
        {
            uint16_t *indices16 = (uint16_t *) indices32;
            for (uint32_t i = 0; i < mesh->IndexCount; ++i)
                indices16[i] = (uint16_t) mesh->Indices[i];
        }
        else memcpy(indices32, mesh->Indices, deviceMesh->IndexMemoryLength);

        vkUnmapMemory(r->Device, deviceMesh->IndexMemory);
    }

clearAndReturn:
    mesh->Flags = (MeshFlags) (mesh->Flags & ~MeshFlagDirty);
    return ErrNone;
}

static void bindPipeline(VkCommandBuffer cm, VulkanCommandBufferState *cmState, VkPipeline pipeline)
{
    if (cmState->ActivePipeline != pipeline)
    {
        vkCmdBindPipeline(cm, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
        cmState->ActivePipeline = pipeline;
    }
}

static void updateUniforms(
    VulkanRenderer  *r,
    VkCommandBuffer  cm,
    VkDescriptorSet  descriptorSet,
    Material        *material,
    SceneNode       *node,
    CameraNode      *camera)
{
    const uint32_t offsets[] =
    {
        r->UniformBuffers[0].Position,
        r->UniformBuffers[1].Position,
        r->UniformBuffers[2].Position,
        r->UniformBuffers[3].Position,
    };

    SetMaterialParameters params = {};
    params.Material = material;
    params.Node     = node;
    params.Camera   = camera;

    uint32_t vertexPosition = offsets[1];
    uint32_t fragmentPosition = offsets[3];

    if (material->Flags & MaterialFlagModelMatrix)
    {
        *((Matrix *) (r->UniformBuffers[1].Data + vertexPosition)) = *Scene_GetWorldTransform(node);
        vertexPosition += sizeof(Matrix);
    }

    for (uint32_t i = 0; i < ArraySize(material->Textures); ++i)
    {
        if (material->Flags & (MaterialFlagTexture1 << i))
        {
            PVector4 *region = (PVector4 *) (r->UniformBuffers[3].Data + fragmentPosition);
            if (material->Textures[i])
            {
                if (material->Textures[i]->Flags & TextureFlagFramebuffer)
                    *region = { 1, 1, 0, 0 };
                else
                {
                    VulkanTexture *deviceTexture = (VulkanTexture *) material->Textures[i]->RendererData;
                    *region =
                    {
                        deviceTexture->TextureCoordinate.Extent.X,
                        deviceTexture->TextureCoordinate.Extent.Y,
                        deviceTexture->TextureCoordinate.Offset.X,
                        deviceTexture->TextureCoordinate.Offset.Y,
                    };
                }
            }
            else
                *region = {};
            fragmentPosition += sizeof(PVector4);
        }
    }

    if (material->SetVertexParameters)
        vertexPosition += material->SetVertexParameters(params, r->UniformBuffers[1].Data + vertexPosition);

    if (material->SetFragmentParameters)
        fragmentPosition += material->SetFragmentParameters(params, r->UniformBuffers[3].Data + fragmentPosition);

    vkCmdBindDescriptorSets(
        cm,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        r->PipelineLayout,
        0,
        1,
        &descriptorSet,
        ArraySize(offsets),
        offsets);

    vertexPosition = Align(vertexPosition, r->PhysicalDeviceProperties.limits.minUniformBufferOffsetAlignment);
    fragmentPosition = Align(fragmentPosition, r->PhysicalDeviceProperties.limits.minUniformBufferOffsetAlignment);

    r->UniformBuffers[1].Position = vertexPosition;
    r->UniformBuffers[3].Position = fragmentPosition;
}

static int createBuiltInMaterials(VulkanRenderer *r)
{
    r->MissingTextureMaterial = NewObjectZ(Material);
    r->MissingTextureMaterial->CullMode = CullModeBack;
    r->MissingTextureMaterial->Name     = "builtin: missing texture";
    r->MissingTextureMaterial->Shader   = "missing_texture";
    Checked(createOrUpdateMaterial(r, r->MissingTextureMaterial));
    return ErrNone;
}

static int createBuiltInTextures(VulkanRenderer *r)
{
    const uint32_t gridSize = 4;
    uint32_t gridData[gridSize * gridSize];

    Texture _texture = {}, *texture = &_texture;
    texture->Name = "grid";
    texture->Bitmap.BitsPerPixel = 32;
    texture->Bitmap.Channels     = 4;
    texture->Bitmap.Width        = gridSize;
    texture->Bitmap.Height       = gridSize;
    texture->Bitmap.Pitch        = texture->Bitmap.Width * (texture->Bitmap.BitsPerPixel / 8);
    texture->Bitmap.Length       = texture->Bitmap.Pitch * texture->Bitmap.Height;
    texture->Bitmap.Data         = (uint8_t *) gridData;
    for (uint32_t y = 0; y < texture->Bitmap.Height; ++y)
    for (uint32_t x = 0; x < texture->Bitmap.Width; ++x)
    {
        uint32_t offset = y * texture->Bitmap.Pitch + x * (texture->Bitmap.BitsPerPixel / 8);
        uint32_t *pixel = (uint32_t *) (texture->Bitmap.Data + offset);
        *pixel = (x + y) % 2 ? 0xffff00ff : 0xffffff00;
    }
    Checked(createOrUpdateDeviceTexture(r, texture));
    r->GridTexture = (VulkanTexture *) texture->RendererData;
    return ErrNone;
}

static int drawNode(
    VulkanRenderer           *r,
    VkCommandBuffer           cm,
    VulkanCommandBufferState *cmState,
    CameraNode               *camera,
    MeshNode                 *node)
{
    if (r->DrawCallCount >= r->DrawCallCapacity)
    {
        Log(LogWarn, "vulkan", "out of uniform buffer space, dropping node [%p]", node);
        return ErrOutOfResources;
    }

    if (!node->Mesh || !node->Material)
        return ErrNone;

    VulkanMesh *deviceMesh = (VulkanMesh *) node->Mesh->RendererData;
    if (!deviceMesh || deviceMesh->BaseMesh->Flags & MeshFlagDirty)
    {
        Checked(createOrUpdateDeviceMesh(r, node->Mesh));
        deviceMesh = (VulkanMesh *) node->Mesh->RendererData;
    }

    if (deviceMesh->VertexCount == 0)
        return ErrNone;

    if (!node->Material->RendererData || (node->Material->Flags & MaterialFlagDirty))
        Checked(createOrUpdateMaterial(r, node->Material));

    Material *material = node->Material;
    VulkanMaterial *vulkanMaterial = (VulkanMaterial *) material->RendererData;

    for (uint32_t i = 0; i < ArraySize(material->Textures); ++i)
    {
        if (material->Textures[i])
        {
            if (material->Textures[i]->Flags & TextureFlagFramebuffer)
            {
                VulkanRenderTexture *renderTexture = (VulkanRenderTexture *) material->Textures[i]->RendererData;
                if (!renderTexture || renderTexture->BarrierFrameNumber < r->FrameNumber)
                {
                    LogDetail("render texture not ready [%s]", material->Textures[i]->Name);
                    material       = r->MissingTextureMaterial;
                    vulkanMaterial = (VulkanMaterial *) material->RendererData;
                }
                else
                {
                    LogDetail("using render texture [%s]", material->Textures[i]->Name);
                }
            }

            else if (!material->Textures[i]->RendererData || (material->Textures[i]->Flags & TextureFlagDirty))
                Checked(createOrUpdateDeviceTexture(r, material->Textures[i]));
        }
    }

    updateUniforms(r, cm, vulkanMaterial->DescriptorSet, material, node, camera);
    bindPipeline(cm, cmState, vulkanMaterial->Pipeline);

    if (deviceMesh->VertexBuffer != cmState->ActiveVertexBuffers[0])
    {
        VkDeviceSize offset = 0;
        vkCmdBindVertexBuffers(cm, 0, 1, &deviceMesh->VertexBuffer, &offset);
        cmState->ActiveVertexBuffers[0] = deviceMesh->VertexBuffer;
    }

    if (deviceMesh->IndexCount > 0)
    {
        if (deviceMesh->IndexBuffer != cmState->ActiveIndexBuffer || deviceMesh->IndexType != cmState->ActiveIndexType)
        {
            vkCmdBindIndexBuffer(cm, deviceMesh->IndexBuffer, 0, deviceMesh->IndexType);
            cmState->ActiveIndexBuffer = deviceMesh->IndexBuffer;
            cmState->ActiveIndexType   = deviceMesh->IndexType;
        }
        vkCmdDrawIndexed(cm, deviceMesh->IndexCount, 1, 0, 0, 0);
    }
    else
        vkCmdDraw(cm, deviceMesh->VertexCount, 1, 0, 0);

    r->DrawCallCount += 1;

    return ErrNone;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int load(Platform *p)
{
    VulkanRenderer *r = (VulkanRenderer *) p->RendererDriver.Data;

    Log(LogInfo, "vulkan", "loading");
    Checked(createDevice(p, r));
    Checked(createSampler(r, VK_FILTER_NEAREST, &r->SamplerNearest));
    Checked(createSampler(r, VK_FILTER_LINEAR, &r->SamplerLinear));
    Checked(createRenderPass(
        r,
        VK_FORMAT_R8G8B8A8_SRGB,
        VK_FORMAT_D24_UNORM_S8_UINT,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        &r->RenderTexturePass));

    r->RenderPassCapacity = 128;
    r->DrawCallCapacity = 4 * 1024;

    r->UniformBuffers[0].Length = 256 * r->RenderPassCapacity;
    r->UniformBuffers[1].Length = 256 * r->DrawCallCapacity;
    r->UniformBuffers[2].Length = 256 * r->RenderPassCapacity;
    r->UniformBuffers[3].Length = 256 * r->DrawCallCapacity;

    for (uint32_t i = 0; i < ArraySize(r->UniformBuffers); ++i)
    {
        Checked(createBuffer(
            r,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
            r->UniformBuffers[i].Length,
            &r->UniformBuffers[i].Buffer,
            &r->UniformBuffers[i].Memory));
        Checked(vkMapMemory(
            r->Device,
            r->UniformBuffers[i].Memory,
            0,
            r->UniformBuffers[i].Length,
            0,
            (void **) &r->UniformBuffers[i].Data));
    }

    Checked(createOrUpdateSwapchain(p, r));

    r->GeometryVertexBufferCapacity = 8096;
    r->GeometryVertexBuffer.VertexBufferLength = sizeof(VulkanVertex) * r->GeometryVertexBufferCapacity;
    Checked(createBuffer(
        r,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
        r->GeometryVertexBuffer.VertexBufferLength,
        &r->GeometryVertexBuffer.VertexBuffer,
        &r->GeometryVertexBuffer.VertexBufferMemory));

    for (uint32_t i = 0; i < 1; ++i)
    {
        r->Atlas[i].Format = VK_FORMAT_R8G8B8A8_SRGB;
        r->Atlas[i].extent = { 4096, 4096 }; // TODO: query device cap
        Checked(createImage(
            r,
            r->Atlas[i].extent.width,
            r->Atlas[i].extent.height,
            r->Atlas[i].Format,
            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_UNDEFINED,
            &r->Atlas[i].Image,
            &r->Atlas[i].ImageMemory,
            &r->Atlas[i].ImageView));
    }

    Checked(createBuiltInTextures(r));
    Checked(createBuiltInMaterials(r));
    return ErrNone;
}

static int draw(Platform *p)
{
    VulkanRenderer *r = (VulkanRenderer *) p->RendererDriver.Data;

    r->FrameNumber = p->FrameNumber;

    uint32_t cameraCount = Scene_SearchNodesOfType(
        p->ActiveScene,
        SceneNodeTypeCamera,
        SceneNodeFlagEnabled,
        SceneNodeFlagNone,
        0,
        1,
        ArraySize(r->CameraNodeBuffer),
        (SceneNode **) r->CameraNodeBuffer);

    if (cameraCount == 0)
        return ErrFinished;

    VkSemaphore drawSemaphore;
    VkSemaphoreCreateInfo sci = {};
    sci.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    sci.flags = VK_SEMAPHORE_TYPE_BINARY;
    VkChecked(vkCreateSemaphore(r->Device, &sci, nullptr, &drawSemaphore));

    VkFence drawFence;
    VkFenceCreateInfo fci = {};
    fci.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    VkChecked(vkCreateFence(r->Device, &fci, nullptr, &drawFence));

    VkResult acquireNextImageResult;
    uint32_t swapchainImageIndex = -1;
    while (true)
    {
        if (r->UpdateSwapchain)
        {
            Log(LogInfo, "vulkan", "recreating swapchain with new size");
            Checked(createOrUpdateSwapchain(p, r));
            r->UpdateSwapchain = 0;
        }

        acquireNextImageResult = vkAcquireNextImageKHR(
            r->Device,
            r->Swapchain,
            UINT64_MAX,
            drawSemaphore,
            VK_NULL_HANDLE,
            &swapchainImageIndex);

        if (acquireNextImageResult != VK_SUBOPTIMAL_KHR &&
            acquireNextImageResult != VK_ERROR_OUT_OF_DATE_KHR &&
            acquireNextImageResult != VK_ERROR_SURFACE_LOST_KHR)
            break;

        r->UpdateSwapchain = 1;
    }

    VkChecked(acquireNextImageResult);

    if (r->GeometryVertexBufferMappedMemory)
    {
        VkMappedMemoryRange mmr = {};
        mmr.sType  = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
        mmr.memory = r->GeometryVertexBuffer.VertexBufferMemory;
        mmr.size   = Align(
            r->GeometryVertexBuffer.VertexCount * sizeof(VulkanVertex),
            r->PhysicalDeviceProperties.limits.nonCoherentAtomSize);
        VkChecked(vkFlushMappedMemoryRanges(r->Device, 1, &mmr));
        vkUnmapMemory(r->Device, r->GeometryVertexBuffer.VertexBufferMemory);
        r->GeometryVertexBufferMappedMemory = nullptr;
    }

    VkCommandBuffer cm;
    VkCommandBufferAllocateInfo cbai = {};
    cbai.sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    cbai.commandPool        = r->CommandPool;
    cbai.level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    cbai.commandBufferCount = 1;
    VkChecked(vkAllocateCommandBuffers(r->Device, &cbai, &cm));

    VkCommandBufferBeginInfo cbbi = {};
    cbbi.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    cbbi.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    VkChecked(vkBeginCommandBuffer(cm, &cbbi));

    VkRect2D scissor = {};
    VkViewport viewport = { 0, 0, 0, 0, 0, 1 };

    r->DrawCallCount = 0;
    r->RenderPassCurrentCount    = 0;

    Log(LogTrace, "vulkan", "drawing");

    for (uint32_t c = 0; c < cameraCount; ++c)
    {
        CameraNode *cameraNode = r->CameraNodeBuffer[c];
        VulkanCommandBufferState cmState = {};

        uint32_t meshCount = Scene_SearchNodesOfType(
            p->ActiveScene,
            SceneNodeTypeMesh,
            SceneNodeFlagEnabled,
            SceneNodeFlagNone,
            cameraNode->RenderLayers,
            0,
            ArraySize(r->MeshNodeBuffer),
            (SceneNode **) r->MeshNodeBuffer);

        LogDetail("started render pass with camera [%s] dcs=%u", cameraNode->Name, meshCount);

        VkClearValue clearValues[2] = {};
        for (uint32_t i = 0; i < 4; ++i)
            clearValues[0].color.float32[i] = cameraNode->ClearColor[i];
        clearValues[1].depthStencil = { 1.0f, 0 };
        VkRenderPassBeginInfo rpbi = {};
        rpbi.sType             = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        rpbi.clearValueCount   = ArraySize(clearValues);
        rpbi.pClearValues      = clearValues;

        VulkanRenderTexture *renderTexture;
        if (cameraNode->Target)
        {
            if (!cameraNode->Target->RendererData || (cameraNode->Target->Flags & TextureFlagDirty))
                Checked(createOrUpdateRenderTexture(
                    r,
                    VK_FORMAT_R8G8B8A8_SRGB,
                    VK_FORMAT_D24_UNORM_S8_UINT,
                    cameraNode->Target));

            renderTexture = (VulkanRenderTexture *) cameraNode->Target->RendererData;
        }
        else
        {
            Assert(r->RenderTexture.RendererData != nullptr);
            renderTexture = (VulkanRenderTexture *) r->RenderTexture.RendererData;
        }

        rpbi.renderPass        = r->RenderTexturePass;
        rpbi.framebuffer       = renderTexture->Framebuffer;
        rpbi.renderArea.extent = renderTexture->Extent;

        scissor.extent = renderTexture->Extent;
        viewport.y      = (float) renderTexture->Extent.height;
        viewport.width  = (float) renderTexture->Extent.width;
        viewport.height = -viewport.y;

        vkCmdSetScissor(cm, 0, 1, &scissor);
        vkCmdSetViewport(cm, 0, 1, &viewport);

        Matrix view, projection;
        const Matrix *globalTransform = Scene_GetWorldTransform(cameraNode);
        Vector4 forwardT = Transform({0, 0, -1, 0}, globalTransform);
        Vector3 forward  = { forwardT.X,  forwardT.Y, forwardT.Z };
        Vector4 upT = Transform({0, 1, 0, 0}, globalTransform);
        Vector3 up  = { upT.X, upT.Y, upT.Z };
        Vector3 globalPosition = Scene_GetWorldPosition(cameraNode);
        LookAtMatrix(globalPosition, globalPosition + forward, up, &view);
        PerspectiveFovMatrix(
            cameraNode->FieldOfView,
            -viewport.width / viewport.height,
            cameraNode->Near,
            cameraNode->Far,
            &projection);

        VulkanRenderPassVertexUniformBlock *renderPassVertexUniformBlock = (VulkanRenderPassVertexUniformBlock *)
            (r->UniformBuffers[0].Data + r->UniformBuffers[0].Position);
        renderPassVertexUniformBlock->ViewProjection = view * projection;
        renderPassVertexUniformBlock->Near           = cameraNode->Near;
        renderPassVertexUniformBlock->Far            = cameraNode->Far;

        vkCmdBeginRenderPass(cm, &rpbi, VK_SUBPASS_CONTENTS_INLINE);

        for (uint32_t i = 0; i < meshCount; ++i)
            drawNode(r, cm, &cmState, cameraNode, r->MeshNodeBuffer[i]);

        // Only draw the lines when the camera is going to the swapchain
        // if (!cameraNode->Target && r->GeometryVertexBuffer.VertexCount > 0)
        // {
        //     if (r->DrawCallCount < r->DrawCallCapacity)
        //     {
        //         Matrix identity;
        //         IdentityMatrix(&identity);
        //         vkCmdBindPipeline(cm, VK_PIPELINE_BIND_POINT_GRAPHICS, r->GeometryPipeline);
        //         updateUniforms(r, cm, r->ColorDescriptorSet, &identity);
        //         VkDeviceSize offset = 0;
        //         vkCmdBindVertexBuffers(cm, 0, 1, &r->GeometryVertexBuffer.VertexBuffer, &offset);
        //         vkCmdDraw(cm, (uint32_t) r->GeometryVertexBuffer.VertexCount, 1, 0, 0);
        //     }
        // }

        vkCmdEndRenderPass(cm);

        if (cameraNode->Target)
        {
            VulkanRenderTexture *renderTexture = (VulkanRenderTexture *) cameraNode->Target->RendererData;
            renderTexture->BarrierFrameNumber = r->FrameNumber;
            VkImageMemoryBarrier imb = {};
            imb.sType                       = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
            imb.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            imb.subresourceRange.levelCount = 1;
            imb.subresourceRange.layerCount = 1;
            imb.oldLayout                   = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            imb.newLayout                   = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            imb.image                       = renderTexture->ColorImage;
            imb.srcAccessMask               = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            imb.dstAccessMask               = VK_ACCESS_SHADER_READ_BIT;
            vkCmdPipelineBarrier(
                cm,
                VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                0,
                0, nullptr,
                0, nullptr,
                1, &imb);
            LogDetail("added pipeline barrier to render texture [%s]", cameraNode->Target->Name);
        }

        r->UniformBuffers[0].Position += Align(sizeof(VulkanRenderPassVertexUniformBlock), r->PhysicalDeviceProperties.limits.minUniformBufferOffsetAlignment);
        r->RenderPassCurrentCount += 1;
    }

    r->GeometryVertexBuffer.VertexCount = 0;

    VkMappedMemoryRange mrs[ArraySize(r->UniformBuffers)] = {};
    for (uint32_t i = 0; i < ArraySize(r->UniformBuffers); ++i)
    {
        mrs[i].sType  = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
        mrs[i].size   = Align(r->UniformBuffers[i].Position, r->PhysicalDeviceProperties.limits.nonCoherentAtomSize);
        mrs[i].memory = r->UniformBuffers[i].Memory;
        r->UniformBuffers[i].Position = 0;
    }
    VkChecked(vkFlushMappedMemoryRanges(r->Device, ArraySize(r->UniformBuffers), mrs));

    VulkanRenderTexture *renderTexture = (VulkanRenderTexture *) r->RenderTexture.RendererData;
    renderTexture->BarrierFrameNumber = r->FrameNumber;
    VkImageMemoryBarrier imb[2] = {};
    imb[0].sType                       = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imb[0].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imb[0].subresourceRange.levelCount = 1;
    imb[0].subresourceRange.layerCount = 1;
    imb[0].oldLayout                   = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imb[0].newLayout                   = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    imb[0].image                       = renderTexture->ColorImage;
    imb[0].srcAccessMask               = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    imb[0].dstAccessMask               = VK_ACCESS_TRANSFER_READ_BIT;
    imb[1].sType                       = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    imb[1].subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imb[1].subresourceRange.levelCount = 1;
    imb[1].subresourceRange.layerCount = 1;
    imb[1].oldLayout                   = VK_IMAGE_LAYOUT_UNDEFINED;
    imb[1].newLayout                   = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    imb[1].image                       = r->SwapchainImages[swapchainImageIndex];
    imb[1].srcAccessMask               = 0;
    imb[1].dstAccessMask               = VK_ACCESS_TRANSFER_WRITE_BIT;
    vkCmdPipelineBarrier(
        cm,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        0,
        0, nullptr,
        0, nullptr,
        2, imb);

    VkImageBlit ib = {};
    ib.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    ib.srcSubresource.layerCount = 1;
    ib.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    ib.dstSubresource.layerCount = 1;
    ib.srcOffsets[1].x = ib.dstOffsets[1].x = (int32_t) r->SurfaceCapabilities.currentExtent.width;
    ib.srcOffsets[1].y = ib.dstOffsets[1].y = (int32_t) r->SurfaceCapabilities.currentExtent.height;
    ib.srcOffsets[1].z = ib.dstOffsets[1].z = (int32_t) 1;
    vkCmdBlitImage(
        cm,
        ((VulkanRenderTexture *) r->RenderTexture.RendererData)->ColorImage,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        r->SwapchainImages[swapchainImageIndex],
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &ib,
        VK_FILTER_NEAREST);

    imb[0].oldLayout     = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    imb[0].newLayout     = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    imb[0].image         = r->SwapchainImages[swapchainImageIndex];
    imb[0].srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    imb[0].dstAccessMask = 0;
    vkCmdPipelineBarrier(
        cm,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        0,
        0, nullptr,
        0, nullptr,
        1, imb);

    VkChecked(vkEndCommandBuffer(cm));

    VkPipelineStageFlags pipelineStageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    VkSubmitInfo submitInfo = {};
    submitInfo.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.commandBufferCount = 1;
    submitInfo.pWaitSemaphores    = &drawSemaphore;
    submitInfo.pCommandBuffers    = &cm;
    submitInfo.pWaitDstStageMask  = &pipelineStageFlags;
    VkChecked(vkQueueSubmit(r->GraphicsQueue, 1, &submitInfo, drawFence));
    VkChecked(vkWaitForFences(r->Device, 1, &drawFence, VK_TRUE, UINT64_MAX));

    destroyBuffers(r);

    VkPresentInfoKHR presentInfo = {};
    presentInfo.sType          = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains    = &r->Swapchain;
    presentInfo.pImageIndices  = &swapchainImageIndex;

    VkResult queuePresentResult = vkQueuePresentKHR(r->PresentQueue, &presentInfo);
    if (queuePresentResult == VK_SUBOPTIMAL_KHR ||
        queuePresentResult == VK_ERROR_OUT_OF_DATE_KHR ||
        queuePresentResult == VK_ERROR_SURFACE_LOST_KHR)
        r->UpdateSwapchain = 1;
    else VkChecked(queuePresentResult);

    vkFreeCommandBuffers(r->Device, r->CommandPool, 1, &cm);
    vkDestroyFence(r->Device, drawFence, nullptr);
    vkDestroySemaphore(r->Device, drawSemaphore, nullptr);

    return ErrNone;
}

static int prepareMesh(Platform *p, Mesh *mesh)
{
    VulkanRenderer *r = (VulkanRenderer *) p->RendererDriver.Data;
    return createOrUpdateDeviceMesh(r, mesh);
}

static int prepareTexture(Platform *p, Texture *texture)
{
    VulkanRenderer *r = (VulkanRenderer *) p->RendererDriver.Data;
    return createOrUpdateDeviceTexture(r, texture);
}

static int prepareMaterial(Platform *p, Material *material)
{
    VulkanRenderer *r = (VulkanRenderer *) p->RendererDriver.Data;
    return createOrUpdateMaterial(r, material);
}

static void drawLine(Platform *p, Vector3 v1, Vector3 v2, Vector4 color)
{
    VulkanRenderer *r = (VulkanRenderer *) p->RendererDriver.Data;

    if (!r->GeometryVertexBufferMappedMemory)
    {
        VkChecked(vkMapMemory(
            r->Device,
            r->GeometryVertexBuffer.VertexBufferMemory,
            0,
            r->GeometryVertexBuffer.VertexBufferLength,
            0,
            &r->GeometryVertexBufferMappedMemory));
    }

    VulkanVertex *vertices = (VulkanVertex *) r->GeometryVertexBufferMappedMemory;
    VulkanVertex *vertex   = vertices + r->GeometryVertexBuffer.VertexCount;

    if (r->GeometryVertexBuffer.VertexCount + 2 <= r->GeometryVertexBufferCapacity)
    {
        vertex->Color    = { color.X, color.Y, color.Z };
        vertex->Position = { v1.X, v1.Y, v1.Z };

        vertex += 1;

        vertex->Color    = { color.X, color.Y, color.Z };
        vertex->Position = { v2.X, v2.Y, v2.Z };

        r->GeometryVertexBuffer.VertexCount += 2;
    }
}

static void drawText(Platform *p, const char *text, Vector4 color)
{

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

KameAPI int Init(Platform *p)
{
    Log(LogInfo, "vulkan", "init");

    p->RendererDriver.Load            = load;
    p->RendererDriver.Draw            = draw;
    p->RendererDriver.PrepareMesh     = prepareMesh;
    p->RendererDriver.PrepareTexture  = prepareTexture;
    p->RendererDriver.PrepareMaterial = prepareMaterial;
    p->RendererDriver.DrawLine        = drawLine;
    p->RendererDriver.DrawText        = drawText;

    p->RendererDriver.Data = NewObjectZ(VulkanRenderer);
    p->RendererDriver.Name = "Vulkan";

    return ErrNone;
}