#pragma once

#include <stddef.h>
#include <stdint.h>

#define KameAPI extern "C"

#define Checked(expr) do { int _r; if ((_r = expr)) return _r; } while(0)
#define CheckedError(expr, ret, category, msg, ...) \
    do { if (expr) { Log(LogError, category, msg, ##__VA_ARGS__); return ret; } } while(0)
#define CheckedFatal(expr, category, msg, ...) \
    do { if (expr) { Log(LogFatal, category, msg, ##__VA_ARGS__); } } while(0)

#define ArraySize(arg) (sizeof(arg) / sizeof(*arg))
#define ArraySetN(arg, len, value) do { for (size_t _i = 0; _i < len; ++_i) arg[_i] = value; } while (0)
#define ArraySet(arg, value) ArraySetN(arg, ArraySize(arg), value)

#define PPCat_(lhs, rhs) lhs ## rhs
#define PPCat(lhs, rhs) PPCat_(lhs, rhs)

#define Between(arg, min, max) ((arg) >= (min) && (arg) < (max))

#define Out

#define SetFlags(lval, flags) do { lval = (decltype(lval)) (lval | flags); } while (0)
#define ClearFlags(lval, flags) do { lval = (decltype(lval)) (lval & ~flags); } while (0)

#ifdef NDEBUG
#define Assert(exp)
#else
#define Assert(exp) do { if (!(exp)) Log(LogFatal, "assert", #exp); } while (0)
#endif

template <typename F> struct ScopeExit { F f; ~ScopeExit() { f(); } };
#define _ScopeExit(idx, expr) \
auto PPCat(_m, idx) = [&]() { expr; }; \
ScopeExit<decltype(PPCat(_m, idx))> PPCat(_sg, idx) { PPCat(_m, idx) };
#define ScopeExit(expr) _ScopeExit(__COUNTER__, expr)

#define ValidateParam(expr) do { if (!(expr)) return ErrInvalidParameter; } while (0)

typedef void *Handle;

struct Platform;

typedef enum
{
    LogDebug  = 1,
    LogTrace  = 2,
    LogInfo   = 3,
    LogWarn   = 4,
    LogError  = 5,
    LogFatal  = 6,
    LogDetail = 7,
} LogLevel;

typedef enum
{
    ErrNone,
    ErrDefault,
    ErrNotImplemented,
    ErrNotSupported,
    ErrNotFinished,
    ErrFinished,
    ErrTaskCancelled,
    ErrTryAgain,
    ErrInvalidData,
    ErrInvalidParameter,
    ErrIteratorFinished,
    ErrOutOfResources,
} Error;

inline size_t Align(size_t value, size_t alignment)
{
    size_t k = value % alignment;
    return k ? value + (alignment - k) : value;
}