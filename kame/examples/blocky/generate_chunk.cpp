#include <stdio.h>
#include <string.h>

#include <kame/engine/random.h>
#include <blocky/blocky.h>

struct BiomeList
{
    const MapBiome *DominantBiome;
    const MapBiome *OtherBiome;
    float           Factor;
};

static void getBiomes(float biomeFactor, BiomeList *biomes)
{
    static const MapBiome candidates[] =
    {
        { MapBiomeTypeForest, MapTileTypeGrass, 0.50f, 0.55f },
        { MapBiomeTypeHills , MapTileTypeGrass, 0.48f, 0.78f },
        { MapBiomeTypeDesert, MapTileTypeSand,  0.50f, 0.55f },
        { MapBiomeTypePlains, MapTileTypeGrass, 0.50f, 0.52f },
    };

    const float biomeWidth = 1.f / ArraySize(candidates);
    int biomeIndex = (int) floorf(biomeFactor / biomeWidth);

    *biomes = {};
    biomes->Factor        = fmodf(biomeFactor, biomeWidth) / biomeWidth;
    biomes->DominantBiome = candidates + biomeIndex;

    if (biomes->Factor < 0.5f)
    {
        biomes->OtherBiome =
            biomeIndex == 0 ?
            candidates + (MapBiomeTypePlains - 1) :
            candidates + biomeIndex - 1;
        biomes->Factor = biomes->Factor * 2.f;
    }
    else
    {
        biomes->OtherBiome =
            biomeIndex == 3 ?
            candidates + (MapBiomeTypeForest - 1) :
            candidates + biomeIndex + 1;
        biomes->Factor = (1.f - biomes->Factor) * 2.f;
    }
}

static MapTileType mapTile(MapTileType type, uint32_t height, uint32_t ih)
{
    if (type == MapTileTypeGrass)
        return ih == height - 1 ? MapTileTypeGrass : MapTileTypeDirt;
    return type;
}

static float perlin(float x, float y, float seed, int iterations)
{
    float weight = 0.f, value = 0.f;
    while (iterations > 0)
    {
        value  += iterations * Random_Noise(x, y, seed);
        weight += iterations;
        x *= 2.f;
        y *= 2.f;
        --iterations;
    }
    return value / weight;
}

static void initializeChunk(MapChunk *chunk, int32_t x, int32_t y)
{
    chunk->X = x;
    chunk->Y = y;

    if (!chunk->Tiles)
        chunk->Tiles = NewObjects(MapTile, MAP_CHUNK_HEIGHT * MAP_CHUNK_SIZE * MAP_CHUNK_SIZE);
    if (!chunk->Mesh)
        chunk->Mesh  = NewObjectZ(DynamicMesh);
    if (!chunk->Mesh->Name)
        chunk->Mesh->Name = NewObjects(char, 24);

    snprintf((char *) chunk->Mesh->Name, 24, "chunk %d %d", x, y);
}

#if 1
static float caveFrequency(uint32_t h)
{
    if (h > 55)
        return 0.28f + (h - 55) * 0.06f;
    float factor = abs((float) h - MAP_CHUNK_HEIGHT / 4);
    return 0.50f - factor * 0.01f;
}

static float mineralFrequency(uint32_t h)
{
    return 1.0f - (float) h / (MAP_CHUNK_HEIGHT / 2);
}

static MapTileType mineralToTile(float mineralFactor)
{
    if (Between(mineralFactor, 0.560f, 0.570f))
        return MapTileTypeIron;
    if (Between(mineralFactor, 0.660f, 0.662f))
        return MapTileTypeGold;
    if (Between(mineralFactor, 0.770f, 0.773f))
        return MapTileTypeDiamond;
    if (Between(mineralFactor, 0.400f, 0.415f))
        return MapTileTypeCoal;
    return MapTileTypeStone;
}

int GenerateChunk(int32_t x, int32_t y, int initializeOnly, MapChunk *chunk, TaskToken *taskToken)
{
    const Vector3 zoom = { 0.025f, 0.025f, 0.025f };

    initializeChunk(chunk, x, y);
    if (initializeOnly)
        return ErrNone;

    const Vector3 basePosition = zoom * Vector3 { (float) x * MAP_CHUNK_SIZE, MAP_CHUNK_HEIGHT * 0.5f, (float) y * MAP_CHUNK_SIZE };

    Vector3 position = basePosition;
    Vector3 step     = zoom * Vector3 { MAP_TILE_SIZE, MAP_TILE_SIZE, MAP_TILE_SIZE };

    memset(chunk->Tiles, 0, sizeof(MapTile) * MAP_CHUNK_HEIGHT * MAP_CHUNK_SIZE * MAP_CHUNK_SIZE);

    for (uint32_t h = 70; --h > 0; position.Y -= step.Y)
    {
        CheckCancellation(taskToken);

        position.Z = basePosition.Z;
        for (uint32_t j = 0; j < MAP_CHUNK_SIZE; ++j, position.Z += step.Z)
        {
            position.X = basePosition.X;
            for (uint32_t i = 0; i < MAP_CHUNK_SIZE; ++i, position.X += step.X)
            {
                MapTile *tile = GetTile(chunk, i, j, h);
                *tile = {};

                Vector4 noise =
                {
                    Random_Noise(position.X        , position.Y        , position.Z        ),
                    Random_Noise(position.X * -2.0f, position.Y * -2.0f, position.Z * -2.0f),
                    Random_Noise(position.X *  4.0f, position.Y *  4.0f, position.Z *  4.0f),
                    Random_Noise(position.X * -8.0f, position.Y * -8.0f, position.Z * -8.0f),
                };

                float biome = Random_Noise(position.X * 0.3f, 0, position.Z * 0.3f);

                float o = noise[0] * 0.2f + noise[1] * 0.2f + noise[2] * 0.3f + noise[3] * 0.3f;

                float caveFactor = caveFrequency(h);
                //float caveNoise = Clamp(o1 * 0.6f + o2 * 0.2f + o3 * 0.2f, 0.0f, 1.0f);

                if (o > caveFactor)
                {
                    float mineralFactor = mineralFrequency(h);
                    if (mineralFactor > noise[3])
                        tile->Type = mineralToTile(noise[3]);
                    else if (h < MAP_CHUNK_HEIGHT / 3)
                        tile->Type = MapTileTypeDirt;
                    else
                    {
                        if (biome < 0.5f)
                            tile->Type = MapTileTypeSand;
                        else tile->Type =
                            (h < MAP_CHUNK_HEIGHT - 1 && GetTile(chunk, i, j, h + 1)->Type != MapTileTypeNone) ?
                            MapTileTypeDirt :
                            MapTileTypeGrass;
                    }
                }
            }
        }
    }

    for (uint32_t j = 0; j < MAP_CHUNK_SIZE; ++j)
    for (uint32_t i = 0; i < MAP_CHUNK_SIZE; ++i)
        GetTile(chunk, i, j, 0)->Type = MapTileTypeBedrock;

    return ErrNone;
}

#else
int GenerateChunk(int32_t x, int32_t y, int initializeOnly, MapChunk *chunk, TaskToken *taskToken)
{
    const float z = 42.f;

    initializeChunk(chunk, x, y);
    if (initializeOnly)
        return ErrNone;

    const Vector2 chunkOffset = Vector2 { (float) x, (float) y } * MAP_CHUNK_SIZE;
    const Vector2 chunkStep   = Vector2 { MAP_TILE_SIZE, MAP_TILE_SIZE };
    for (uint32_t iy = 0; iy < MAP_CHUNK_SIZE  ; ++iy)
    for (uint32_t ix = 0; ix < MAP_CHUNK_SIZE  ; ++ix)
    {
        CheckCancellation(taskToken);

        Vector2 worldPosition = chunkOffset + chunkStep * Vector2 { (float) ix, (float) iy };
        Vector2 heightPosition = worldPosition * 0.02f;
        Vector2 biomePosition = worldPosition * 0.002f;
        float biomeFactor  = Random_Noise(biomePosition.X, biomePosition.Y, z);
        float heightFactor = perlin(heightPosition.X, heightPosition.Y, z, 3);
        float mixFactor    = Random_Noise(worldPosition.X, worldPosition.Y, z);
        float caveFactor   = perlin(heightPosition.X, heightPosition.Y, z, 4);

        BiomeList biome;
        getBiomes(biomeFactor, &biome);

        uint32_t height;
        if (biome.Factor < 0.015f)
        {
            float dominantHeight = Lerp(biome.DominantBiome->MinHeight, biome.DominantBiome->MaxHeight, heightFactor);
            float otherHeight = Lerp(biome.OtherBiome->MinHeight, biome.OtherBiome->MaxHeight, heightFactor);
            height = (uint32_t) (Lerp(otherHeight, dominantHeight, biome.Factor / 0.015f) * MAP_CHUNK_HEIGHT);
        }
        else
            height = (uint32_t) (Lerp(biome.DominantBiome->MinHeight, biome.DominantBiome->MaxHeight, heightFactor) * MAP_CHUNK_HEIGHT);

        MapTile *bottomTile = GetTile(chunk, ix, iy, 0);
        *bottomTile = {};
        bottomTile->Type = MapTileTypeBedrock;

        for (uint32_t ih = 1; ih < MAP_CHUNK_HEIGHT; ++ih)
        {
            MapTile *tile = GetTile(chunk, ix, iy, ih);
            *tile = {};

            if (caveFactor < 0.4f - mixFactor * 0.025f - fabsf(MAP_CHUNK_SIZE - (float) ih) * 0.015f) // caves
            { }
            else if (height * (0.60f + 0.05f * mixFactor) > ih) // rock layer
                tile->Type = MapTileTypeStone;
            else if (height > ih) // overworld layer
            {
                if (biome.Factor < 0.015f)
                {
                    float heightOffset = fmodf(ih * mixFactor, 1.0f);
                    if (mixFactor + heightOffset > biomeFactor)
                        tile->Type = mapTile(biome.DominantBiome->MainTile, height, ih);
                    else
                        tile->Type = mapTile(biome.OtherBiome->MainTile, height, ih);
                }
                else
                    tile->Type = mapTile(biome.DominantBiome->MainTile, height, ih);
            }
        }
    }

    return ErrNone;
}
#endif