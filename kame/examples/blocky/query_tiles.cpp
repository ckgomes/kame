#include <blocky/blocky.h>

static uint32_t queryChunkTiles(MapChunk *chunk, int32_t d[2], Vector3 bounds[2], uint32_t size, MapTileQuery *buffer)
{
    Assert(bounds[0].X <= bounds[1].X && bounds[0].Y <= bounds[1].Y && bounds[0].Z <= bounds[1].Z);

    if (!chunk->Tiles)
        return 0;

    int32_t iMin = Max((int32_t) floor(bounds[0].X), 0);
    int32_t jMin = Max((int32_t) floor(bounds[0].Z), 0);
    int32_t hMin = Max((int32_t) floor(bounds[0].Y), 0);
    int32_t iMax = Min((int32_t) ceil(bounds[1].X), MAP_CHUNK_SIZE);
    int32_t jMax = Min((int32_t) ceil(bounds[1].Z), MAP_CHUNK_SIZE);
    int32_t hMax = Min((int32_t) ceil(bounds[1].Y), MAP_CHUNK_HEIGHT);

    uint32_t count = 0;

    for (int32_t h = hMin; h < hMax; ++h)
    for (int32_t j = jMin; j < jMax; ++j)
    for (int32_t i = iMin; i < iMax; ++i)
    {
        MapTileQuery *tile = buffer + count++;
        tile->Type = GetTile(chunk, i, j, h)->Type;
        tile->Position[0] = i + d[0];
        tile->Position[2] = j + d[1];
        tile->Position[1] = h;
    }

    return count;
}

uint32_t QueryTiles(BlockyBehaviorNode *data, Vector3 bounds[2], uint32_t size, MapTileQuery *buffer)
{
    Assert(data != nullptr && bounds != nullptr && buffer != nullptr);
    Assert(data->LoadedChunks != nullptr && data->LoadedChunkGridSize > 0);

    uint32_t check = 0b111111111;

    if (data->LoadedChunkGridSize >= 3)
    {
        if (bounds[0].X >= 0)
            check &= 0b110110110;
        if (bounds[1].X <  MAP_CHUNK_SIZE)
            check &= 0b011011011;
        if (bounds[0].Z >= 0)
            check &= 0b111111000;
        if (bounds[1].Z <  MAP_CHUNK_SIZE)
            check &= 0b000111111;
    }

    const uint32_t centerIndex = data->LoadedChunkGridSize * data->LoadedChunkGridSize / 2;

    uint32_t count = 0;

    for (uint32_t b = 0; b < 9; ++b)
    {
        int32_t x = (b % 3) - 1;
        int32_t y = (b / 3) - 1;
        if (check & (1 << b))
        {
            int32_t d[2] = { x * MAP_CHUNK_SIZE, y * MAP_CHUNK_SIZE };
            Vector3 offset = Vector3 { (float) x, 0.f, (float) y } * MAP_CHUNK_SIZE;
            Vector3 localBounds[2] = { bounds[0] - offset, bounds[1] - offset };
            MapChunk *chunk = data->LoadedChunks[centerIndex + y * data->LoadedChunkGridSize + x];

            count += queryChunkTiles(
                chunk,
                d,
                localBounds,
                size - count,
                buffer + count);
        }
    }

    return count;
}