### About

An example game using the engine.  
A certain block building game clone.  

### Running

Set the following environment variables when running:  

| Environment variable | Description                                                    |
|----------------------|----------------------------------------------------------------|
| KAME_TEXTURE_PATH_1  | Path to the tileset texture used to cover the blocks           |
| KAME_GRID_SIZE       | The quantity of chunks loaded around the player                |

### Chunk movement

Chunks are structures used to group tiles.  
The world is only loaded around the player.  
When moving between chunks, the game will only rebuild the chunks that have been paged in

Consider the following diagram of chunks around the player

        +---------+---------+---------+
        |         |         |         |
        |    0    |    1    |    2    |
        |         |         |         |
        +---------+---------+---------+
        |         |         |         |
        |    3    |    4    |    5    |
        |         |         |         |
        +---------+---------+---------+
        |         |         |         |
        |    6    |    7    |    8    |
        |         |         |         |
        +---------+---------+---------+

        (-1, 0) -> move chunks to opposite direction
                   rebuild the left column

        +---------+---------+---------+
        |         |         |         |
        |    X    |    0    |    1    |
        |         |         |         |
        +---------+---------+---------+
        |         |         |         |
        |    X    |    3    |    4    |
        |         |         |         |
        +---------+---------+---------+
        |         |         |         |
        |    X    |    6    |    7    |
        |         |         |         |
        +---------+---------+---------+

        (1, 1) -> move chunks to the opposite direction
                  rebuild the left column
                  rebuild the bottom row, except the leftmost cell
        +---------+---------+---------+
        |         |         |         |
        |    1    |    2    |    X    |
        |         |         |         |
        +---------+---------+---------+
        |         |         |         |
        |    4    |    5    |    X    |
        |         |         |         |
        +---------+---------+---------+
        |         |         |         |
        |    7    |    8    |    X    |
        |         |         |         |
        +---------+---------+---------+
        +---------+---------+---------+
        |         |         |         |
        |    X    |    X    |    X    |
        |         |         |         |
        +---------+---------+---------+
        |         |         |         |
        |    1    |    2    |    X    |
        |         |         |         |
        +---------+---------+---------+
        |         |         |         |
        |    4    |    5    |    X    |
        |         |         |         |
        +---------+---------+---------+

        render front to back for better performance
        the ideal way to do this is render in a spiral pattern
        +---------+---------+---------+---------+---------+
        |         |         |         |         |         |
        |    0    |    1    |    2    |    3    |    4    |
        |      4B |         |         |         |      4L |
        +---------+---------+---------+---------+---------+
        |         |         |         |         |         |
        |    5    |    6    |    7    |    8    |    9    |
        |         |      2B |         |      2L |         |
        +---------+---------+---------+---------+---------+
        |         |         |         |         |         |
        |   10    |   11    |   12    |   13    |   14    |
        |         |         |      1R |      1T |         |
        +---------+---------+---------+---------+---------+
        |         |         |         |         |         |
        |   15    |   16    |   17    |   18    |   19    |
        |         |      3R |         |         |      3T |
        +---------+---------+---------+---------+---------+
        |         |         |         |         |         |
        |   20    |   21    |   22    |   23    |   24    |
        |      5R |         |         |         |         | STOP
        +---------+---------+---------+---------+---------+

When the movement is larger than 1 unit, just rebuild everything from scratch
