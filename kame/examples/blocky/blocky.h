#pragma once

#include <kame/engine/platform.h>
#include <kame/engine/thread.h>

#define MAP_CHUNK_HEIGHT 128
#define MAP_CHUNK_SIZE   16
#define MAP_TILE_SIZE    1

enum BlockyLayers : uint64_t
{
    BlockyLayerMap     = 1,
    BlockyLayerMinimap = 2,
};

enum MapBiomeType : uint32_t
{
    MapBiomeTypeNone,
    MapBiomeTypeForest,
    MapBiomeTypeHills,
    MapBiomeTypeDesert,
    MapBiomeTypePlains,
};

enum MapTileType : uint32_t
{
    MapTileTypeNone,
    MapTileTypeBedrock,
    MapTileTypeDirt,
    MapTileTypeGrass,
    MapTileTypeMud,
    MapTileTypeSand,
    MapTileTypeStone,
    MapTileTypeCoal,
    MapTileTypeIron,
    MapTileTypeGold,
    MapTileTypeRedstone,
    MapTileTypeDiamond,
    MapTileTypeLapiz,
    MapTileTypeWood,
    MapTileTypeLeaf,
};

struct MapTile
{
    MapTileType Type;
};

struct MapBiome
{
    MapBiomeType Type;
    MapTileType  MainTile;
    float        MinHeight;
    float        MaxHeight;
};

struct MapChunk
{
    int32_t      X, Y;
    MapTile     *Tiles;
    DynamicMesh *Mesh;
    MeshNode    *Node;
    uint64_t     TaskId;
};

struct MapTileQuery : MapTile
{
    int32_t Position[3];
};

struct BlockyBehaviorNode : BehaviorNode
{
    SceneNode   *World;

    CameraNode  *PlayerCamera;
    MeshNode    *Player;
    SceneNode   *PlayerGimbal;
    Vector3      PlayerVelocity;
    int32_t      PlayerChunkX, PlayerChunkY;
    int          PlayerEnableClipping;
    int          PlayerAxisClipped[6];

    CameraNode  *MinimapCamera;
    MeshNode    *Minimap;

    int          ForceChunkUpdate;

    uint32_t     LoadedChunkGridSize;
    MapChunk   **LoadedChunks;

    Texture     *Tileset;
    Material    *ColorMaterial, *TextureMaterial, *MinimapMaterial;

    Handle       Random;
    Handle       ThreadPool;
};

int UpdateChunkMesh(MapChunk *chunk, TaskToken *taskToken);
int GenerateChunk(int32_t x, int32_t y, int initializeOnly, Out MapChunk *chunk, TaskToken *taskToken);

MapTile * GetTile(MapChunk *chunk, uint32_t x, uint32_t y, uint32_t h);
uint32_t QueryTiles(BlockyBehaviorNode *data, Vector3 bounds[2], uint32_t size, MapTileQuery *buffer);

void UpdateChunkGrid(BlockyBehaviorNode *data, int32_t dx, int32_t dy);