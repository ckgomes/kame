#include <stdio.h>

#include <blocky/blocky.h>
#include <kame/engine/thread.h>

#define KAME_MT 1

static MapChunk ** indexPGrid(uint32_t i, uint32_t j, uint32_t chunkGridSize, MapChunk **chunkGrid)
{
    return chunkGrid + j * chunkGridSize + i;
}
static MapChunk * indexGrid(uint32_t i, uint32_t j, uint32_t chunkGridSize, MapChunk **chunkGrid)
{
    return chunkGrid[j * chunkGridSize + i];
}

static uint32_t shiftLeft(uint32_t chunkGridSize, MapChunk **chunkGrid)
{
    for (uint32_t j = 0; j < chunkGridSize; ++j)
    {
        MapChunk *i0 = *indexPGrid(0, j, chunkGridSize, chunkGrid);
        for (uint32_t i = 1; i < chunkGridSize; ++i)
            *indexPGrid(i - 1, j, chunkGridSize, chunkGrid) = *indexPGrid(i, j, chunkGridSize, chunkGrid);
        *indexPGrid(chunkGridSize - 1, j, chunkGridSize, chunkGrid) = i0;
    }
    return chunkGridSize - 1;
}

static uint32_t shiftRight(uint32_t chunkGridSize, MapChunk **chunkGrid)
{
    for (uint32_t j = 0; j < chunkGridSize; ++j)
    {
        MapChunk *in = *indexPGrid(chunkGridSize - 1, j, chunkGridSize, chunkGrid);
        for (uint32_t i = chunkGridSize; --i > 0;)
            *indexPGrid(i, j, chunkGridSize, chunkGrid) = *indexPGrid(i - 1, j, chunkGridSize, chunkGrid);
        *indexPGrid(0, j, chunkGridSize, chunkGrid) = in;
    }
    return 0;
}

static uint32_t shiftDown(uint32_t chunkGridSize, MapChunk **chunkGrid)
{
    for (uint32_t i = 0; i < chunkGridSize; ++i)
    {
        MapChunk *jn = *indexPGrid(i, chunkGridSize - 1, chunkGridSize, chunkGrid);
        for (uint32_t j = chunkGridSize; --j > 0;)
            *indexPGrid(i, j, chunkGridSize, chunkGrid) = *indexPGrid(i, j - 1, chunkGridSize, chunkGrid);
        *indexPGrid(i, 0, chunkGridSize, chunkGrid) = jn;
    }
    return 0;
}

static uint32_t shiftUp(uint32_t chunkGridSize, MapChunk **chunkGrid)
{
    for (uint32_t i = 0; i < chunkGridSize; ++i)
    {
        MapChunk *j0 = *indexPGrid(i, 0, chunkGridSize, chunkGrid);
        for (uint32_t j = 1; j < chunkGridSize; ++j)
            *indexPGrid(i, j - 1, chunkGridSize, chunkGrid) = *indexPGrid(i, j, chunkGridSize, chunkGrid);
        *indexPGrid(i, chunkGridSize - 1, chunkGridSize, chunkGrid) = j0;
    }
    return chunkGridSize - 1;
}

struct UpdateChunkTaskData
{
    int32_t    X, Y;
    MapChunk  *Chunk;
};

static int updateChunkTask(uint32_t taskId, void *arg)
{
    UpdateChunkTaskData *data = (UpdateChunkTaskData *) arg;
    TaskToken taskToken = { taskId, &data->Chunk->TaskId };

    HandleCancellation(&taskToken);
    GenerateChunk(data->X, data->Y, 0, data->Chunk, &taskToken);
    HandleCancellation(&taskToken);
    UpdateChunkMesh(data->Chunk, &taskToken);
    HandleCancellation(&taskToken);
    Scene_SetEnabled(data->Chunk->Node, 0);

cancelled:
    FreeMemory(arg);
    return ErrNone;
}

static void dispatchUpdateChunk(BlockyBehaviorNode *data, MapChunk *chunk, uint32_t i, uint32_t j)
{
    int32_t x = data->PlayerChunkX + i - data->LoadedChunkGridSize / 2;
    int32_t y = data->PlayerChunkY + j - data->LoadedChunkGridSize / 2;

    #if KAME_MT
    UpdateChunkTaskData *d = NewObject(UpdateChunkTaskData);
    *d = { x, y, chunk };
    Scene_SetDisabled(chunk->Node, 0);
    QueueTask(data->ThreadPool, updateChunkTask, d, &d->Chunk->TaskId);
    #else
    GenerateChunk(x, y, 0, chunk, nullptr);
    UpdateChunkMesh(chunk, nullptr);
    Scene_SetEnabled(chunk->Node, 0);
    #endif
}

static void initializeChunk(BlockyBehaviorNode *data, uint32_t i, uint32_t j)
{
    MapChunk *chunk = indexGrid(i, j, data->LoadedChunkGridSize, data->LoadedChunks);
    dispatchUpdateChunk(data, chunk, i, j);
    int32_t x = i - data->LoadedChunkGridSize / 2;
    int32_t y = j - data->LoadedChunkGridSize / 2;
    chunk->Node->Position = Vector3 { (float) x, 0.f, (float) y } * MAP_CHUNK_SIZE;
    Scene_SetDirty(chunk->Node);
}

void UpdateChunkGrid(BlockyBehaviorNode *data, int32_t dx, int32_t dy)
{
    if ((dx == 0 && dy == 0) || dx > 1 || dx < -1 || dy > 1 || dy < -1) // rebuild
    {
        uint32_t stride = 1;
        uint32_t i = data->LoadedChunkGridSize / 2, j = i;
        for (uint32_t step = 0; step < data->LoadedChunkGridSize / 2; ++step)
        {
            for (uint32_t ie = i + stride; i < ie; ++i) // right
                initializeChunk(data, i, j);
            for (uint32_t je = j + stride; j < je; ++j) // up
                initializeChunk(data, i, j);
            stride += 1;
            for (uint32_t ie = i - stride; i > ie; --i) // left
                initializeChunk(data, i, j);
            for (uint32_t je = j - stride; j > je; --j) // down
                initializeChunk(data, i, j);
            stride += 1;
        }
        for (uint32_t i = 0; i < data->LoadedChunkGridSize; ++i)
            initializeChunk(data, i, 0);
    }
    else // incremental update
    {
        uint32_t ir = UINT32_MAX;
        uint32_t jr = UINT32_MAX;

        if (dx == -1)
            ir = shiftRight(data->LoadedChunkGridSize, data->LoadedChunks);
        else if (dx == 1)
            ir = shiftLeft(data->LoadedChunkGridSize, data->LoadedChunks);

        if (dy == -1)
            jr = shiftDown(data->LoadedChunkGridSize, data->LoadedChunks);
        else if (dy == 1)
            jr = shiftUp(data->LoadedChunkGridSize, data->LoadedChunks);

        for (uint32_t j = 0; j < data->LoadedChunkGridSize; ++j)
        for (uint32_t i = 0; i < data->LoadedChunkGridSize; ++i)
        {
            MapChunk *chunk = indexGrid(i, j, data->LoadedChunkGridSize, data->LoadedChunks);
            int32_t x = i - data->LoadedChunkGridSize / 2;
            int32_t y = j - data->LoadedChunkGridSize / 2;
            chunk->Node->Position = Vector3 { (float) x, 0.f, (float) y } * MAP_CHUNK_SIZE;
            Scene_SetDirty(chunk->Node);
        }

        if (ir != UINT32_MAX)
        {
            for (uint32_t j = 0; j < data->LoadedChunkGridSize; ++j)
            {
                MapChunk *chunk = indexGrid(ir, j, data->LoadedChunkGridSize, data->LoadedChunks);
                dispatchUpdateChunk(data, chunk, ir, j);
            }
        }

        if (jr != UINT32_MAX)
        {
            for (uint32_t i = 0; i < data->LoadedChunkGridSize; ++i)
            {
                if (i == ir)
                    continue;

                MapChunk *chunk = indexGrid(i, jr, data->LoadedChunkGridSize, data->LoadedChunks);
                dispatchUpdateChunk(data, chunk, i, jr);
            }
        }
    }
}