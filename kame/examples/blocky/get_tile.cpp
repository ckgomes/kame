#include <blocky/blocky.h>

MapTile * GetTile(MapChunk *chunk, uint32_t x, uint32_t y, uint32_t h)
{
    Assert(x < MAP_CHUNK_SIZE && y < MAP_CHUNK_SIZE && h < MAP_CHUNK_HEIGHT);
    return chunk->Tiles + (h * MAP_CHUNK_SIZE * MAP_CHUNK_SIZE) + (y * MAP_CHUNK_SIZE) + x;
}