#include <string.h>
#include <stdio.h>

#include <kame/engine/platform.h>
#include <kame/engine/keysyms.h>
#include <kame/engine/random.h>
#include <kame/engine/thread.h>

#include <blocky/blocky.h>

struct BlockyVertexBlock
{
    Matrix Model;
};
struct BlockyFragmentBlock
{
    PVector4 FogColor;
};

static void addFace(Mesh *mesh, uint32_t faceNumber, uint32_t v0, uint32_t v1, uint32_t v2)
{
    uint32_t *face = mesh->Indices + (3 * faceNumber);
    *face++ = v0;
    *face++ = v1;
    *face   = v2;
}

static Mesh * createCubeMesh()
{
    Mesh *mesh = NewObjectZ(Mesh);

    mesh->Name        = "cube";
    mesh->VertexCount = 8;

    mesh->Positions = NewObjects(PVector3, mesh->VertexCount);
    mesh->Positions[0] = { -0.5f, -0.5f, -0.5f };
    mesh->Positions[1] = { -0.5f, -0.5f,  0.5f };
    mesh->Positions[2] = { -0.5f,  0.5f, -0.5f };
    mesh->Positions[3] = { -0.5f,  0.5f,  0.5f };
    mesh->Positions[4] = {  0.5f, -0.5f, -0.5f };
    mesh->Positions[5] = {  0.5f, -0.5f,  0.5f };
    mesh->Positions[6] = {  0.5f,  0.5f, -0.5f };
    mesh->Positions[7] = {  0.5f,  0.5f,  0.5f };

    mesh->Colors = NewObjects(PVector4, mesh->VertexCount);
    mesh->Colors[0] = { 0.0f, 0.0f, 0.0f };
    mesh->Colors[1] = { 0.0f, 0.0f, 1.0f };
    mesh->Colors[2] = { 0.0f, 1.0f, 0.0f };
    mesh->Colors[3] = { 0.0f, 1.0f, 1.0f };
    mesh->Colors[4] = { 1.0f, 0.0f, 0.0f };
    mesh->Colors[5] = { 1.0f, 0.0f, 1.0f };
    mesh->Colors[6] = { 1.0f, 1.0f, 0.0f };
    mesh->Colors[7] = { 1.0f, 1.0f, 1.0f };

    mesh->TextureCoordinates = NewObjects(PVector2, mesh->VertexCount);
    mesh->TextureCoordinates[0] = { 0.0f, 0.5f };
    mesh->TextureCoordinates[1] = { 0.5f, 0.5f };
    mesh->TextureCoordinates[2] = { 0.5f, 0.0f };
    mesh->TextureCoordinates[3] = { 0.0f, 0.0f };
    mesh->TextureCoordinates[4] = { 0.5f, 1.0f };
    mesh->TextureCoordinates[5] = { 1.0f, 1.0f };
    mesh->TextureCoordinates[6] = { 1.0f, 0.5f };
    mesh->TextureCoordinates[7] = { 0.5f, 0.5f };

    mesh->IndexCount = 36;
    mesh->Indices    = NewObjects(uint32_t, mesh->IndexCount);

    addFace(mesh,  0, 1,5,3); // Left
    addFace(mesh,  1, 5,7,3);
    addFace(mesh,  2, 0,5,1); // Bottom
    addFace(mesh,  3, 0,4,5);
    addFace(mesh,  4, 0,2,4); // Right
    addFace(mesh,  5, 2,6,4);
    addFace(mesh,  6, 2,3,7); // Top
    addFace(mesh,  7, 2,7,6);
    addFace(mesh,  8, 7,5,4); // Front
    addFace(mesh,  9, 4,6,7);
    addFace(mesh, 10, 3,2,0); // Back
    addFace(mesh, 11, 3,0,1);

    return mesh;
}

static Mesh * createQuadMesh()
{
    Mesh *mesh = NewObjectZ(Mesh);

    mesh->Name        = "quad";
    mesh->VertexCount = 4;

    mesh->Positions = NewObjects(PVector3, mesh->VertexCount);
    mesh->Positions[0] = { -0.5f, -0.5f, 0.0f };
    mesh->Positions[1] = {  0.5f, -0.5f, 0.0f };
    mesh->Positions[2] = {  0.5f,  0.5f, 0.0f };
    mesh->Positions[3] = { -0.5f,  0.5f, 0.0f };

    mesh->Colors = NewObjects(PVector4, mesh->VertexCount);
    mesh->Colors[0] = { 0.0f, 1.0f, 1.0f };
    mesh->Colors[1] = { 1.0f, 0.0f, 1.0f };
    mesh->Colors[2] = { 1.0f, 1.0f, 0.0f };
    mesh->Colors[3] = { 1.0f, 1.0f, 1.0f };

    mesh->TextureCoordinates = NewObjects(PVector2, mesh->VertexCount);
    mesh->TextureCoordinates[0] = { 0.0f, 1.0f };
    mesh->TextureCoordinates[1] = { 1.0f, 1.0f };
    mesh->TextureCoordinates[2] = { 1.0f, 0.0f };
    mesh->TextureCoordinates[3] = { 0.0f, 0.0f };

    mesh->IndexCount = 6;
    mesh->Indices    = NewObjects(uint32_t, mesh->IndexCount);

    addFace(mesh, 0, 0,1,2);
    addFace(mesh, 1, 0,2,3);

    return mesh;
}

static DynamicMesh * createDynamicMesh()
{
    DynamicMesh *mesh = NewObjectZ(DynamicMesh);
    mesh->Name = "dynamicMesh";

    mesh->PositionCapacity = 128;
    mesh->Positions = NewObjects(PVector3, mesh->PositionCapacity);

    mesh->ColorCapacity = 128;
    mesh->Colors = NewObjects(PVector4, mesh->ColorCapacity);

    return mesh;
}

static MeshNode * createMeshNode(SceneNode *parent, Mesh *mesh, Material *material, Vector3 position)
{
    MeshNode *node = NewObjectZ(MeshNode);
    Texture *texture = material->Textures[0];

    node->Type     = SceneNodeTypeMesh;
    node->Flags    = SceneNodeFlagTransformDirty;
    node->Mesh     = mesh;
    node->Position = position;
    node->Scale    = { texture ? (float) texture->Bitmap.Width / texture->Bitmap.Height : 1, 1, 1 };
    node->Layers   = BlockyLayerMap;
    node->Material = material;

    Scene_SetNodeParent(node, parent);
    return node;
}

static CameraNode * createCameraNode(SceneNode *parent, Vector3 position, float depth, RenderTexture *renderTexture)
{
    CameraNode *node = NewObjectZ(CameraNode);

    node->Type        = SceneNodeTypeCamera;
    node->Flags       = (SceneNodeFlags) (SceneNodeFlagTransformDirty | SceneNodeFlagEnabled);
    node->Near        = 0.1f;
    node->Far         = node->Near + depth;
    node->FieldOfView = M_PIf / 3;
    node->Position    = position;
    node->Scale       = { 1, 1, 1 };
    node->Target      = renderTexture;

    Scene_SetNodeParent(node, parent);
    return node;
}

static SceneNode * createEmptyNode(SceneNode *parent, Vector3 position)
{
    SceneNode *node = NewObjectZ(SceneNode);

    node->Type     = SceneNodeTypeNone;
    node->Flags    = SceneNodeFlagTransformDirty;
    node->Position = position;
    node->Scale    = { 1, 1, 1 };

    Scene_SetNodeParent(node, parent);
    return node;
}

static Texture * createTexture(uint32_t index)
{
    char variableName[64];
    snprintf(variableName, ArraySize(variableName), "KAME_TEXTURE_PATH_%u", index);

    const char *texturePath = getenv(variableName);
    if (texturePath)
    {
        char *textureName;
        MemoryMappedFile textureFile;
        CheckedFatal(OpenMemoryMappedFile(texturePath, &textureFile), "Could not open file %s", texturePath);
        Texture *texture = NewObjectZ(Texture);
        texture->AssetPath = texturePath;
        texture->Name      = textureName = NewObjects(char, 32);
        snprintf(textureName, 32, "texture_%u", index);
        LoadBitmap((size_t) textureFile.End - (size_t) textureFile.Start, textureFile.Start, &texture->Bitmap);
        CheckedFatal(CloseMemoryMappedFile(&textureFile), "Could not close file %s", texturePath);
        return texture;
    }

    return nullptr;
}

static void updateChunks(BlockyBehaviorNode *data)
{
    Vector3 relativePosition = Floor(data->Player->Position / MAP_CHUNK_SIZE);
    int32_t dx = (int32_t) relativePosition.X;
    int32_t dy = (int32_t) relativePosition.Z;

    if (!data->ForceChunkUpdate && (dx == 0 && dy == 0))
        return;

    data->ForceChunkUpdate = 0;

    data->PlayerChunkX += dx;
    data->PlayerChunkY += dy;

    Log(LogTrace, "blocky", "moved to chunk [%3d:%3d]", data->PlayerChunkX, data->PlayerChunkY);
    UpdateChunkGrid(data, dx, dy);

    data->Player->Position.X -= dx * MAP_CHUNK_SIZE;
    data->Player->Position.Z -= dy * MAP_CHUNK_SIZE;
}

static void slideAndCollide(BlockyBehaviorNode *data, RootNode *scene)
{
    Vector3 motion = data->PlayerVelocity * scene->Platform->TimeStep;

    ArraySet(data->PlayerAxisClipped, 0);

    if (data->PlayerEnableClipping)
    {
        uint32_t axisList[] = { 1, 0, 2 };

        for (uint32_t axisIndex = 0; axisIndex < ArraySize(axisList); ++axisIndex)
        {
            const uint32_t axis = axisList[axisIndex];

            Vector3 bounds[2];
            Scene_GetAxisAlignedBounds(data->Player, bounds + 0, bounds + 1);

            for (int32_t c = axisIndex; c >= 0; --c)
            {
                bounds[0][axisList[c]] += motion[axisList[c]];
                bounds[1][axisList[c]] += motion[axisList[c]];
            }

            MapTileQuery tiles[64];
            const uint32_t tileCount = QueryTiles(data, bounds, ArraySize(tiles), tiles);

            if (tileCount == ArraySize(tiles))
                Log(LogWarn, "blocky", "query tile overflow");

            for (uint32_t tileIndex = 0; tileIndex < tileCount; ++tileIndex)
            {
                const MapTileQuery tile = tiles[tileIndex];
                if (tile.Type == MapTileTypeNone)
                    continue;

                if (motion[axis] < 0.f)
                {
                    const float minPenetration = (tile.Position[axis] + MAP_TILE_SIZE) - bounds[0][axis];
                    if (minPenetration > 0.f && minPenetration <= motion[axis] * -1.5f)
                    {
                        motion[axis] += minPenetration;
                        data->PlayerVelocity[axis] = 0.f;
                        data->PlayerAxisClipped[2 * axis] = 1;
                        break;
                    }
                }
                else if (motion[axis] > 0.f)
                {
                    const float maxPenetration = bounds[1][axis] - tile.Position[axis];
                    if (maxPenetration > 0.f && maxPenetration <= motion[axis] * 1.5f)
                    {
                        motion[axis] -= maxPenetration;
                        data->PlayerVelocity[axis] = 0.f;
                        data->PlayerAxisClipped[2 * axis + 1] = 1;
                        break;
                    }
                }
            }
        }
    }

    data->Player->Position += motion;
}

static void drawQuery(BlockyBehaviorNode *data, RootNode *scene)
{
    Vector3 bounds[2];
    Scene_GetAxisAlignedBounds(data->Player, bounds + 0, bounds + 1);

    MapTileQuery tiles[256];
    uint32_t tileCount = QueryTiles(data, bounds, ArraySize(tiles), tiles);

    for (uint32_t i = 0; i < tileCount; ++i)
    {
        Vector3 nb[2] = { {}, {1,1,1} };
        Vector3 offset = { (float) tiles[i].Position[0], (float) tiles[i].Position[1], (float) tiles[i].Position[2] };
        nb[0] += offset;
        nb[1] += offset;

        for (int j = 0; j < 2; ++j)
        for (int i = 0; i < 2; ++i)
        {
            scene->Platform->RendererDriver.DrawLine(
                scene->Platform,
                { nb[0].X, nb[i].Y, nb[j].Z },
                { nb[1].X, nb[i].Y, nb[j].Z },
                { 1.f, 1.f, 1.f, 1.f });
            scene->Platform->RendererDriver.DrawLine(
                scene->Platform,
                { nb[i].X, nb[0].Y, nb[j].Z },
                { nb[i].X, nb[1].Y, nb[j].Z },
                { 1.f, 1.f, 1.f, 1.f });
            scene->Platform->RendererDriver.DrawLine(
                scene->Platform,
                { nb[i].X, nb[j].Y, nb[0].Z },
                { nb[i].X, nb[j].Y, nb[1].Z },
                { 1.f, 1.f, 1.f, 1.f });
        }
    }

    for (int j = 0; j < 2; ++j)
    for (int i = 0; i < 2; ++i)
    {
        scene->Platform->RendererDriver.DrawLine(
            scene->Platform,
            { bounds[0].X, bounds[i].Y, bounds[j].Z },
            { bounds[1].X, bounds[i].Y, bounds[j].Z },
            { 0.f, 1.f, 1.f, 1.f });
        scene->Platform->RendererDriver.DrawLine(
            scene->Platform,
            { bounds[i].X, bounds[0].Y, bounds[j].Z },
            { bounds[i].X, bounds[1].Y, bounds[j].Z },
            { 0.f, 1.f, 1.f, 1.f });
        scene->Platform->RendererDriver.DrawLine(
            scene->Platform,
            { bounds[i].X, bounds[j].Y, bounds[0].Z },
            { bounds[i].X, bounds[j].Y, bounds[1].Z },
            { 0.f, 1.f, 1.f, 1.f });
    }
}

KameAPI BlockyBehaviorNode * Allocate()
{
    return NewObjectZ(BlockyBehaviorNode);
}

KameAPI int Load(SceneNode *node, RootNode *scene)
{
    Log(LogInfo, "blocky", "load behavior %p", node);

    BlockyBehaviorNode *data = (BlockyBehaviorNode *) node;

    data->Random = Random_New(42);
    Checked(CreateThreadPool(1, "ChunkWorker", &data->ThreadPool));

    data->Tileset = createTexture(1);

    data->ColorMaterial = NewObjectZ(Material);
    data->ColorMaterial->Flags = MaterialFlagModelMatrix;
    data->ColorMaterial->Name = "color";
    data->ColorMaterial->Shader = "color";
    data->ColorMaterial->CullMode = CullModeBack;

    data->TextureMaterial = NewObjectZ(Material);
    data->TextureMaterial->Flags = (MaterialFlags) (MaterialFlagModelMatrix | MaterialFlagTexture1);
    data->TextureMaterial->Name = "texture";
    data->TextureMaterial->Shader = "texture";
    data->TextureMaterial->CullMode = CullModeBack;
    data->TextureMaterial->Textures[0] = data->Tileset;
    data->TextureMaterial->SetFragmentParameters = [](SetMaterialParameters params, void *data) -> uint32_t
    {
        *((Vector4 *) data) = params.Camera->ClearColor;
        return sizeof(Vector4);
    };

    data->World = createEmptyNode(scene, {});
    data->World->Name = "world";

    data->Player = createMeshNode(data->World, createCubeMesh(), data->ColorMaterial, { 0.f, 68.f, 0.f });
    data->Player->Scale = { 0.5f, 1.f, 0.5f };
    data->Player->Name = "player";
    data->Player->Bounds.Extent = { 1.f, 1.f, 1.f };
    data->PlayerEnableClipping = 0;
    Scene_SetEnabled(data->Player, 0);

    data->PlayerGimbal = createEmptyNode(data->Player, { });
    data->PlayerGimbal->Name = "player-gimbal";

    const char *chunkGridSize = getenv("KAME_GRID_SIZE");
    data->LoadedChunkGridSize = (chunkGridSize ? atoi(chunkGridSize) : 1) * 2 + 1;
    if (data->LoadedChunkGridSize == 0)
        data->LoadedChunkGridSize = 3;

    data->LoadedChunks = NewObjects(MapChunk *, data->LoadedChunkGridSize * data->LoadedChunkGridSize);

    for (uint32_t j = 0; j < data->LoadedChunkGridSize; ++j)
    for (uint32_t i = 0; i < data->LoadedChunkGridSize; ++i)
    {
        uint32_t index = j * data->LoadedChunkGridSize + i;
        MapChunk *chunk = data->LoadedChunks[index] = NewObjectZ(MapChunk);
        chunk->Mesh = NewObjectZ(DynamicMesh);
        chunk->Node = createMeshNode(data->World, chunk->Mesh, data->TextureMaterial, {});
        chunk->Node->Name = chunk->Mesh->Name;
        chunk->Node->Bounds.Extent = { MAP_CHUNK_SIZE, MAP_CHUNK_HEIGHT, MAP_CHUNK_SIZE };
        chunk->Node->Bounds.Offset = { 0.5f * MAP_CHUNK_SIZE, MAP_CHUNK_HEIGHT * 0.5f, 0.5f * MAP_CHUNK_SIZE };
    }

    data->ForceChunkUpdate = 1;
    updateChunks(data);

    data->PlayerCamera = createCameraNode(data->PlayerGimbal, { 0, .5f, 5.f }, data->LoadedChunkGridSize / 2 * MAP_CHUNK_SIZE, nullptr);
    data->PlayerCamera->ClearColor   = { .6f, .6f, .6f, 1.f };
    data->PlayerCamera->Name         = "player.cam";
    data->PlayerCamera->Order        = 20;
    data->PlayerCamera->RenderLayers = BlockyLayerMap;

    RenderTexture *renderTexture = NewObjectZ(RenderTexture);
    renderTexture->Name          = "minimap.cam";
    renderTexture->Flags         = TextureFlagFramebuffer;
    renderTexture->Bitmap.Width  = 400;
    renderTexture->Bitmap.Height = 400;

    data->MinimapMaterial = NewObjectZ(Material);
    data->MinimapMaterial->Flags = (MaterialFlags) (MaterialFlagModelMatrix | MaterialFlagTexture1);
    data->MinimapMaterial->Name = "minimap";
    data->MinimapMaterial->Shader = "texture";
    data->MinimapMaterial->Textures[0] = renderTexture;

    data->MinimapCamera = createCameraNode(data->Player, { 0, 200, 0 }, 350, renderTexture);
    data->MinimapCamera->Rotation.X = -M_PI_2f32;
    data->MinimapCamera->ClearColor = { 1.0f, .1f, .1f, 1.f };
    data->MinimapCamera->Name       = renderTexture->Name;
    data->MinimapCamera->Order      = 10;

    data->Minimap = createMeshNode(data->PlayerCamera, createQuadMesh(), data->MinimapMaterial, { 1, 0, -2.5f });
    data->Minimap->Name   = "minimap";
    data->Minimap->Scale  = { 1, 0.5f, 1 };
    data->Minimap->Flags  = SceneNodeFlagEnabled;
    data->Minimap->Layers = BlockyLayerMap;

    scene->Platform->WindowDriver.SetTitle(scene->Platform, "blocky");

    return ErrNone;
}

KameAPI int Tick(SceneNode *node, RootNode *scene)
{
    BlockyBehaviorNode *data = (BlockyBehaviorNode *) node;

    const float motion = scene->Platform->KeyboardState[KeySymLShift] ? 500.f : 50.f;
    const float angularMotion = scene->Platform->TimeStep;

    Vector4 moment = { 0.f, 0.f, 0.f };

    if (scene->Platform->KeyboardState[KeySymR] && !scene->Platform->PreviousKeyboardState[KeySymR])
        data->ForceChunkUpdate = 1;
    if (scene->Platform->KeyboardState[KeySymP] && !scene->Platform->PreviousKeyboardState[KeySymP])
        data->PlayerEnableClipping = !data->PlayerEnableClipping;

    if (data->PlayerEnableClipping)
    {
        moment.Y -= 70.f;
        if (data->PlayerAxisClipped[2] && data->PlayerVelocity.Y <= 0.f && scene->Platform->KeyboardState[KeySymSpace])
            moment.Y += 1400.f;
    }
    else
    {
        if (scene->Platform->KeyboardState[KeySymC    ]) moment.Y -= motion;
        if (scene->Platform->KeyboardState[KeySymSpace]) moment.Y += motion;
    }

    if (scene->Platform->KeyboardState[KeySymA    ]) moment.X -= motion;
    if (scene->Platform->KeyboardState[KeySymD    ]) moment.X += motion;
    if (scene->Platform->KeyboardState[KeySymW    ]) moment.Z -= motion;
    if (scene->Platform->KeyboardState[KeySymS    ]) moment.Z += motion;

    if (scene->Platform->KeyboardState[KeySymLeft ]) data->Player->Rotation.Y -= angularMotion;
    if (scene->Platform->KeyboardState[KeySymRight]) data->Player->Rotation.Y += angularMotion;
    if (scene->Platform->KeyboardState[KeySym1    ]) data->PlayerCamera->Position.Z += angularMotion;
    if (scene->Platform->KeyboardState[KeySym2    ]) data->PlayerCamera->Position.Z -= angularMotion;
    if (scene->Platform->KeyboardState[KeySymUp   ]) data->PlayerGimbal->Rotation.X += angularMotion;
    if (scene->Platform->KeyboardState[KeySymDown ]) data->PlayerGimbal->Rotation.X -= angularMotion;
    if (scene->Platform->KeyboardState[KeySymQ    ]) data->PlayerGimbal->Rotation.Z += angularMotion;
    if (scene->Platform->KeyboardState[KeySymE    ]) data->PlayerGimbal->Rotation.Z -= angularMotion;

    Matrix rotation;
    RotateMatrix(data->Player->Rotation, &rotation);
    moment = Transform(moment * scene->Platform->TimeStep, &rotation);

    data->PlayerVelocity += { moment.X, moment.Y, moment.Z };
    data->PlayerVelocity -= 5.f * data->PlayerVelocity * scene->Platform->TimeStep;
    slideAndCollide(data, scene);

    updateChunks(data);

    Scene_SetDirty(data->Player);

    if (data->Player)
        Scene_DrawBounds(scene->Platform, data->Player, { 0.f, 0.f, 1.f });

    Scene_DrawBounds(
        scene->Platform,
        data->LoadedChunks[data->LoadedChunkGridSize * data->LoadedChunkGridSize / 2]->Node,
        { 1.f, 1.f, 0.f });

    //drawQuery(data, scene);

    return ErrNone;
}