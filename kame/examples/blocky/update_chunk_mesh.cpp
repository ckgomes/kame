#include <blocky/blocky.h>

struct TilesetIndex { uint32_t X, Y; };

static TilesetIndex indexTileset(MapTileType type)
{
    switch (type)
    {
        case MapTileTypeBedrock: return {  1,  1 };
        case MapTileTypeDirt:    return {  2,  0 };
        case MapTileTypeGrass:   return { 12, 12 };
        case MapTileTypeLeaf:    return {  3,  3 };
        case MapTileTypeMud:     return { 14,  4 };
        case MapTileTypeSand:    return {  2,  1 };
        case MapTileTypeStone:   return {  1,  0 };
        case MapTileTypeWood:    return {  4,  1 };
        case MapTileTypeCoal:    return {  2,  2 };
        case MapTileTypeIron:    return {  1,  2 };
        case MapTileTypeGold:    return {  0,  2 };
        case MapTileTypeDiamond: return {  2,  3 };
    }

    return { 11, 11 };
}

static void addNormals(DynamicMesh *mesh, PVector3 normal)
{
    mesh->Normals[mesh->VertexCount    ] = normal;
    mesh->Normals[mesh->VertexCount + 1] = normal;
    mesh->Normals[mesh->VertexCount + 2] = normal;
    mesh->Normals[mesh->VertexCount + 3] = normal;
}

static void addTextureCoordinates(DynamicMesh *mesh, MapTileType type, int side)
{
    TilesetIndex tileIndex = indexTileset(type);

    if (type == MapTileTypeGrass)
    {
        if (side == 1) tileIndex = { 3, 0 };
        if (side == 2) tileIndex = indexTileset(MapTileTypeDirt);
    }

    PVector2 textureCoordinateStep = PVector2 { 1.f, 1.f } / 16.f;
    PVector2 textureCoordinate = PVector2 { (float) tileIndex.X, (float) tileIndex.Y } * textureCoordinateStep;
    mesh->TextureCoordinates[mesh->VertexCount    ] = textureCoordinate;
    mesh->TextureCoordinates[mesh->VertexCount + 1] = textureCoordinate + PVector2 { textureCoordinateStep.X, 0.f };
    mesh->TextureCoordinates[mesh->VertexCount + 2] = textureCoordinate + PVector2 { 0.f, textureCoordinateStep.Y };
    mesh->TextureCoordinates[mesh->VertexCount + 3] = textureCoordinate + PVector2 { textureCoordinateStep.X, textureCoordinateStep.Y };
}

int UpdateChunkMesh(MapChunk *chunk, TaskToken *taskToken)
{
    // create enough data to fit the worst case
    if (!chunk->Mesh->Positions)
    {
        chunk->Mesh->IndexCapacity = MAP_CHUNK_HEIGHT * MAP_CHUNK_SIZE * MAP_CHUNK_SIZE * 36;
        chunk->Mesh->Indices       = NewObjects(uint32_t, chunk->Mesh->IndexCapacity);

        chunk->Mesh->PositionCapacity = MAP_CHUNK_HEIGHT * MAP_CHUNK_SIZE * MAP_CHUNK_SIZE * 24;
        chunk->Mesh->Positions        = NewObjects(PVector3, chunk->Mesh->PositionCapacity);

        chunk->Mesh->NormalCapacity = chunk->Mesh->PositionCapacity;
        chunk->Mesh->Normals        = NewObjects(PVector3, chunk->Mesh->NormalCapacity);

        chunk->Mesh->TextureCoordinateCapacity = chunk->Mesh->PositionCapacity;
        chunk->Mesh->TextureCoordinates        = NewObjects(PVector2, chunk->Mesh->TextureCoordinateCapacity);
    }

    chunk->Mesh->VertexCount = 0;
    chunk->Mesh->IndexCount = 0;

    for (uint32_t ih = 0; ih < MAP_CHUNK_HEIGHT; ++ih)
    {
        CheckCancellation(taskToken);

        for (uint32_t iy = 0; iy < MAP_CHUNK_SIZE  ; ++iy)
        for (uint32_t ix = 0; ix < MAP_CHUNK_SIZE  ; ++ix)
        {
            const float xMin = (float) ix, xMax = xMin + MAP_TILE_SIZE;
            const float zMin = (float) iy, zMax = zMin + MAP_TILE_SIZE;
            const float yMin = (float) ih, yMax = yMin + MAP_TILE_SIZE;

            MapTile *tile = GetTile(chunk, ix, iy, ih);

            if (tile->Type == MapTileTypeNone)
                continue;

            if (ih == 0 || GetTile(chunk, ix, iy, ih - 1)->Type == MapTileTypeNone) // bottom
            {
                uint32_t baseIndex = chunk->Mesh->VertexCount;
                chunk->Mesh->Positions[chunk->Mesh->VertexCount    ] = { xMin, yMin, zMin };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 1] = { xMin, yMin, zMax };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 2] = { xMax, yMin, zMin };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 3] = { xMax, yMin, zMax };
                addNormals(chunk->Mesh, { 0, -1, 0 });
                addTextureCoordinates(chunk->Mesh, tile->Type, 2);
                chunk->Mesh->VertexCount += 4;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount    ] = baseIndex + 3;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 1] = baseIndex + 1;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 2] = baseIndex;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 3] = baseIndex;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 4] = baseIndex + 2;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 5] = baseIndex + 3;
                chunk->Mesh->IndexCount += 6;
            }

            if (ih == MAP_CHUNK_HEIGHT - 1 || GetTile(chunk, ix, iy, ih + 1)->Type == MapTileTypeNone) // top
            {
                uint32_t baseIndex = chunk->Mesh->VertexCount;
                chunk->Mesh->Positions[chunk->Mesh->VertexCount    ] = { xMin, yMax, zMin };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 1] = { xMin, yMax, zMax };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 2] = { xMax, yMax, zMin };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 3] = { xMax, yMax, zMax };
                addNormals(chunk->Mesh, { 0, 1, 0 });
                addTextureCoordinates(chunk->Mesh, tile->Type, 0);
                chunk->Mesh->VertexCount += 4;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount    ] = baseIndex;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 1] = baseIndex + 1;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 2] = baseIndex + 3;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 3] = baseIndex + 3;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 4] = baseIndex + 2;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 5] = baseIndex;
                chunk->Mesh->IndexCount += 6;
            }

            if (iy == 0 || GetTile(chunk, ix, iy - 1, ih)->Type == MapTileTypeNone) // front
            {
                uint32_t baseIndex = chunk->Mesh->VertexCount;
                chunk->Mesh->Positions[chunk->Mesh->VertexCount    ] = { xMin, yMax, zMin };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 1] = { xMax, yMax, zMin };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 2] = { xMin, yMin, zMin };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 3] = { xMax, yMin, zMin };
                addNormals(chunk->Mesh, { 0, 0, -1 });
                addTextureCoordinates(chunk->Mesh, tile->Type, 1);
                chunk->Mesh->VertexCount += 4;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount    ] = baseIndex + 2;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 1] = baseIndex;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 2] = baseIndex + 1;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 3] = baseIndex + 1;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 4] = baseIndex + 3;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 5] = baseIndex + 2;
                chunk->Mesh->IndexCount += 6;
            }

            if (ix == 0 || GetTile(chunk, ix - 1, iy, ih)->Type == MapTileTypeNone) // left
            {
                uint32_t baseIndex = chunk->Mesh->VertexCount;
                chunk->Mesh->Positions[chunk->Mesh->VertexCount    ] = { xMin, yMax, zMax };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 1] = { xMin, yMax, zMin };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 2] = { xMin, yMin, zMax };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 3] = { xMin, yMin, zMin };
                addNormals(chunk->Mesh, { -1, 0, 0 });
                addTextureCoordinates(chunk->Mesh, tile->Type, 1);
                chunk->Mesh->VertexCount += 4;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount    ] = baseIndex + 3;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 1] = baseIndex + 2;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 2] = baseIndex;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 3] = baseIndex;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 4] = baseIndex + 1;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 5] = baseIndex + 3;
                chunk->Mesh->IndexCount += 6;
            }

            if (iy == MAP_CHUNK_SIZE - 1 || GetTile(chunk, ix, iy + 1, ih)->Type == MapTileTypeNone) // back
            {
                uint32_t baseIndex = chunk->Mesh->VertexCount;
                chunk->Mesh->Positions[chunk->Mesh->VertexCount    ] = { xMax, yMax, zMax };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 1] = { xMin, yMax, zMax };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 2] = { xMax, yMin, zMax };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 3] = { xMin, yMin, zMax };
                addNormals(chunk->Mesh, { 0, 0, 1 });
                addTextureCoordinates(chunk->Mesh, tile->Type, 1);
                chunk->Mesh->VertexCount += 4;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount    ] = baseIndex;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 1] = baseIndex + 1;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 2] = baseIndex + 3;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 3] = baseIndex + 3;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 4] = baseIndex + 2;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 5] = baseIndex;
                chunk->Mesh->IndexCount += 6;
            }

            if (ix == MAP_CHUNK_SIZE - 1 || GetTile(chunk, ix + 1, iy, ih)->Type == MapTileTypeNone) // right
            {
                uint32_t baseIndex = chunk->Mesh->VertexCount;
                chunk->Mesh->Positions[chunk->Mesh->VertexCount    ] = { xMax, yMax, zMax };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 1] = { xMax, yMax, zMin };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 2] = { xMax, yMin, zMax };
                chunk->Mesh->Positions[chunk->Mesh->VertexCount + 3] = { xMax, yMin, zMin };
                addNormals(chunk->Mesh, { 1, 0, 0 });
                addTextureCoordinates(chunk->Mesh, tile->Type, 1);
                chunk->Mesh->VertexCount += 4;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount    ] = baseIndex;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 1] = baseIndex + 2;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 2] = baseIndex + 3;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 3] = baseIndex + 3;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 4] = baseIndex + 1;
                chunk->Mesh->Indices[chunk->Mesh->IndexCount + 5] = baseIndex;
                chunk->Mesh->IndexCount += 6;
            }
        }
    }

    if (chunk->Mesh->VertexCount > chunk->Mesh->PositionCapacity || chunk->Mesh->IndexCount > chunk->Mesh->IndexCapacity)
        Log(LogFatal, "blocky", "mesh generation buffer overflow");

    chunk->Mesh->Flags = (MeshFlags) (chunk->Mesh->Flags | MeshFlagDirty);
    return ErrNone;
}