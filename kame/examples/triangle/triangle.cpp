#include <stdio.h>
#include <kame/engine/platform.h>

struct TriangleBehaviorNode : BehaviorNode
{
    MeshNode   *Triangle;
    CameraNode *Camera;
};

static Mesh * createTriangleMesh()
{
    Mesh *mesh = NewObjectZ(Mesh);

    mesh->Name        = "triangle";
    mesh->VertexCount = 3;

    mesh->Positions = NewObjects(PVector3, mesh->VertexCount);
    mesh->Positions[0] = { -0.5f, -0.5f, 0.0f };
    mesh->Positions[1] = {  0.5f, -0.5f, 0.0f };
    mesh->Positions[2] = {  0.0f,  0.5f, 0.0f };

    mesh->Colors = NewObjects(PVector4, mesh->VertexCount);
    mesh->Colors[0] = { 1.0f, 0.0f, 0.0f };
    mesh->Colors[1] = { 0.0f, 1.0f, 0.0f };
    mesh->Colors[2] = { 0.0f, 0.0f, 1.0f };

    mesh->TextureCoordinates = NewObjects(PVector2, mesh->VertexCount);
    mesh->TextureCoordinates[0] = { 0.0f, 1.0f };
    mesh->TextureCoordinates[1] = { 1.0f, 1.0f };
    mesh->TextureCoordinates[2] = { 0.5f, 0.0f };

    mesh->IndexCount = 6;
    mesh->Indices = NewObjects(uint32_t, mesh->IndexCount);
    mesh->Indices[0] = 0;
    mesh->Indices[1] = 1;
    mesh->Indices[2] = 2;
    mesh->Indices[3] = 2;
    mesh->Indices[4] = 1;
    mesh->Indices[5] = 0;

    return mesh;
}

static MeshNode * createMeshNode(SceneNode *parent, Mesh *mesh, Texture *texture, Vector3 position)
{
    MeshNode *node = NewObjectZ(MeshNode);

    node->Type     = SceneNodeTypeMesh;
    node->Flags    = SceneNodeFlagEnabled;
    node->Mesh     = mesh;
    node->Position = position;
    node->Scale    = { texture ? (float) texture->Bitmap.Width / texture->Bitmap.Height : 1, 1, 1 };

    node->Material = NewObjectZ(Material);
    node->Material->Flags = MaterialFlagModelMatrix;
    node->Material->Textures[0] = texture;
    node->Material->Shader      = texture ? "texture" : "color";
    node->Material->CullMode    = CullModeBack;

    if (texture)
        SetFlags(node->Material->Flags, MaterialFlagTexture1);

    Scene_SetNodeParent(node, parent);
    return node;
}

static CameraNode * createCameraNode(SceneNode *parent, Vector3 position, float depth)
{
    CameraNode *node = NewObjectZ(CameraNode);

    node->Type        = SceneNodeTypeCamera;
    node->Flags       = SceneNodeFlagEnabled;
    node->Near        = 0.1f;
    node->Far         = node->Near + depth;
    node->FieldOfView = M_PIf / 3;
    node->Position    = position;
    node->Scale       = { 1, 1, 1 };

    Scene_SetNodeParent(node, parent);
    return node;
}

KameAPI TriangleBehaviorNode * Allocate()
{
    return NewObjectZ(TriangleBehaviorNode);
}

KameAPI int Load(TriangleBehaviorNode *node, RootNode *scene)
{
    Mesh *triangle  = createTriangleMesh();
    node->Triangle  = createMeshNode(scene, triangle, nullptr, { 0, 0, -2.5f });

    node->Camera = createCameraNode(
        scene,
        { 0.f, 0.1f, 0.f },
        100.f);
    node->Camera->Name = "camera";
    node->Camera->Order = 2;

    node->Camera->ClearColor = { 0.2f, 0.2f, 0.3f, 1.f };

    scene->Platform->WindowDriver.SetTitle(scene->Platform, "triangle");

    return ErrNone;
}

KameAPI int Tick(TriangleBehaviorNode *node, RootNode *scene)
{
    node->Triangle->Rotation.Y += scene->Platform->TimeStep;
    Scene_SetDirty(node->Triangle);

    return ErrNone;
}