#include <stdio.h>
#include <kame/engine/platform.h>

struct RenderTextureBehavior : BehaviorNode
{
    CameraNode  *Camera;
    SceneNode   *CameraGimbal;

    MeshNode    *Cubes[3];

    MeshNode    *Casters[8];
    CameraNode  *CasterCameras[ArraySize(Casters)];
};

static void addFace(Mesh *mesh, uint32_t faceNumber, uint32_t v0, uint32_t v1, uint32_t v2)
{
    uint32_t *face = mesh->Indices + (3 * faceNumber);
    *face++ = v0;
    *face++ = v1;
    *face   = v2;
}

static Mesh * createCubeMesh()
{
    Mesh *mesh = NewObjectZ(Mesh);

    mesh->Name        = "cube";
    mesh->VertexCount = 8;

    mesh->Positions = NewObjects(PVector3, mesh->VertexCount);
    mesh->Positions[0] = { -0.5f, -0.5f, -0.5f };
    mesh->Positions[1] = { -0.5f, -0.5f,  0.5f };
    mesh->Positions[2] = { -0.5f,  0.5f, -0.5f };
    mesh->Positions[3] = { -0.5f,  0.5f,  0.5f };
    mesh->Positions[4] = {  0.5f, -0.5f, -0.5f };
    mesh->Positions[5] = {  0.5f, -0.5f,  0.5f };
    mesh->Positions[6] = {  0.5f,  0.5f, -0.5f };
    mesh->Positions[7] = {  0.5f,  0.5f,  0.5f };

    mesh->Colors = NewObjects(PVector4, mesh->VertexCount);
    mesh->Colors[0] = { 0.0f, 0.0f, 0.0f };
    mesh->Colors[1] = { 0.0f, 0.0f, 1.0f };
    mesh->Colors[2] = { 0.0f, 1.0f, 0.0f };
    mesh->Colors[3] = { 0.0f, 1.0f, 1.0f };
    mesh->Colors[4] = { 1.0f, 0.0f, 0.0f };
    mesh->Colors[5] = { 1.0f, 0.0f, 1.0f };
    mesh->Colors[6] = { 1.0f, 1.0f, 0.0f };
    mesh->Colors[7] = { 1.0f, 1.0f, 1.0f };

    mesh->IndexCount = 36;
    mesh->Indices    = NewObjects(uint32_t, mesh->IndexCount);

    addFace(mesh,  0, 1,5,3); // Left
    addFace(mesh,  1, 5,7,3);
    addFace(mesh,  2, 0,5,1); // Bottom
    addFace(mesh,  3, 0,4,5);
    addFace(mesh,  4, 0,2,4); // Right
    addFace(mesh,  5, 2,6,4);
    addFace(mesh,  6, 2,3,7); // Top
    addFace(mesh,  7, 2,7,6);
    addFace(mesh,  8, 7,5,4); // Front
    addFace(mesh,  9, 4,6,7);
    addFace(mesh, 10, 3,2,0); // Back
    addFace(mesh, 11, 3,0,1);

    return mesh;
}

static Mesh * createQuadMesh()
{
    Mesh *mesh = NewObjectZ(Mesh);

    mesh->Name        = "quad";
    mesh->VertexCount = 4;

    mesh->Positions = NewObjects(PVector3, mesh->VertexCount);
    mesh->Positions[0] = { -0.5f, -0.5f, 0.0f };
    mesh->Positions[1] = {  0.5f, -0.5f, 0.0f };
    mesh->Positions[2] = {  0.5f,  0.5f, 0.0f };
    mesh->Positions[3] = { -0.5f,  0.5f, 0.0f };

    mesh->Colors = NewObjects(PVector4, mesh->VertexCount);
    mesh->Colors[0] = { 0.0f, 1.0f, 1.0f };
    mesh->Colors[1] = { 1.0f, 0.0f, 1.0f };
    mesh->Colors[2] = { 1.0f, 1.0f, 0.0f };
    mesh->Colors[3] = { 1.0f, 1.0f, 1.0f };

    mesh->TextureCoordinates = NewObjects(PVector2, mesh->VertexCount);
    mesh->TextureCoordinates[0] = { 0.0f, 1.0f };
    mesh->TextureCoordinates[1] = { 1.0f, 1.0f };
    mesh->TextureCoordinates[2] = { 1.0f, 0.0f };
    mesh->TextureCoordinates[3] = { 0.0f, 0.0f };

    mesh->IndexCount = 6;
    mesh->Indices    = NewObjects(uint32_t, mesh->IndexCount);

    addFace(mesh, 0, 0,1,2);
    addFace(mesh, 1, 0,2,3);

    return mesh;
}

static SceneNode * createEmptyNode(SceneNode *parent, Vector3 position)
{
    SceneNode *node = NewObjectZ(SceneNode);

    node->Type     = SceneNodeTypeNone;
    node->Flags    = SceneNodeFlagTransformDirty;
    node->Position = position;
    node->Scale    = { 1, 1, 1 };

    Scene_SetNodeParent(node, parent);
    return node;
}

static MeshNode * createMeshNode(SceneNode *parent, Mesh *mesh, Material *material, Vector3 position)
{
    MeshNode *node = NewObjectZ(MeshNode);
    Texture *texture = material ? material->Textures[0] : nullptr;

    node->Type     = SceneNodeTypeMesh;
    node->Flags    = SceneNodeFlagEnabled;
    node->Mesh     = mesh;
    node->Position = position;
    node->Scale    = { texture ? (float) texture->Bitmap.Width / texture->Bitmap.Height : 1, 1, 1 };
    node->Layers   = 1;

    Scene_SetNodeParent(node, parent);
    return node;
}

static CameraNode * createCameraNode(SceneNode *parent, Vector3 position, float depth)
{
    CameraNode *node = NewObjectZ(CameraNode);

    node->Type        = SceneNodeTypeCamera;
    node->Flags       = SceneNodeFlagEnabled;
    node->Near        = 0.1f;
    node->Far         = node->Near + depth;
    node->FieldOfView = M_PIf / 3;
    node->Position    = position;
    node->Scale       = { 1, 1, 1 };

    Scene_SetNodeParent(node, parent);
    return node;
}

static RenderTexture * createRenderTexture(const char *name)
{
    RenderTexture *renderTexture = NewObjectZ(RenderTexture);

    renderTexture->Name          = name;
    renderTexture->Flags         = TextureFlagFramebuffer;
    renderTexture->Bitmap.Width  = 800;
    renderTexture->Bitmap.Height = 800;

    return renderTexture;
}

KameAPI RenderTextureBehavior * Allocate()
{
    return NewObjectZ(RenderTextureBehavior);
}

KameAPI int Load(RenderTextureBehavior *node, RootNode *scene)
{
    Mesh *cube = createCubeMesh();
    Mesh *quad = createQuadMesh();

    Material *cubeMaterial = NewObjectZ(Material);
    cubeMaterial->Shader = "color";
    cubeMaterial->Flags = MaterialFlagModelMatrix;

    for (uint32_t i = 0; i < ArraySize(node->Cubes); ++i)
    {
        node->Cubes[i] = createMeshNode(scene, cube, nullptr, { });
        node->Cubes[i]->Scale    = { .5f, .5f, .5f };
        node->Cubes[i]->Material = cubeMaterial;
        node->Cubes[i]->Name     = "cube";
        sincosf(2 * M_PIf * i / ArraySize(node->Cubes), &node->Cubes[i]->Position.X, &node->Cubes[i]->Position.Z);
    }

    for (uint32_t i = 0; i < ArraySize(node->Casters); ++i)
    {
        node->Casters[i] = createMeshNode(scene, quad, nullptr, { });
        float t = 2 * M_PIf * i / ArraySize(node->Casters);
        sincosf(t, &node->Casters[i]->Position.X, &node->Casters[i]->Position.Z);
        node->Casters[i]->Rotation.Y = M_PIf - t;
        node->Casters[i]->Position *= 6.f;
        node->Casters[i]->Scale *= 4.75f;

        char *casterName = NewObjects(char, 16);
        snprintf(casterName, 16, "caster.cam %u", i);
        node->CasterCameras[i] = createCameraNode(node->Casters[i], { 0, 0, -1 }, 100.f);
        node->CasterCameras[i]->Rotation.Y = M_PIf;
        node->CasterCameras[i]->Name = casterName;
        node->CasterCameras[i]->Target = createRenderTexture(casterName);
        node->CasterCameras[i]->Order = i + 1;
        node->CasterCameras[i]->ClearColor = { 0.f, 0.f, 1.f, 1.f };

        for (uint32_t c = 0; c < 3; ++c)
            node->CasterCameras[i]->ClearColor[c] = fmod((i + 3.33f) * (c + 7.77f), 1.0f);

        node->Casters[i]->Material = NewObjectZ(Material);
        node->Casters[i]->Material->Flags = (MaterialFlags) (MaterialFlagModelMatrix | MaterialFlagTexture1);
        node->Casters[i]->Material->Shader = "texture";
        node->Casters[i]->Material->Textures[0] = node->CasterCameras[i]->Target;
    }

    node->CameraGimbal = createEmptyNode(scene, {});
    node->CameraGimbal->Rotation.X = M_PIf * -0.125f;
    node->Camera = createCameraNode(node->CameraGimbal, { 0, 0, 4 }, 100.f);
    node->Camera->ClearColor = { .2f, .2f, .2f, 1.f };
    node->Camera->Order = ArraySize(node->Casters) + 1;
    //Scene_SetNodeParent(node->Camera, node->Casters[0]);
    return ErrNone;
}

KameAPI int Tick(RenderTextureBehavior *node, RootNode *scene)
{
    node->CameraGimbal->Rotation.Y += scene->Platform->TimeStep * 0.2f;
    Scene_SetDirty(node->CameraGimbal);

    for (uint32_t i = 0; i < ArraySize(node->Cubes); ++i)
    {
        node->Cubes[i]->Position.Y = sinf(scene->Platform->TimeStep * scene->Platform->TickNumber + i);
        Scene_SetDirty(node->Cubes[i]);
    }

    return ErrNone;
}