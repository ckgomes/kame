#include <kame/engine/platform.h>
#include <kame/engine/font.h>

struct RenderTextNode : BehaviorNode
{
    CameraNode *Camera;
    MeshNode   *Quad;
    BitmapFont *Font;
};

static void addFace(Mesh *mesh, uint32_t faceNumber, uint32_t v0, uint32_t v1, uint32_t v2)
{
    uint32_t *face = mesh->Indices + (3 * faceNumber);
    *face++ = v0;
    *face++ = v1;
    *face   = v2;
}

static Mesh * createQuadMesh()
{
    Mesh *mesh = NewObjectZ(Mesh);

    mesh->Name        = "quad";
    mesh->VertexCount = 4;

    mesh->Positions = NewObjects(PVector3, mesh->VertexCount);
    mesh->Positions[0] = { -0.5f, -0.5f, 0.0f };
    mesh->Positions[1] = {  0.5f, -0.5f, 0.0f };
    mesh->Positions[2] = {  0.5f,  0.5f, 0.0f };
    mesh->Positions[3] = { -0.5f,  0.5f, 0.0f };

    mesh->Colors = NewObjects(PVector4, mesh->VertexCount);
    mesh->Colors[0] = { 0.0f, 1.0f, 1.0f };
    mesh->Colors[1] = { 1.0f, 0.0f, 1.0f };
    mesh->Colors[2] = { 1.0f, 1.0f, 0.0f };
    mesh->Colors[3] = { 1.0f, 1.0f, 1.0f };

    mesh->TextureCoordinates = NewObjects(PVector2, mesh->VertexCount);
    mesh->TextureCoordinates[0] = { 0.0f, 1.0f };
    mesh->TextureCoordinates[1] = { 1.0f, 1.0f };
    mesh->TextureCoordinates[2] = { 1.0f, 0.0f };
    mesh->TextureCoordinates[3] = { 0.0f, 0.0f };

    mesh->IndexCount = 6;
    mesh->Indices    = NewObjects(uint32_t, mesh->IndexCount);

    addFace(mesh, 0, 0,1,2);
    addFace(mesh, 1, 0,2,3);

    return mesh;
}

static MeshNode * createMeshNode(SceneNode *parent, Mesh *mesh, Texture *texture, Vector3 position)
{
    MeshNode *node = NewObjectZ(MeshNode);

    node->Type     = SceneNodeTypeMesh;
    node->Flags    = SceneNodeFlagEnabled;
    node->Mesh     = mesh;
    node->Position = position;
    node->Scale    = { texture ? (float) texture->Bitmap.Width / texture->Bitmap.Height : 1, 1, 1 };

    node->Material = NewObjectZ(Material);
    node->Material->Flags = (MaterialFlags) (MaterialFlagModelMatrix | MaterialFlagTexture1);
    node->Material->Shader = "texture";
    node->Material->Textures[0] = texture;

    Scene_SetNodeParent(node, parent);
    return node;
}

static CameraNode * createCameraNode(SceneNode *parent, Vector3 position, float depth)
{
    CameraNode *node = NewObjectZ(CameraNode);

    node->Type        = SceneNodeTypeCamera;
    node->Flags       = SceneNodeFlagEnabled;
    node->Near        = 0.1f;
    node->Far         = node->Near + depth;
    node->FieldOfView = M_PIf / 3;
    node->Position    = position;
    node->Scale       = { 1, 1, 1 };

    Scene_SetNodeParent(node, parent);
    return node;
}

static uint32_t *fillCharCodes(uint32_t min, uint32_t max, uint32_t count, uint32_t *charCodes)
{
    Assert(min <= max);
    Assert(max - min + 1 <= count);
    Assert(charCodes != nullptr);

    for (uint32_t i = min; i <= max; ++i)
        charCodes[i - min] = i;

    return charCodes + max - min + 1;
}

KameAPI RenderTextNode * Allocate()
{
    return NewObjectZ(RenderTextNode);
}

KameAPI int Load(RenderTextNode *node, RootNode *scene)
{
    node->Camera = createCameraNode(scene, { }, 100.f);
    node->Camera->ClearColor = { .1f, .1f, .3f, 1.f };

    node->Font = NewObjectZ(BitmapFont);

    uint32_t *charCodes = NewObjects(uint32_t, 512);
    uint32_t *charCodesOffset = charCodes;
    charCodesOffset = fillCharCodes(' ', '~', 512 - (charCodesOffset - charCodes), charCodesOffset);

    ScopeExit(FreeMemory(charCodes));
    Checked(Font_LoadFromFile("data/arial.ttf", charCodesOffset - charCodes, charCodes, node->Font));

    node->Quad = createMeshNode(scene, createQuadMesh(), node->Font, { 0, 0, -2 });
    node->Quad->Scale *= 2.f;

    scene->Platform->WindowDriver.SetTitle(scene->Platform, "rendertext");

    return ErrNone;
}

KameAPI int Tick(RenderTextNode *node, RootNode *scene)
{
    return ErrNone;
}