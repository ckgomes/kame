#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>
#include <execinfo.h>
#include <time.h>

#include <sys/errno.h>
#include <sys/wait.h>
#include <sys/unistd.h>
#include <sys/fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <pthread.h>

#include <kame/engine/posix_platform.h>
#include <kame/engine/engine.h>

struct PosixLibrary { const char *Name; Handle Lib; };

static int initX11Driver(Platform *p)
{
    LibInit initX11;
    Handle libX11 = nullptr;
    Checked(OpenLibrary("kame_x11", &libX11));
    Checked(LoadLibrarySymbol("Init", libX11, (Handle *) &initX11));
    return initX11(p);
}

static int loadWindowDriver(Platform *p)
{
    if (initX11Driver(p) == ErrNone)
    {
        p->WindowDriver.Load(p);
        return ErrNone;
    }

    return ErrDefault;
}

static pid_t executeProcessPipe(int stdinFd, int stdoutFd, const char *path, ...)
{
    va_list vl;
    char *argv[32];
    pid_t pid = fork();

    if (pid == -1)
    {
        Log(LogError, "posix", "fork failed: %s", strerror(errno));
        return -1;
    }

    if (pid) return pid;

    va_start(vl, path);
    argv[0] = (char*) path;
    for (size_t i = 1; i < ArraySize(argv); ++i)
    {
        argv[i] = va_arg(vl, char*);

        if (argv[i] == NULL)
            break;
    }
    va_end(vl);
    if (stdinFd >= 0) dup2(stdinFd, STDIN_FILENO);
    if (stdoutFd >= 0) dup2(stdoutFd, STDOUT_FILENO);
    execvp(path, argv);
    abort();
}

static int waitProcess(pid_t pid, int flags)
{
    int status;
    pid_t r = waitpid(pid, &status, flags);
    if (r == 0) return ErrNotFinished;
    if (r == -1)
    {
        Log(LogError, "posix", "waitpid failed: %s", strerror(errno));
        return ErrDefault;
    }
    else if (status)
    {
        Log(LogError, "posix", "process exited with code %d", status);
        return ErrDefault;
    }
    return ErrNone;
}

static int executeAddr2Line(const char *elfPath, PosixPipedProcess *pipedProcess)
{
    if (pipe(pipedProcess->pipeParentToChild) == -1) return -errno;
    if (pipe(pipedProcess->pipeChildToParent) == -1) return -errno;

    pipedProcess->pid = executeProcessPipe(
        pipedProcess->pipeParentToChild[STDIN_FILENO],
        pipedProcess->pipeChildToParent[STDOUT_FILENO],
        "/usr/bin/addr2line",
        "-e",
        elfPath,
        nullptr);

    return ErrNone;
}

static void *threadMain(void *data)
{
    PosixThread *posixThread = (PosixThread *) data;
    Log(LogTrace, "posix", "spawned thread %u", (uint32_t) posixThread->Id);
    posixThread->Result = posixThread->Main(posixThread->Data);
    return nullptr;
}

void Log(LogLevel level, const char *category, const char *fmt, ...)
{
    #define WHITE(b)  "\033[" #b ";37m"
    #define BLUE(b)   "\033[" #b ";36m"
    #define YELLOW(b) "\033[" #b ";33m"
    #define RED(b)    "\033[" #b ";31m"

    static void       *addresses[32];
    static char        buf[1024];
    static Dl_info     fa;
    static int         categoryLength;
    static const char *color;
    static int         tty     = isatty(STDOUT_FILENO);
    static const char *logging = getenv("KAME_LOGGING");
    static int         ignored = 1;

    if (level < LogDetail)
    {
        categoryLength = snprintf(buf, sizeof(buf), "%s:", category) - 1;
        ignored = 0;

        if (logging)
        {
            const char *logcf = strstr(logging, buf);
            if (logcf) switch (logcf[categoryLength + 1])
            {
                case 'd': ignored = level < LogDebug; break;
                case 't': ignored = level < LogTrace; break;
                case 'i': ignored = level < LogInfo;  break;
                case 'w': ignored = level < LogWarn;  break;
                case 'e': ignored = level < LogError; break;
                case 'f': ignored = level < LogFatal; break;
            }
        }
    }
    if (ignored)
        return;

    switch (level)
    {
        case LogDebug:
            color = tty ? WHITE(0) : "";
            printf("%s[D] %s:%s ", tty ? WHITE(1) : "", category, color);
            break;
        case LogTrace:
            color = tty ? WHITE(0) : "";
            printf("%s[T] %s:%s ", tty ? WHITE(1) : "", category, color);
            break;
        case LogInfo:
            color = tty ? BLUE(0) : "";
            printf("%s[I] %s:%s ", tty ? BLUE(1) : "", category, color);
            break;
        case LogWarn:
            color = tty ? YELLOW(0) : "";
            printf("%s[W] %s:%s ", tty ? YELLOW(1) : "", category, color);
            break;
        case LogError:
            color = tty ? RED(0) : "";
            printf("%s[E] %s:%s ", tty ? RED(1) : "", category, color);
            break;
        case LogFatal:
            color = tty ? RED(0) : "";
            printf("%s[F] %s:%s ", tty ? RED(1) : "", category, color);
            break;
        case LogDetail:
            printf("%s    %*s  ", color, categoryLength, "");
            break;
    }

    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);

    puts(tty ? "\033[0;37m" : "");

    if (level == LogFatal)
    {
        int addressCount = backtrace(addresses, ArraySize(addresses));

        // The first symbol is the current function, which we want to hide
        for (int i = 1; i < addressCount; ++i)
        {
            PosixPipedProcess pipedProcess;

            dladdr(addresses[i], &fa);

            if (executeAddr2Line(fa.dli_fname, &pipedProcess) != ErrNone)
                printf("    error spawning addr2line process\n");
            else
            {
                int len = snprintf(buf, sizeof(buf), "%p\n", (void*) ((uint8_t*) addresses[i] - (uint8_t*) fa.dli_fbase));
                if (write(pipedProcess.pipeParentToChild[STDOUT_FILENO], buf, len) < 0)
                    printf("    error writing to addr2line process: %s\n", strerror(errno));
                read(pipedProcess.pipeChildToParent[STDIN_FILENO], buf, sizeof(buf));
                if (kill(pipedProcess.pid, SIGTERM))
                    printf("    error killing addr2line process: %s\n", strerror(errno));
                int o = 0;
                while (buf[o] != '\0' && buf[o] != '\n' && buf[o] != '?') ++o;
                if (o) printf("    %.*s\n", o, buf);
            }
        }

        #ifndef NDEBUG
        raise(SIGINT);
        #endif
        exit(EXIT_FAILURE);
    }
}

int OpenMemoryMappedFile(const char *path, MemoryMappedFile *file)
{
    Assert(path != nullptr && file != nullptr);
    int fd = open(path, O_RDONLY);
    CheckedError(fd < 0, ErrDefault, "posix", "open [%s]: %s", path, strerror(errno));
    struct stat statResult;
    CheckedError(fstat(fd, &statResult) < 0, ErrDefault, "posix", "fstat [%s]: %s", path, strerror(errno));
    void *address = mmap(nullptr, (size_t) statResult.st_size, PROT_READ, MAP_SHARED, fd, 0);
    CheckedError(address == MAP_FAILED, ErrDefault, "posix", "mmap [%s]: %s", path, strerror(errno));

    file->Handle = (void *)(size_t) fd;
    file->Start  = address;
    file->End    = (char *) address + statResult.st_size;
    return ErrNone;
}

int CloseMemoryMappedFile(MemoryMappedFile *file)
{
    Assert(file != nullptr);
    int r = munmap(file->Start, (size_t) ((char *) file->End - (char *) file->Start));
    CheckedError(r, ErrDefault, "posix", "munmap [%s]", strerror(errno));
    CheckedError(close((int)(size_t) file->Handle), ErrDefault, "posix", "close: %s", strerror(errno));
    return ErrNone;
}

int GetFileTimestamp(const char *path, uint64_t *timestamp)
{
    Assert(path != nullptr && timestamp != nullptr);
    struct stat s;
    CheckedError(stat(path, &s), ErrDefault, "posix", "stat [%s]: [%s]", path, strerror(errno));
    *timestamp = (uint64_t) 1000 * (uint64_t) s.st_mtim.tv_sec + (uint64_t) s.st_mtim.tv_nsec / (uint64_t) 1000000;
    return ErrNone;
}

void *AllocateMemory(size_t size, size_t align)
{
    void *ptr = aligned_alloc(align, size);
    if (!ptr)
        Log(LogError, "posix", "could not allocate %zu bytes", size);
    return ptr;
}

void FreeMemory(void *ptr)
{
    free(ptr);
}

int OpenLibrary(const char *libraryName, Handle *library)
{
    Assert(libraryName != nullptr && library != nullptr);

    Handle libraryHandle;

    if (strstr(libraryName, "/"))
        libraryHandle = dlopen(libraryName, RTLD_LOCAL | RTLD_NOW);
    else
    {
        char buf[128];
        int w = snprintf(buf, sizeof(buf), "./lib%s.so", libraryName);
        CheckedError(w >= sizeof(buf), ErrDefault, "posix", "openLib: buffer overflow caused by %s", libraryName);
        libraryHandle = dlopen(buf, RTLD_LOCAL | RTLD_NOW);
    }

    CheckedError(!libraryHandle, ErrDefault, "posix", "openLib: %s", dlerror());

    PosixLibrary *lib  = NewObjectZ(PosixLibrary);
    lib->Name = libraryName;
    lib->Lib  = libraryHandle;

    *library = lib;
    return ErrNone;
}

int CloseLibrary(Handle library)
{
    Assert(library != nullptr);
    PosixLibrary *lib = (PosixLibrary *) library;
    CheckedError(dlclose(lib->Lib), ErrDefault, "posix", "dlclose [%s]", lib->Name);
    return ErrNone;
}

int LoadLibrarySymbol(const char *symbolName, Handle library, Handle *symbol)
{
    Assert(symbolName != nullptr && library != nullptr && symbol != nullptr);
    PosixLibrary *lib = (PosixLibrary *) library;
    *symbol = dlsym(lib->Lib, symbolName);
    CheckedError(!*symbol, ErrDefault, "posix", "could not load symbol [%s] in library [%s]", symbol, lib->Name);
    return ErrNone;
}

uint64_t GetElapsedTicks()
{
    struct timespec tv;
    clock_gettime(CLOCK_MONOTONIC, &tv);
    return (uint64_t) 1000000000 * (uint64_t) tv.tv_sec + (uint64_t) tv.tv_nsec;
}

uint64_t GetTicksPerSecond()
{
    return 1000000000;
}

int Sleep(uint64_t ticks)
{
    struct timespec tv;
    tv.tv_sec  = ticks / GetTicksPerSecond();
    tv.tv_nsec = ticks % GetTicksPerSecond();
    CheckedError(nanosleep(&tv, nullptr), ErrDefault, "posix", "nanosleep [%s]", strerror(errno));
    return ErrNone;
}

int CreateThread(FnThreadMain main, void *data, const char *name, Handle *thread)
{
    Assert(main != nullptr && thread != nullptr);
    PosixThread *posixThread;
    *thread = posixThread = NewObjectZ(PosixThread);
    posixThread->Main = main;
    posixThread->Data = data;
    CheckedError(
        pthread_attr_init(&posixThread->Attributes),
        ErrDefault,
        "posix", "pthread_attr_init [%s]", strerror(errno));
    CheckedError(
        pthread_attr_setstacksize(&posixThread->Attributes, 1024*1024),
        ErrDefault,
        "posix", "pthread_attr_setstacksize [%s]", strerror(errno));
    CheckedError(
        pthread_create(&posixThread->Id, &posixThread->Attributes, threadMain, posixThread),
        ErrDefault,
        "posix", "pthread_create [%s]", strerror(errno));
    if (name)
    {
        CheckedError(
            pthread_setname_np(posixThread->Id, name),
            ErrDefault,
            "posix", "pthread_setname_np [%s]", strerror(errno));
    }
    return ErrNone;
}

static int run(Platform *p)
{
    CheckedFatal(loadWindowDriver(p), "posix.run", "no window driver could be loaded");
    CheckedFatal(p->WindowDriver.LoadRenderer(p), "posix.run", "no renderer driver could be loaded");

    Checked(engineLoad(p));

    p->Running = 1;

    while (p->Running)
        Checked(engineUpdate(p));

    return ErrNone;
}

int main(int argc, char *argv[])
{
    PosixPlatform p = {};
    p.argc = argc;
    p.argv = argv;

    return run(&p) == ErrDefault ? EXIT_SUCCESS : EXIT_FAILURE;
}