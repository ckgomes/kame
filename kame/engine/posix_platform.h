#pragma once

#include <sys/types.h>
#include <pthread.h>

#include <kame/engine/platform.h>

struct PosixPlatform : Platform
{
    int    argc;
    char **argv;
};

struct PosixPipedProcess
{
    pid_t pid;

    int pipeChildToParent[2];
    int pipeParentToChild[2];

    int    argc;
    char **argv;
};

struct PosixThread
{
    pthread_t       Id;
    pthread_attr_t  Attributes;
    FnThreadMain    Main;
    void           *Data;
    int             Result;
};