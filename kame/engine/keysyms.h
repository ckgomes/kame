#pragma once

#include <kame/base.h>

enum KeySym : uint32_t
{
    KeySymBackspace    = 8,
    KeySymTab          = 9,
    KeySymClear        = 12,
    KeySymReturn       = 13,
    KeySymPause        = 19,
    KeySymEscape       = 27,
    KeySymSpace        = 32,
    KeySymExclaim      = 33,
    KeySymQuotedBl     = 34,
    KeySymHash         = 35,
    KeySymDollar       = 36,
    KeySymAmpersand    = 38,
    KeySymQuote        = 39,
    KeySymLeftParen    = 40,
    KeySymRightParen   = 41,
    KeySymAsterisk     = 42,
    KeySymPlus         = 43,
    KeySymComma        = 44,
    KeySymMinus        = 45,
    KeySymPeriod       = 46,
    KeySymSlash        = 47,
    KeySym0            = 48,
    KeySym1            = 49,
    KeySym2            = 50,
    KeySym3            = 51,
    KeySym4            = 52,
    KeySym5            = 53,
    KeySym6            = 54,
    KeySym7            = 55,
    KeySym8            = 56,
    KeySym9            = 57,
    KeySymColon        = 58,
    KeySymSemicolon    = 59,
    KeySymLess         = 60,
    KeySymEquals       = 61,
    KeySymGreater      = 62,
    KeySymQuestion     = 63,
    KeySymAt           = 64,
    KeySymLeftBracket  = 91,
    KeySymBackslash    = 92,
    KeySymRightBracket = 93,
    KeySymCaret        = 94,
    KeySymUnderscore   = 95,
    KeySymBackquote    = 96,
    KeySymA            = 97,
    KeySymB            = 98,
    KeySymC            = 99,
    KeySymD            = 100,
    KeySymE            = 101,
    KeySymF            = 102,
    KeySymG            = 103,
    KeySymH            = 104,
    KeySymI            = 105,
    KeySymJ            = 106,
    KeySymK            = 107,
    KeySymL            = 108,
    KeySymM            = 109,
    KeySymN            = 110,
    KeySymO            = 111,
    KeySymP            = 112,
    KeySymQ            = 113,
    KeySymR            = 114,
    KeySymS            = 115,
    KeySymT            = 116,
    KeySymU            = 117,
    KeySymV            = 118,
    KeySymW            = 119,
    KeySymX            = 120,
    KeySymY            = 121,
    KeySymZ            = 122,
    KeySymDelete       = 127,
    KeySymKP0          = 256,
    KeySymKP1          = 257,
    KeySymKP2          = 258,
    KeySymKP3          = 259,
    KeySymKP4          = 260,
    KeySymKP5          = 261,
    KeySymKP6          = 262,
    KeySymKP7          = 263,
    KeySymKP8          = 264,
    KeySymKP9          = 265,
    KeySymKPPeriod     = 266,
    KeySymKPDivide     = 267,
    KeySymKPMultiply   = 268,
    KeySymKPMinus      = 269,
    KeySymKPPlus       = 270,
    KeySymKPEnter      = 271,
    KeySymKPEquals     = 272,
    KeySymUp           = 273,
    KeySymDown         = 274,
    KeySymRight        = 275,
    KeySymLeft         = 276,
    KeySymInsert       = 277,
    KeySymHome         = 278,
    KeySymEnd          = 279,
    KeySymPageUp       = 280,
    KeySymPageDown     = 281,
    KeySymF1           = 282,
    KeySymF2           = 283,
    KeySymF3           = 284,
    KeySymF4           = 285,
    KeySymF5           = 286,
    KeySymF6           = 287,
    KeySymF7           = 288,
    KeySymF8           = 289,
    KeySymF9           = 290,
    KeySymF10          = 291,
    KeySymF11          = 292,
    KeySymF12          = 293,
    KeySymF13          = 294,
    KeySymF14          = 295,
    KeySymF15          = 296,
    KeySymNumLock      = 300,
    KeySymCapsLock     = 301,
    KeySymScrollLock   = 302,
    KeySymRShift       = 303,
    KeySymLShift       = 304,
    KeySymRCtrl        = 305,
    KeySymLCtrl        = 306,
    KeySymRAlt         = 307,
    KeySymLAlt         = 308,
    KeySymRMeta        = 309,
    KeySymLMeta        = 310,
    KeySymLSuper       = 311,
    KeySymRSuper       = 312,
    KeySymMode         = 313,
    KeySymCompose      = 314,
    KeySymHelp         = 315,
    KeySymPrint        = 316,
    KeySymSysReq       = 317,
    KeySymBreak        = 318,
    KeySymMenu         = 319,
};