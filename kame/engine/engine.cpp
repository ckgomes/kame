#include <string.h>

#include <kame/engine/platform.h>
#include <kame/engine/engine.h>

static int loadBehaviors(SceneNode *node, RootNode *scene)
{
    if (node->Type == SceneNodeTypeBehavior)
    {
        BehaviorNode *behaviorNode = (BehaviorNode *) node;
        if (behaviorNode->Load)
            Checked(behaviorNode->Load(node, scene));
    }

    for (uint32_t i = 0; i < node->ChildCount; ++i)
        Checked(loadBehaviors(node->Children[i], scene));

    return ErrNone;
}

static int loadScene(SceneNode *node, RootNode *scene)
{
    if (node->Flags & SceneNodeFlagEnabled)
    {
        switch (node->Type)
        {
        case SceneNodeTypeMesh:
        {
            MeshNode *meshNode = (MeshNode *) node;
            Mesh     *mesh     = meshNode->Mesh;
            if (!mesh->RendererData || mesh->Flags & MeshFlagDirty)
                scene->Platform->RendererDriver.PrepareMesh(scene->Platform, mesh);
            if (meshNode->Material && (!meshNode->Material->RendererData || meshNode->Material->Flags & MaterialFlagDirty))
                scene->Platform->RendererDriver.PrepareMaterial(scene->Platform, meshNode->Material);
        }
        break;
        }
    }

    for (uint32_t i = 0; i < node->ChildCount; ++i)
        loadScene(node->Children[i], scene);

    return ErrNone;
}

static int loadBehaviorLibrary(Platform *p)
{
    if (p->BehaviorLibrary)
    {
        if (p->BehaviorNode)
        {
            p->BehaviorNode->Load = nullptr;
            p->BehaviorNode->Tick = nullptr;
        }

        Checked(CloseLibrary(p->BehaviorLibrary));
        p->BehaviorLibrary = nullptr;
    }

    Checked(OpenLibrary(p->BehaviorLibraryPath, &p->BehaviorLibrary));

    if (!p->BehaviorNode)
    {
        BehaviorNodeAllocate behaviorAllocate;
        Checked(LoadLibrarySymbol("Allocate", p->BehaviorLibrary, (Handle *)&behaviorAllocate));
        p->BehaviorNode = behaviorAllocate();
        p->BehaviorNode->Type = SceneNodeTypeBehavior;
        Scene_SetEnabled(p->BehaviorNode, 0);
        Scene_SetNodeParent(p->BehaviorNode, p->ActiveScene);
    }

    Checked(LoadLibrarySymbol("Load", p->BehaviorLibrary, (Handle *)&p->BehaviorNode->Load));
    Checked(LoadLibrarySymbol("Tick", p->BehaviorLibrary, (Handle *)&p->BehaviorNode->Tick));

    return ErrNone;
}

int engineLoad(Platform *p)
{
    p->ActiveScene = NewObjectZ(RootNode);
    p->ActiveScene->Type     = SceneNodeTypeRoot;
    p->ActiveScene->Platform = p;
    p->ActiveScene->Scale    = { 1, 1, 1 };

    p->TimeStep      = 1.0f / 60.0f;
    p->TimeStepTicks = (uint64_t)(p->TimeStep * GetTicksPerSecond());

    if ((p->BehaviorLibraryPath = getenv("KAME_BEHAVIOR_LIBRARY")))
    {
        Checked(GetFileTimestamp(p->BehaviorLibraryPath, &p->BehaviorLibraryChangedAt));
        Checked(loadBehaviorLibrary(p));
        p->BehaviorLibraryLoaded = 1;
    }

    Checked(loadBehaviors(p->ActiveScene, p->ActiveScene));
    Checked(loadScene(p->ActiveScene, p->ActiveScene));

    Scene_SetDirty(p->ActiveScene);

    p->StartTicks = p->PreviousElapsedTicks = p->ElapsedTicks = p->SimulatedTicks = GetElapsedTicks();

    return ErrNone;
}

int engineUpdate(Platform *p)
{
    if (p->BehaviorLibraryLoaded)
    {
        uint64_t behaviorChangedAt;
        if (GetFileTimestamp(p->BehaviorLibraryPath, &behaviorChangedAt) == ErrNone)
        {
            if (behaviorChangedAt > p->BehaviorLibraryChangedAt)
            {
                if (loadBehaviorLibrary(p) == ErrNone)
                {
                    Log(LogInfo, "engine", "reloaded behavior library [%s]", p->BehaviorLibraryPath);
                    p->BehaviorLibraryChangedAt = behaviorChangedAt;
                }
            }
        }
    }

    p->ElapsedTicks         = GetElapsedTicks();
    p->TimeDelta            = (float) ((p->ElapsedTicks - p->PreviousElapsedTicks) / (double)(GetTicksPerSecond()));
    p->PreviousElapsedTicks = p->ElapsedTicks;

    // if the last frame was too long ago, do a timeskip instead of simulating everything
    if (p->ElapsedTicks - p->SimulatedTicks >= GetTicksPerSecond())
        p->SimulatedTicks = p->ElapsedTicks;

    while (p->ElapsedTicks - p->SimulatedTicks >= p->TimeStepTicks)
    {
        p->TickNumber += 1;

        BehaviorNode *behaviors[128];
        uint32_t behaviorCount = Scene_SearchNodesOfType(
            p->ActiveScene,
            SceneNodeTypeBehavior,
            SceneNodeFlagEnabled,
            SceneNodeFlagNone,
            0,
            1,
            ArraySize(behaviors),
            (SceneNode **) behaviors);

        memcpy(p->PreviousKeyboardState, p->KeyboardState, sizeof(p->KeyboardState));
        CheckedFatal(p->WindowDriver.ProcessEvents(p), "engine", "engineTick: WindowDriver.ProcessEvents");

        for (uint32_t i = 0; i < behaviorCount; ++i)
        {
            BehaviorNode *behavior = behaviors[i];
            if (behavior->Tick)
                behavior->Tick(behavior, p->ActiveScene);
        }
        p->SimulatedTicks += p->TimeStepTicks;
    }

    p->FrameNumber += 1;
    int drawError = p->RendererDriver.Draw(p);
    if (drawError != ErrFinished)
        CheckedFatal(drawError, "engine", "run: RendererDriver.Draw");

    static const char *singleFrame = getenv("KAME_SINGLE_FRAME");
    static const int isSingleFrame = singleFrame ? atoi(singleFrame) : 0;
    if (isSingleFrame)
        p->Running = 0;

    return ErrNone;
}