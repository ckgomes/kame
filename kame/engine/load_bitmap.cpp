#include <kame/engine/platform.h>

#include <string.h>
#include <png.h>
#define FILE void*
#include <jpeglib.h>

struct ErrorJPEG
{
  struct jpeg_error_mgr Manager;
  jmp_buf SetJumpBuffer;
  int     Result;
  int     Error;
};

static void errorHandlerJPEG(j_common_ptr info)
{
    ErrorJPEG *error = (ErrorJPEG *) info->err;
    longjmp(error->SetJumpBuffer, 1);
}

static int loadJPEG(size_t length, const void *data, Bitmap *bitmap)
{
    ErrorJPEG error = {};
    struct jpeg_decompress_struct info = {};

    jpeg_create_decompress(&info);
    info.err = jpeg_std_error(&error.Manager);
    info.err->error_exit = errorHandlerJPEG;

    error.Error = ErrDefault;
    if (setjmp(error.SetJumpBuffer))
    {
        error.Result = error.Error;
        goto leave;
    }

    jpeg_mem_src(&info, (uint8_t *) data, (unsigned long) length);

    error.Error = ErrInvalidData;
    if (jpeg_read_header(&info, TRUE) != JPEG_HEADER_OK)
    {
        error.Result = ErrInvalidData;
        goto leave;
    }

    info.out_color_space = JCS_EXT_RGBX;

    error.Error = ErrDefault;
    if (!jpeg_start_decompress(&info))
    {
        error.Result = ErrDefault;
        goto leave;
    }

    if (info.out_color_space != JCS_EXT_RGBX)
    {
        error.Result = ErrDefault;
        goto leave;
    }

    bitmap->Width        = (uint32_t) info.output_width;
    bitmap->Height       = (uint32_t) info.output_height;
    bitmap->BitsPerPixel = 32;
    bitmap->Channels     = 3;
    bitmap->Pitch        = bitmap->Width * (bitmap->BitsPerPixel / 8);
    bitmap->Length       = bitmap->Pitch * bitmap->Height;
    bitmap->Data         = NewObjects(uint8_t, bitmap->Length);

    for (uint8_t *pos = bitmap->Data, *end = pos + info.output_height * bitmap->Pitch; pos < end; pos += bitmap->Pitch)
        jpeg_read_scanlines(&info, &pos, 1);

    if (!jpeg_finish_decompress(&info))
    {
        error.Result = ErrDefault;
        goto leave;
    }

leave:
    jpeg_destroy_decompress(&info);
    return error.Result;
}

typedef struct
{
    uint8_t *pos, *end;
} LibPNGReader;

static void readDataPNG(png_structp pngPtr, uint8_t *bytes, size_t length)
{
    LibPNGReader *reader = (LibPNGReader*) png_get_io_ptr(pngPtr);
    uint8_t *pos = reader->pos;
    reader->pos += length;
    memcpy(bytes, pos, length);
}

static int loadPNG(size_t length, const void *data, Bitmap *bitmap)
{
    int depth, color_type;

    if (!data || length < 8 || !png_check_sig((uint8_t *) data, 8))
        return ErrInvalidData;

    png_structp pngPtr = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);

    png_infop info = png_create_info_struct(pngPtr);

    png_jmpbuf(pngPtr);

    LibPNGReader reader = { (uint8_t *) data, (uint8_t *) data + length };
    png_set_read_fn(pngPtr, &reader, readDataPNG);

    png_set_sig_bytes(pngPtr, 0);
    png_read_info(pngPtr, info);

    png_set_strip_16(pngPtr);                          // Truncate 16 bit PNGs to 8 bits
	png_set_expand_gray_1_2_4_to_8(pngPtr);            // Expand grayscales to 8 bits
	png_set_palette_to_rgb(pngPtr);                    // Convert grayscales to RGB, but this may the changed
    png_set_tRNS_to_alpha(pngPtr);                     // Convert tRNS transparency to a proper alpha channel
    png_set_add_alpha(pngPtr, 0xFF, PNG_FILLER_AFTER); // Add a filler alpha channel, if it doesn't exist
    png_set_interlace_handling(pngPtr);                // Deinterlace image if needed

    png_read_update_info(pngPtr, info);

    png_get_IHDR(pngPtr, info, &bitmap->Width, &bitmap->Height, &depth, &color_type, nullptr, nullptr, nullptr);
    bitmap->Channels     = (uint32_t) png_get_channels(pngPtr, info);
    bitmap->BitsPerPixel = bitmap->Channels * 8;
    bitmap->Pitch        = bitmap->BitsPerPixel / 8 * bitmap->Width;
    bitmap->Length       = bitmap->Pitch * bitmap->Height;
    bitmap->Data         = NewObjects(uint8_t, bitmap->Length);

    CheckedError(bitmap->Channels != 4, ErrDefault, "png", "expected 4 channels");

    for (uint8_t *pos = bitmap->Data, *end = pos + bitmap->Height * bitmap->Pitch; pos < end; pos += bitmap->Pitch)
        png_read_row(pngPtr, pos, nullptr);

    png_destroy_read_struct(&pngPtr, &info, nullptr);

    return ErrNone;
}

int LoadBitmap(size_t length, const void *data, Bitmap *bitmap)
{
    int result;

    if ((result = loadJPEG(length, data, bitmap)) != ErrInvalidData)
        return result;

    if ((result = loadPNG(length, data, bitmap)) != ErrInvalidData)
        return result;

    return result;
}