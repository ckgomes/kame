#pragma once

#include <kame/base.h>

int engineLoad(Platform *p);
int engineUpdate(Platform *p);