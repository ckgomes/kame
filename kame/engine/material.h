#pragma once

#include <kame/base.h>
#include <kame/engine/texture.h>

enum MeshTopology : uint16_t
{
    MeshTopologyTriangles = 0,
    MeshTopologyLines     = 1,
};

enum CullMode : uint16_t
{
    CullModeNever = 0,
    CullModeBack  = 1,
    CullModeFront = 2,
};

enum BlendMode : uint16_t
{
    BlendModeNone           = 0,
    BlendModeAdditive       = 1,
    BlendModeSubtractive    = 2,
    BlendModeMultiplicative = 3,
    BlendModeAlpha          = 4,
};

enum DrawMode : uint16_t
{
    DrawModeFill      = 0,
    DrawModeWireframe = 1,
};

enum MaterialFlags : uint32_t
{
    // The renderer should consider that the material has changed
    MaterialFlagDirty = (1 << 0),

    // The shader uses the model matrix
    MaterialFlagModelMatrix = (1 << 1),

    // The shader uses the following texture slots
    MaterialFlagTexture1 = (1 << 2),
    MaterialFlagTexture2 = (1 << 3),
    MaterialFlagTexture3 = (1 << 4),
    MaterialFlagTexture4 = (1 << 5),
    MaterialFlagTexture5 = (1 << 6),
    MaterialFlagTexture6 = (1 << 7),
    MaterialFlagTexture7 = (1 << 8),
    MaterialFlagTexture8 = (1 << 9),
};

struct SceneNode;
struct CameraNode;
struct Material;

struct SetMaterialParameters
{
    Material   *Material;
    SceneNode  *Node;
    CameraNode *Camera;
};

typedef uint32_t (*FnSetMaterialParameters)(SetMaterialParameters params, void *data);

struct Material
{
    const char *AssetPath;
    const char *Name;

    // The name of shader used
    // In Vulkan, SPIRV modules are loaded from the data/shaders directory
    const char *Shader;

    // The material is only used rendered in these layers
    uint64_t RenderLayers;

    MaterialFlags Flags;

    Texture *Textures[8];

    CullMode     CullMode;
    BlendMode    BlendMode;
    DrawMode     DrawMode;
    MeshTopology MeshTopology;

    FnSetMaterialParameters SetVertexParameters;
    FnSetMaterialParameters SetFragmentParameters;

    Handle RendererData;
};
