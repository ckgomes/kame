#pragma once

#include <kame/base.h>
#include <kame/math.h>
#include <kame/engine/texture.h>

struct BitmapFontGlyph
{
    uint32_t    Code;
    float       Width, Height;
    float       Advance;
    PVector2    Bearing;
    Rectangle2  Area;
    float      *Kernings;
};

struct BitmapFont : Texture
{
    uint32_t         GlyphCount;
    BitmapFontGlyph *Glyphs;
};

KameAPI uint32_t Font_IndexGlyph(BitmapFont *bitmapFont, uint32_t charCode);

KameAPI int Font_LoadFromFile(
    const char     *path,
    uint32_t        charCount,
    const uint32_t *charCodes,
    Out BitmapFont *bitmapFont);