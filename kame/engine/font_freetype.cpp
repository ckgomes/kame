#include <freetype/freetype.h>

#include <kame/engine/font.h>
#include <kame/engine/platform.h>

#define FTChecked(expr) \
do { auto _r = (FT_Error) expr; if (_r) Log(LogFatal, "font", #expr ": [%s]", FT_Error_String(_r)); } while (0)

static void dbgPrintGlyph(FT_Bitmap *bitmap, unsigned int charCode)
{
    LogDetail("GLYPH %u", charCode);
    for (uint32_t y = 0; y < bitmap->rows; ++y)
    {
        char rowbuf[128];
        int rowbufPos = 0;
        for (uint32_t x = 0; x < bitmap->width; ++x)
        {
            uint8_t value = bitmap->buffer[y * bitmap->pitch + x];
            value /= (25);
            rowbuf[rowbufPos++] = value < 10 ? ('0' + value) : ('A' + value - 10);
        }
        rowbuf[rowbufPos] = '\0';
        LogDetail("%s", rowbuf);
    }
}

static uint32_t binarySearchGlyph(uint32_t charCode, uint32_t count, BitmapFontGlyph *glyphs)
{
    int32_t index, minBound = 0, maxBound = (int32_t) count - 1;

    while (1)
    {
        if (minBound > maxBound)
            break;

        index = (maxBound + minBound) / 2;

        if (glyphs[index].Code > charCode)
            maxBound = index - 1;
        else if (glyphs[index].Code < charCode)
            minBound = index + 1;
        else
            return (uint32_t) index;
    }

    return UINT32_MAX;
}

uint32_t Font_IndexGlyph(BitmapFont *bitmapFont, uint32_t charCode)
{
    Assert(bitmapFont != nullptr && bitmapFont->Glyphs != nullptr);
    return binarySearchGlyph(charCode, bitmapFont->GlyphCount, bitmapFont->Glyphs);
}

int Font_LoadFromFile(
    const char     *path,
    uint32_t        charCount,
    const uint32_t *charCodes,
    BitmapFont     *bitmapFont)
{
    ValidateParam(path != nullptr);
    ValidateParam(charCount > 0);
    ValidateParam(charCodes != nullptr);
    ValidateParam(bitmapFont != nullptr);

    FT_Library library;
    FT_Face    face;

    Log(LogTrace, "font", "loading ttf [%s]", path);

    FTChecked(FT_Init_FreeType(&library));
    ScopeExit(FTChecked(FT_Done_FreeType(library)));

    FTChecked(FT_New_Face(library, path, 0, &face));
    ScopeExit(FTChecked(FT_Done_Face(face)));

    bitmapFont->AssetPath  = path;
    bitmapFont->Name       = path;
    bitmapFont->GlyphCount = charCount;
    bitmapFont->Glyphs     = NewObjects(BitmapFontGlyph, charCount);

    for (uint32_t i = 0; i < charCount; ++i)
    {
        FT_ULong charCode = (FT_ULong) charCodes[i];
        FT_UInt charIndex = FT_Get_Char_Index(face, charCode);
        FTChecked(FT_Load_Glyph(face, charIndex, FT_LOAD_BITMAP_METRICS_ONLY));

        BitmapFontGlyph *glyph = bitmapFont->Glyphs + i;
        glyph->Width     = face->glyph->metrics.width        / 64.f;
        glyph->Height    = face->glyph->metrics.height       / 64.f;
        glyph->Bearing.X = face->glyph->metrics.horiBearingX / 64.f;
        glyph->Bearing.Y = face->glyph->metrics.horiBearingY / 64.f;
        glyph->Advance   = face->glyph->metrics.horiAdvance  / 64.f;
        glyph->Kernings  = NewObjects(float, charCount);

        for (uint32_t k = 0; k < charCount; ++k)
        {
            FT_ULong otherCharCode = (FT_ULong) charCodes[k];
            FT_UInt otherCharIndex = FT_Get_Char_Index(face, otherCharCode);

            FT_Vector kerning;
            FTChecked(FT_Get_Kerning(face, charIndex, otherCharIndex, FT_KERNING_DEFAULT, &kerning));
            glyph->Kernings[k] = kerning.x / 64.f;
        }
    }

    bitmapFont->Bitmap.Width        = 256;
    bitmapFont->Bitmap.Height       = 256;
    bitmapFont->Bitmap.BitsPerPixel = 32;
    bitmapFont->Bitmap.Channels     = 4;
    bitmapFont->Bitmap.Pitch        = bitmapFont->Bitmap.Width * (bitmapFont->Bitmap.BitsPerPixel / 8);
    bitmapFont->Bitmap.Length       = bitmapFont->Bitmap.Height * bitmapFont->Bitmap.Pitch;
    bitmapFont->Bitmap.Data         = NewObjects(uint8_t, bitmapFont->Bitmap.Length);

    uint32_t channel     = 0;
    uint32_t currentY    = 0;
    uint32_t currentX    = 0;
    uint32_t maxCurrentY = 0;

    for (uint32_t i = 0; i < charCount; ++i)
    {
        FT_ULong charCode = (FT_ULong) charCodes[i];
        FTChecked(FT_Load_Char(face, charCode, FT_LOAD_RENDER));
        FTChecked(FT_Render_Glyph(face->glyph, FT_RENDER_MODE_SDF));
        FT_Bitmap *bitmap = &face->glyph->bitmap;

        if (bitmapFont->Bitmap.Width - currentX < bitmap->width)
        {
            currentY += maxCurrentY;
            currentX = maxCurrentY = 0;
        }
        if (bitmapFont->Bitmap.Height - currentY < bitmap->rows)
        {
            if (channel == 3)
                return ErrOutOfResources;

            channel += 1;
            currentY = currentX = maxCurrentY = 0;
        }

        BitmapFontGlyph *glyph = bitmapFont->Glyphs + i;
        glyph->Area.X      = (float) currentX / (float) bitmapFont->Bitmap.Width;
        glyph->Area.Y      = (float) currentY / (float) bitmapFont->Bitmap.Height;
        glyph->Area.Width  = (float) bitmap->width / (float) bitmapFont->Bitmap.Width;
        glyph->Area.Height = (float) bitmap->rows  / (float) bitmapFont->Bitmap.Height;

        for (uint32_t y = 0; y < bitmap->rows; ++y)
        {
            uint8_t *srcRow = bitmap->buffer + y * bitmap->pitch;
            uint8_t *dstRow = bitmapFont->Bitmap.Data + (currentY + y) * bitmapFont->Bitmap.Pitch;
            for (uint32_t x = 0; x < bitmap->width; ++x)
            {
                uint8_t *pixel = dstRow + (currentX + x) * (bitmapFont->Bitmap.BitsPerPixel / 8);
                pixel[channel] = srcRow[x];
            }
        }

        currentX += bitmap->width;
        maxCurrentY = Max(maxCurrentY, bitmap->rows);
    }

    return ErrNone;
}