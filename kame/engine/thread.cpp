#include <stdatomic.h>
#include <stdio.h>

#include <kame/engine/thread.h>
#include <kame/engine/platform.h>

struct ThreadPoolTask
{
    FnTask  Task;
    void   *Data;
};

struct ThreadPoolContext
{
    atomic_uint_least64_t InsertIndex, WriteIndex, ReadIndex;
    Handle                Threads[16];
    ThreadPoolTask        Tasks[1024];
};

int threadMain(void *data)
{
    ThreadPoolContext *context = (ThreadPoolContext *) data;
    while (1)
    {
        uint64_t readIndex = atomic_load(&context->ReadIndex);
        uint64_t writeIndex = atomic_load(&context->WriteIndex);

        if (readIndex == writeIndex)
            Sleep(GetTicksPerSecond() / 200);
        else if (readIndex < writeIndex)
        {
            if (atomic_compare_exchange_strong(&context->ReadIndex, &readIndex, readIndex + 1))
            {
                ThreadPoolTask *taskPointer = context->Tasks + readIndex % ArraySize(context->Tasks);
                atomic_thread_fence(memory_order_acquire);
                /*volatile*/ ThreadPoolTask task = *taskPointer;
                task.Task(readIndex + 1, task.Data);
            }
        }
    }

    return ErrNone;
}

int CreateThreadPool(uint32_t threadCount, const char *name, Handle *threadPool)
{
    ThreadPoolContext *context;

    if (threadCount > ArraySize(context->Threads))
        return ErrInvalidParameter;

    *threadPool = context = NewObjectZ(ThreadPoolContext);

    for (uint32_t i = 0; i < threadCount; ++i)
    {
        char buf[16];
        snprintf(buf, sizeof(buf), "%s:%u", name, i);
        Checked(CreateThread(threadMain, context, buf, context->Threads + i));
    }

    return ErrNone;
}

int QueueTask(Handle threadPool, FnTask task, void *data, uint64_t *taskId)
{
    ThreadPoolContext *context = (ThreadPoolContext *) threadPool;
    uint64_t insertIndex = atomic_fetch_add(&context->InsertIndex, 1);
    context->Tasks[insertIndex % ArraySize(context->Tasks)] = { task, data };
    atomic_thread_fence(memory_order_release);
    atomic_fetch_add(&context->WriteIndex, 1);
    if (taskId)
        *taskId = insertIndex + 1;
    return ErrNone;
}