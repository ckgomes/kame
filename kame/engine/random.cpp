#include <kame/engine/random.h>
#include <kame/engine/platform.h>

struct Random
{
    uint64_t Next;
};

static uint16_t nextValue(Random *random)
{
    random->Next = random->Next * 1103515245 + 12345;
    return (uint16_t) ((random->Next / 65536) % 32768);
}

Handle Random_New(uint64_t seed)
{
    Random *random = NewObject(Random);
    random->Next = seed;
    return random;
}

void Random_Bytes(Handle handle, size_t length, void *bytes)
{
    Random *random = (Random *) handle;
    uint8_t *data = (uint8_t *) bytes;

    while (length >= 2)
    {
        *((uint16_t *) data) = nextValue(random);
        data += 2;
        length -= 2;
    }

    if (length == 1)
        *data = (uint8_t) nextValue(random);
}

int32_t Random_Int32(Handle handle, int32_t min, int32_t max)
{
    Random *random = (Random *) handle;
    return (int32_t) ((uint32_t) (nextValue(random) << 16) | (uint32_t) nextValue(random));
}

float Random_Float32(Handle handle, float min, float max)
{
    const uint32_t exponentMask = 0x3F000000;
    const uint32_t mantissaMask = 0x007FFFFF;
    Random *random = (Random *) handle;
    union { float f; uint32_t b; } number;
    number.b = (((uint32_t) nextValue(random) << 16) | (uint32_t) nextValue(random));
    number.b = exponentMask | (number.b & mantissaMask);
    return min + 2.f * (number.f - 0.5f) * (max - min);
}

#define USE_SSE 1
#if USE_SSE
#include <xmmintrin.h>
#include <emmintrin.h>
static inline __m128 floor_ps(const __m128 x) {
    __m128i v0 = _mm_setzero_si128();
    __m128i v1 = _mm_cmpeq_epi32(v0, v0);
    __m128i ji = _mm_srli_epi32(v1, 25);
    __m128i tmp = _mm_slli_epi32(ji, 23); // I edited this (Added tmp) not sure about it
    __m128 j = _mm_castsi128_ps(tmp); //create vector 1.0f // I edited this not sure about it
    __m128i i = _mm_cvttps_epi32(x);
    __m128 fi = _mm_cvtepi32_ps(i);
    __m128 igx = _mm_cmpgt_ps(fi, x);
    j = _mm_and_ps(igx, j);
    return _mm_sub_ps(fi, j);
}
inline __m128 mod_ps(const __m128 a, const __m128 aDiv) {
    __m128 c = _mm_div_ps(a, aDiv);
    __m128i i = _mm_cvttps_epi32(c);
    __m128 cTrunc = _mm_cvtepi32_ps(i);
    __m128 base = _mm_mul_ps(cTrunc, aDiv);
    __m128 r = _mm_sub_ps(a, base);
    return r;
}

static inline __m128 perm_ps(__m128 x)
{
    return mod_ps(_mm_mul_ps(_mm_add_ps(_mm_mul_ps(x, _mm_set_ps1(34.f)), _mm_set_ps1(1.f)), x), _mm_set_ps1(289.f));
}
static inline __m128 fract_ps(__m128 x)
{
    return _mm_sub_ps(x, floor_ps(x));
}
static inline float randomNoise_ps(Vector3 v)
{
    __m128 p, a, d, b, k1, k2, c, k3, k4, o1, o2, o3, o4;
    p = _mm_load_ps((const float *) &v);
    a = floor_ps(p);
    d = _mm_sub_ps(p, a);
    d = _mm_mul_ps(_mm_mul_ps(d, d), _mm_sub_ps(_mm_set_ps1(3.f), _mm_mul_ps(_mm_set_ps1(2.f), d)));
    b = _mm_add_ps(_mm_shuffle_ps(a, a, 0b01010000), _mm_set_ps(1.f, 0.f, 1.f, 0.f));
    k1 = perm_ps(_mm_shuffle_ps(b, b, 0b01000100));
    k2 = perm_ps(_mm_add_ps(_mm_shuffle_ps(k1, k1, 0b01000100), _mm_shuffle_ps(b, b, 0b11111010)));
    c = _mm_add_ps(k2, _mm_shuffle_ps(a, a, 0b10101010));
    k3 = perm_ps(c);
    k4 = perm_ps(_mm_add_ps(c, _mm_set_ps1(1.f)));
    o1 = fract_ps(_mm_mul_ps(k3, _mm_set_ps1(1.f / 41.f)));
    o2 = fract_ps(_mm_mul_ps(k4, _mm_set_ps1(1.f / 41.f)));
    o3 = _mm_add_ps(
        _mm_mul_ps(o2, _mm_shuffle_ps(d, d, 0b10101010)),
        _mm_mul_ps(o1, _mm_sub_ps(_mm_set_ps1(1.f), _mm_shuffle_ps(d, d, 0b10101010))));
    o4 = _mm_add_ps(
        _mm_mul_ps(_mm_shuffle_ps(o3, o3, 0b00001101), _mm_shuffle_ps(d, d, 0b00000000)),
        _mm_mul_ps(
            _mm_shuffle_ps(o3, o3, 0b00001000),
            _mm_sub_ps(_mm_set_ps1(1.f), _mm_shuffle_ps(d, d, 0b00000000))));
    Vector4 s, t;
    _mm_store_ps((float *) &s, o4);
    _mm_store_ps((float *) &t, d);
    return s.Y * t.Y + s.X * (1.0f - t.Y);
}

float Random_Noise(float x, float y, float z)
{
    return randomNoise_ps({ x, y, z });
}
#else
static Vector4 mod289(Vector4 x) { return x - Floor(x * (1.0f / 289.0f)) * 289.0f; }
static Vector4 perm(Vector4 x) { return mod289(((x * 34.0f) + 1.0f) * x); }

float Random_Noise(float x, float y, float z)
{
    Vector3 p = { x, y, z };
    Vector3 a = Floor(p);
    Vector3 d = p - a;
    d = d * d * (3.0f - 2.0f * d);

    Vector4 b = Vector4 { a.X, a.X, a.Y, a.Y } + Vector4 { 0.0f, 1.0f, 0.0f, 1.0f };
    Vector4 k1 = perm({ b.X, b.Y, b.X, b.Y });
    Vector4 k2 = perm(Vector4 { k1.X, k1.Y, k1.X, k1.Y } + Vector4 { b.Z, b.Z, b.W, b.W });

    Vector4 c = k2 + a.Z;
    Vector4 k3 = perm(c);
    Vector4 k4 = perm(c + 1.0f);

    Vector4 o1 = Fract(k3 * (1.0f / 41.0f));
    Vector4 o2 = Fract(k4 * (1.0f / 41.0f));

    Vector4 o3 = o2 * d.Z + o1 * (1.0f - d.Z);
    Vector2 o4 = Vector2 { o3.Y, o3.W } * d.X + Vector2 { o3.X, o3.Z } * (1.0f - d.X);

    return o4.Y * d.Y + o4.X * (1.0f - d.Y);
}
#endif