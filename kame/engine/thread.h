#pragma once

#include <kame/base.h>

typedef int (*FnTask)(uint32_t taskId, void *data);

struct TaskToken
{
    uint64_t  TaskId;
    uint64_t *ReferenceTaskId;
};

#define CheckCancellation(token) do { if (token && (token)->TaskId != *(token)->ReferenceTaskId) return ErrTaskCancelled; } while (0)
#define HandleCancellation(token) do { if (token && (token)->TaskId != *(token)->ReferenceTaskId) goto cancelled; } while (0)

KameAPI int CreateThreadPool(uint32_t theaadCount, const char *name, Out Handle *threadPool);

KameAPI int QueueTask(Handle threadPool, FnTask task, void *data, Out uint64_t *taskId);