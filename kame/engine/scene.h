#pragma once

#include <kame/engine/mesh.h>
#include <kame/engine/texture.h>

enum SceneNodeType : uint32_t
{
    SceneNodeTypeNone,
    SceneNodeTypeRoot,
    SceneNodeTypeBehavior,
    SceneNodeTypeCamera,
    SceneNodeTypeMesh,
    SceneNodeTypeRigidBody,
};

enum SceneNodeFlags : uint32_t
{
    SceneNodeFlagNone           = 0,
    SceneNodeFlagEnabled        = 1,
    SceneNodeFlagTransformDirty = 2,
};

struct SceneNodeBounds
{
    Vector3 Extent, Offset;
};

struct SceneNode
{
    SceneNodeType    Type;
    SceneNodeFlags   Flags;
    uint32_t         ChildCount, ChildCapacity;
    SceneNode      **Children;
    SceneNode       *Parent;
    const char      *Name;
    uint64_t         Layers;
    uint32_t         Order;

    Vector3 Rotation;
    Vector3 Position;
    Vector3 Scale;

    SceneNodeBounds Bounds;

    Matrix GlobalTransform;
};

struct RootNode;
struct BehaviorNode;

typedef BehaviorNode * (*BehaviorNodeAllocate)();
typedef int (*BehaviorNodeLoad)(SceneNode *node, RootNode *scene);
typedef int (*BehaviorNodeTick)(SceneNode *node, RootNode *scene);

struct BehaviorNode : SceneNode
{
    BehaviorNodeLoad Load;
    BehaviorNodeTick Tick;
};

struct CameraNode : SceneNode
{
    float FieldOfView;
    float Near, Far;

    uint64_t RenderLayers;

    Vector4 ClearColor;

    RenderTexture *Target;
};

struct MeshNode : SceneNode
{
    Mesh     *Mesh;
    Material *Material;
};

struct RootNode : SceneNode
{
    Platform *Platform;
};

KameAPI uint32_t Scene_SearchNodesOfType(
    SceneNode       *node,
    SceneNodeType    type,
    SceneNodeFlags   includeMask,
    SceneNodeFlags   excludeMask,
    uint64_t         layers,
    int              sort,
    uint32_t         size,
    Out SceneNode  **nodes);

KameAPI int Scene_SetNodeParent(SceneNode *node, SceneNode *parent);
KameAPI const Matrix * Scene_GetWorldTransform(SceneNode *node);
KameAPI Vector3 Scene_GetWorldPosition(SceneNode *node);
KameAPI Vector3 Scene_GetWorldScale(SceneNode *node);
KameAPI void Scene_SetDirty(SceneNode *node);
KameAPI void Scene_SetDisabled(SceneNode *node, int recurse);
KameAPI void Scene_SetEnabled(SceneNode *node, int recurse);
KameAPI void Scene_GetAxisAlignedBounds(SceneNode *node, Vector3 *min, Vector3 *max);
KameAPI void Scene_DrawBounds(Platform *p, SceneNode *node, Vector3 color);