#include <string.h>

#include <kame/engine/scene.h>
#include <kame/engine/platform.h>

static uint32_t binarySearchNodeOrder(uint32_t order, SceneNode **nodes, uint32_t count)
{
    int32_t index, minBound = 0, maxBound = (int32_t) count - 1;

    while (1)
    {
        if (minBound > maxBound)
            return (uint32_t) minBound;

        index = (maxBound + minBound) / 2;

        if (nodes[index]->Order > order)
            maxBound = index - 1;
        else if (nodes[index]->Order < order)
            minBound = index + 1;
        else
            return (uint32_t) index;
    }

    return UINT32_MAX;
}

static int searchNodesOfType(
    SceneNode       *node,
    SceneNodeType    type,
    SceneNodeFlags   includeMask,
    SceneNodeFlags   excludeMask,
    uint64_t         layers,
    int              sort,
    uint32_t         size,
    uint32_t        *count,
    SceneNode      **nodes)
{
    if ((node->Type == type) &&
        ((node->Layers & layers) != 0 || layers == 0) &&
        (node->Flags & includeMask) == includeMask &&
        (node->Flags & excludeMask) == 0)
    {
        if (*count >= size)
            return ErrOutOfResources;

        uint32_t index = binarySearchNodeOrder(node->Order, nodes, *count);
        if (index < UINT32_MAX)
        {
            for (uint32_t i = *count; i > index; --i)
                nodes[i] = nodes[i - 1];
            nodes[index] = node;
        }
        else
            nodes[*count] = node;
        (*count) += 1;
    }

    for (uint32_t i = 0; i < node->ChildCount; ++i)
        Checked(searchNodesOfType(node->Children[i], type, includeMask, excludeMask, layers, sort, size, count, nodes));

    return ErrNone;
}

uint32_t Scene_SearchNodesOfType(
    SceneNode       *node,
    SceneNodeType    type,
    SceneNodeFlags   includeMask,
    SceneNodeFlags   excludeMask,
    uint64_t         layers,
    int              sort,
    uint32_t         size,
    SceneNode      **nodes)
{
    uint32_t count = 0;
    if (size > 0)
    {
        int error = searchNodesOfType(node, type, includeMask, excludeMask, layers, sort, size, &count, nodes);
        if (error == ErrOutOfResources)
            Log(LogWarn, "scene", "search nodes discarded results beacuse the buffer full [%p]", nodes);
    }
    return count;
}

int Scene_SetNodeParent(SceneNode *node, SceneNode *parent)
{
    if (node->Parent)
    {
        for (uint32_t i = 0; i < node->Parent->ChildCount; ++i)
        {
            if (node->Parent->Children[i] == node)
            {
                for (uint32_t j = i + 1; j < node->Parent->ChildCount; ++j)
                    node->Parent[j - 1] = node->Parent[j];
                node->Parent->ChildCount -= 1;
                break;
            }
        }
    }

    if (parent)
    {
        if (!parent->Children)
        {
            parent->ChildCapacity = 1;
            parent->Children      = NewObject(SceneNode *);
        }
        else
        {
            parent->ChildCapacity = parent->ChildCount + 1;
            SceneNode **children = NewObjects(SceneNode *, parent->ChildCapacity);
            memcpy(children, parent->Children, sizeof(SceneNode *) * parent->ChildCount);
            FreeMemory(parent->Children);
            parent->Children = children;
        }

        parent->Children[parent->ChildCount] = node;
        parent->ChildCount += 1;
    }
    else
    {
        Log(LogError, "scene", "set node parent cannot be null");
        return ErrInvalidParameter;
    }

    node->Parent = parent;
    return ErrNone;
}

const Matrix * Scene_GetWorldTransform(SceneNode *node)
{
    if (node->Flags & SceneNodeFlagTransformDirty)
    {
        const Matrix *parentTransform =
            node->Type == SceneNodeTypeRoot ?
            &node->GlobalTransform :
            Scene_GetWorldTransform(node->Parent);

        ModelMatrix(node->Position, node->Scale, node->Rotation, &node->GlobalTransform);
        node->GlobalTransform *= (*parentTransform);
        node->Flags = (SceneNodeFlags) (node->Flags & ~SceneNodeFlagTransformDirty);
    }

    return &node->GlobalTransform;
}

Vector3 Scene_GetWorldPosition(SceneNode *node)
{
    const Matrix *world = Scene_GetWorldTransform(node);
    return { (*world)[3].X, (*world)[3].Y, (*world)[3].Z };
}

Vector3 Scene_GetWorldScale(SceneNode *node)
{
    Vector3 scale;
    const Matrix *world = Scene_GetWorldTransform(node);
    for (int i = 0; i < 3; ++i)
        scale[i] = Length(Vector3 { (*world)[0][i], (*world)[1][i], (*world)[2][i] });
    return scale;
}

void Scene_SetDirty(SceneNode *node)
{
    SetFlags(node->Flags, SceneNodeFlagTransformDirty);

    for (uint32_t i = 0; i < node->ChildCount; ++i)
        Scene_SetDirty(node->Children[i]);
}

void Scene_SetDisabled(SceneNode *node, int recurse)
{
    ClearFlags(node->Flags, SceneNodeFlagEnabled);

    if (recurse)
    {
        for (uint32_t i = 0; i < node->ChildCount; ++i)
            Scene_SetDisabled(node->Children[i], recurse);
    }
}

void Scene_SetEnabled(SceneNode *node, int recurse)
{
    SetFlags(node->Flags, SceneNodeFlagEnabled);

    if (recurse)
    {
        for (uint32_t i = 0; i < node->ChildCount; ++i)
            Scene_SetEnabled(node->Children[i], recurse);
    }
}

void Scene_GetAxisAlignedBounds(SceneNode *node, Vector3 *min, Vector3 *max)
{
    Vector3 position   = Scene_GetWorldPosition(node) + node->Bounds.Offset;
    Vector3 scale      = Scene_GetWorldScale(node);
    Vector3 halfExtent = node->Bounds.Extent * 0.5f * scale;
    *min = position - halfExtent;
    *max = position + halfExtent;
}

void Scene_DrawBounds(Platform *p, SceneNode *node, Vector3 color)
{
    const Matrix *world = Scene_GetWorldTransform(node);
    Vector3 halfExtent = node->Bounds.Extent * 0.5f;

    Vector3 vertices[8];
    Vector3 bounds[] = { node->Bounds.Offset - halfExtent, node->Bounds.Offset + halfExtent };

    //Log(LogTrace, "scene", "draw bounds");

    for (int k = 0; k < 2; ++k)
    for (int j = 0; j < 2; ++j)
    for (int i = 0; i < 2; ++i)
    {
        int index = k * 4 + j * 2 + i;
        Vector4 worldPosition = Transform({ bounds[i].X, bounds[j].Y, bounds[k].Z, 1.f }, world );
        vertices[index] = { worldPosition.X, worldPosition.Y, worldPosition.Z };
    }

    Vector4 drawColor = { color.X, color.Y, color.Z, 1.f };
    p->RendererDriver.DrawLine(p, vertices[0], vertices[1], drawColor);
    p->RendererDriver.DrawLine(p, vertices[1], vertices[3], drawColor);
    p->RendererDriver.DrawLine(p, vertices[3], vertices[2], drawColor);
    p->RendererDriver.DrawLine(p, vertices[2], vertices[0], drawColor);
    p->RendererDriver.DrawLine(p, vertices[4], vertices[5], drawColor);
    p->RendererDriver.DrawLine(p, vertices[5], vertices[7], drawColor);
    p->RendererDriver.DrawLine(p, vertices[7], vertices[6], drawColor);
    p->RendererDriver.DrawLine(p, vertices[6], vertices[4], drawColor);
    p->RendererDriver.DrawLine(p, vertices[0], vertices[4], drawColor);
    p->RendererDriver.DrawLine(p, vertices[6], vertices[2], drawColor);
    p->RendererDriver.DrawLine(p, vertices[1], vertices[5], drawColor);
    p->RendererDriver.DrawLine(p, vertices[7], vertices[3], drawColor);
}