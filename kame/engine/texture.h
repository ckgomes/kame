#pragma once

#include <kame/base.h>

enum TextureFlags : uint32_t
{
    TextureFlagDirty       = 1,
    TextureFlagFramebuffer = 2,
};

struct Bitmap
{
    uint32_t  Width, Height, Channels, BitsPerPixel, Pitch, Length;
    uint8_t  *Data;
};

struct Texture
{
    const char *AssetPath;
    const char *Name;

    Bitmap Bitmap;

    TextureFlags Flags;

    Handle RendererData;
};

struct RenderTexture : Texture
{
};