#pragma once

#include <kame/base.h>
#include <kame/engine/mesh.h>
#include <kame/engine/texture.h>
#include <kame/engine/scene.h>

// Window driver
typedef int (*FnWindowLoad)(Platform *p);
typedef int (*FnWindowLoadRenderer)(Platform *p);
typedef int (*FnWindowSetTitle)(Platform *p, const char *title);
typedef int (*FnWindowPresent)(Platform *p);
typedef int (*FnWindowProcessEvents)(Platform *p);

// Renderer driver
typedef int (*FnRendererLoad)(Platform *p);
typedef int (*FnRendererDraw)(Platform *p);
typedef int (*FnRendererPrepareMesh)(Platform *p, Mesh *mesh);
typedef int (*FnRendererPrepareTexture)(Platform *p, Texture *texture);
typedef int (*FnRendererPrepareMaterial)(Platform *p, Material *material);
typedef void (*FnRendererDrawLine)(Platform *p, Vector3 v1, Vector3 v2, Vector4 color);
typedef void (*FnRendererDrawText)(Platform *p, const char *text, Vector4 color);

typedef int (*FnThreadMain)(void *data);

struct PlatformWindowDriver
{
    const char *Name;
    void *Data;

    FnWindowLoad          Load;
    FnWindowLoadRenderer  LoadRenderer;
    FnWindowSetTitle      SetTitle;
    FnWindowPresent       Present;
    FnWindowProcessEvents ProcessEvents;
};

struct PlatformRendererDriver
{
    const char *Name;
    void *Data;

    FnRendererLoad            Load;
    FnRendererDraw            Draw;
    FnRendererPrepareMesh     PrepareMesh;
    FnRendererPrepareTexture  PrepareTexture;
    FnRendererPrepareMaterial PrepareMaterial;

    FnRendererDrawLine       DrawLine;
    FnRendererDrawText       DrawText;
};

struct Platform
{
    RootNode *ActiveScene;

    uint64_t TickNumber, FrameNumber;
    uint64_t StartTicks, ElapsedTicks, PreviousElapsedTicks, SimulatedTicks, TimeStepTicks;
    float TimeDelta, TimeStep;

    const char   *BehaviorLibraryPath;
    Handle        BehaviorLibrary;
    int           BehaviorLibraryLoaded;
    uint64_t      BehaviorLibraryChangedAt;
    BehaviorNode *BehaviorNode;

    PlatformWindowDriver WindowDriver;
    PlatformRendererDriver RendererDriver;

    uint8_t KeyboardState[512], PreviousKeyboardState[512];

    int Running;
};

struct MemoryRegion
{
    void *Start, *End;
};

struct MemoryMappedFile : MemoryRegion
{
    Handle Handle;
};

typedef int (*LibInit)(Platform *p);

KameAPI void Log(LogLevel level, const char *category, const char *fmt, ...);
#define LogDetail(fmt, ...) Log(LogDetail, nullptr, fmt, ## __VA_ARGS__);

KameAPI int OpenMemoryMappedFile(const char *path, Out MemoryMappedFile *file);
KameAPI int CloseMemoryMappedFile(MemoryMappedFile *file);
KameAPI int GetFileTimestamp(const char *path, Out uint64_t *timestamp);

KameAPI void *AllocateMemory(size_t size, size_t align);
KameAPI void FreeMemory(void *ptr);

KameAPI int OpenLibrary(const char *libraryName, Out Handle *library);
KameAPI int CloseLibrary(Handle library);
KameAPI int LoadLibrarySymbol(const char *symbolName, Handle library, Out Handle *symbol);

KameAPI int LoadBitmap(size_t length, const void *data, Out Bitmap *bitmap);

KameAPI uint64_t GetElapsedTicks();
KameAPI uint64_t GetTicksPerSecond();

KameAPI int Sleep(uint64_t ticks);
KameAPI int CreateThread(FnThreadMain main, void *data, const char *name, Out Handle *thread);

#define NewObject(type) ((type *)AllocateMemory(sizeof(type), alignof(type)))
#define NewObjects(type, n) ((type *)(AllocateMemory(sizeof(type) * (size_t)(n), alignof(type))))
#define NewObjectZ(type) &(*NewObject(type) = {})

template <class T>
T * ListPush(T **list, uint32_t *count, uint32_t *capacity)
{
    Assert(*count <= *capacity);

    if (!*list)
    {
        *capacity = 4;
        *list = NewObjects(T, *capacity);
    }
    else if (*count == *capacity)
    {
        *capacity = 3 * (*capacity + 1) / 2;
        T *newList = NewObjects(T, *capacity);
        for (uint32_t i = 0; i < *count; ++i)
            newList[i] = (T &&) (*list)[i];
        *list = newList;
    }

    T *value = (*list) + (*count);
    *value = {};
    *count += 1;
    return value;
}