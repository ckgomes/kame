#pragma once

#include <kame/base.h>
#include <kame/math.h>
#include <kame/engine/material.h>

enum MeshFlags : uint32_t
{
    MeshFlagDirty    = 1,
    MeshFlagReadable = 2,
};

struct MeshPart
{
    // When the mesh uses Indices, the Offset and Length refer to the Indices array
    // Otherwise, Offset and Length refer to the Vertices
    uint32_t  Offset, Length;
    uint32_t  MaterialCount;
    Material *Materials;
};

struct Mesh
{
    const char *AssetPath;
    const char *Name;

    PVector3  *Positions;
    PVector3  *Normals;
    PVector2  *TextureCoordinates;
    PVector4  *Colors;

    uint32_t   VertexCount;

    // When present, the mesh is considered to be indexed
    uint32_t   IndexCount;
    uint32_t  *Indices;

    MeshFlags  Flags;

    // If no parts are present, the mesh is considered to be one singular part
    uint32_t   PartCount;
    MeshPart  *Parts;

    Handle RendererData;
};

struct DynamicMesh : Mesh
{
    uint32_t PositionCapacity, NormalCapacity, TextureCoordinateCapacity, ColorCapacity, IndexCapacity;
};

KameAPI int Mesh_LoadFromFile(Platform *p, const char *path, Out Mesh *mesh);