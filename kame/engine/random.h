#pragma once

#include <kame/base.h>

KameAPI Handle Random_New(uint64_t seed);

KameAPI void Random_Bytes(Handle random, size_t length, Out void *bytes);
KameAPI int32_t Random_Int32(Handle random, int32_t min, int32_t max);
KameAPI float Random_Float32(Handle random, float min, float max);

KameAPI float Random_Noise(float x, float y, float z);