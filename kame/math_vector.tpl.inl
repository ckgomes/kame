#ifndef VECTOR_TYPE
#error Missing required definition: VECTOR_TYPE
#endif

#ifndef SCALAR_TYPE
#error Missing required definition: SCALAR_TYPE
#endif

#ifndef VECTOR_SIZE
#error Missing required definition: VECTOR_SIZE
#endif

#define OP +
#include "math_vector_ops.tpl.inl"

#define OP -
#include "math_vector_ops.tpl.inl"

#define OP *
#include "math_vector_ops.tpl.inl"

#define OP /
#include "math_vector_ops.tpl.inl"

inline VECTOR_TYPE operator+(VECTOR_TYPE vec)
{
    return vec;
}

inline VECTOR_TYPE operator-(VECTOR_TYPE vec)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        vec[x] = -vec[x];
    return vec;
}

inline SCALAR_TYPE Dot(VECTOR_TYPE lhs, VECTOR_TYPE rhs)
{
    SCALAR_TYPE r = 0;
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        r += lhs[x] * rhs[x];
    return r;
}

inline SCALAR_TYPE Length(VECTOR_TYPE vec)
{
    return sqrt(Dot(vec, vec));
}

inline VECTOR_TYPE Normalize(VECTOR_TYPE vec)
{
    return vec / Length(vec);
}

inline VECTOR_TYPE Floor(VECTOR_TYPE vec)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        vec[x] = floor(vec[x]);
    return vec;
}

inline VECTOR_TYPE Ceil(VECTOR_TYPE vec)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        vec[x] = ceil(vec[x]);
    return vec;
}

inline VECTOR_TYPE Round(VECTOR_TYPE vec)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        vec[x] = round(vec[x]);
    return vec;
}

inline VECTOR_TYPE Fract(VECTOR_TYPE vec)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        vec[x] = vec[x] - floor(vec[x]);
    return vec;
}

inline VECTOR_TYPE Lerp(VECTOR_TYPE a, VECTOR_TYPE b, SCALAR_TYPE t)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        a[x] = Lerp(a[x], b[x], t);
    return a;
}

inline VECTOR_TYPE Abs(VECTOR_TYPE a)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        a[x] = fabs(a[x]);
    return a;
}

inline VECTOR_TYPE Mod(VECTOR_TYPE a, SCALAR_TYPE b)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        a[x] = fmod(a[x], b);
    return a;
}

inline VECTOR_TYPE Max(VECTOR_TYPE a, VECTOR_TYPE b)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        a[x] = a[x] > b[x] ? a[x] : b[x];
    return a;
}

inline VECTOR_TYPE Min(VECTOR_TYPE a, VECTOR_TYPE b)
{
    for (size_t x = 0; x < VECTOR_SIZE; ++x)
        a[x] = a[x] < b[x] ? a[x] : b[x];
    return a;
}

#if VECTOR_SIZE == 3
inline VECTOR_TYPE Cross(VECTOR_TYPE lhs, VECTOR_TYPE rhs)
{
    return VECTOR_TYPE
    {
        lhs.Y * rhs.Z - lhs.Z * rhs.Y,
        lhs.Z * rhs.X - lhs.X * rhs.Z,
        lhs.X * rhs.Y - lhs.Y * rhs.X,
    };
}
#endif

#undef VECTOR_TYPE
#undef SCALAR_TYPE
#undef VECTOR_SIZE