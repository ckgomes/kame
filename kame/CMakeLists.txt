kame_add_source(math.cpp)

option(BUILD_EXAMPLES "Build examples" OFF)

set(EXEC_NAME "${PROJECT_NAME}_engine")

add_subdirectory(engine)
add_subdirectory(drivers)

if (BUILD_EXAMPLES)
    add_subdirectory(examples)
endif()