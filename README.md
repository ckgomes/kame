### About

An example game engine with a flexible platform layer, currently implementing X11 and Vulkan.

### Build dependencies

| Dependency | Version | Required | Description                                         |
|------------|---------|----------|-----------------------------------------------------|
| cmake      | ^3.21   | Yes      | Build generator                                     |
| ninja      | -       | No       | CMake Ninja build support                           |
| make       | -       | No       | CMake Makefile build support                        |
| vulkan-sdk | ^1.2    | No       | Graphics API - Vulkan rendering                     |
| x11        | -       | No       | Display server - Windowing system and input         |

One-liner to install the build dependencies on Arch Linux (using Ninja)  
```bash
pacman -S vulkan-devel cmake ninja
```

### Building

```bash
mkdir -p build
cd build
cmake -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_BUILD_TYPE=Debug -G Ninja ..
ninja
```

Feel free to change build settings according to your needs.

### Running

The engine should be executed from the build directory:

```bash
cd build
./kame.elf
```

The following environment variables can be used:

| Environment variable          | Description                                           |
|-------------------------------|-------------------------------------------------------|
| KAME_VULKAN_PHYSICAL_DEVICE   | Specify the name of the GPU to use with Vulkan        |
| KAME_VULKAN_ENABLE_VALIDATION | Enable the validation layer                           |
| KAME_BEHAVIOR_LIBRARY         | Path to the library containing a behavior node        |

### Loading textures

One may load textures by setting the environemnt variable `KAME_TEXTURE_PATH_{N}` to the path of the texture to load.  
By default, only texture 1 is shown, that is `KAME_TEXTURE_PATH_1`  

These files **must be** JPEG images.  

### Compiling shaders

`This step is no longer needed - shaders are compiled automatically during the build`

Vulkan requires shaders to be compiled into SPIRV bytecode before it can be loaded.  

To compile a GLSL shader into SPIRV the following command can be used:

```bash
glslc -D<STAGE> -fshader-stage=<STAGE_NAME> <FILE> -o <OUTPUT>
```

Where:

||
|-|
|STAGE is either [VERTEX_STAGE\|FRAGMENT_STAGE] depending on the shader being compiled|
|STAGE_NAME is either [vertex\|fragment] depending on the shader being compiled|
|FILE is the path to the input file being compiled|
|OUTPUT is the path to the output SPIRV|

A working example:

```bash
mkdir -p build/data/shaders
glslc -DVERTEX_STAGE -fshader-stage=vertex assets/shaders/color.glsl -o build/data/shaders/color.vertex.spirv
glslc -DFRAGMENT_STAGE -fshader-stage=fragment assets/shaders/color.glsl -o build/data/shaders/color.fragment.spirv
```