#version 430
#include "base.glsl"

layout(location = 0) pipe vec2 pTextureCoordinate;
layout(location = 1) pipe float pDepth;
layout(location = 2) pipe float pFog;
layout(location = 3) pipe vec3 pNormal;

////////////////////////////////////////////////////////////////////////////////

#ifdef VERTEX_STAGE

    layout(binding = UNIFORM_BLOCK, std140)
    uniform DrawVertexBlock
    {
        mat4 model;
    } u;

    layout(location = 0, component = 0) in vec3 pos;
    layout(location = 1, component = 0) in vec3 normal;
    layout(location = 2, component = 0) in vec2 textureCoordinate;
    layout(location = 3, component = 0) in vec3 color;

    void main()
    {
        vec4 clip = g.viewProjection * u.model * vec4(pos, 1.);
        gl_Position = clip;
        pTextureCoordinate = textureCoordinate;
        pNormal = (u.model * vec4(normal, 0.)).xyz;

        float depth = smoothstep(g.near, g.far, clip.z);
        pDepth = depth;
        pFog = smoothstep(0.5, 1.0, depth);
    }

#endif

////////////////////////////////////////////////////////////////////////////////

#ifdef FRAGMENT_STAGE

    layout (binding = UNIFORM_BLOCK, std140)
    uniform DrawFragmentBlock
    {
        vec4 textureRegion0;
        vec4 fogColor;
    } u;

    layout(binding = 8) uniform sampler2D albedoTex;

    layout(location = 0) out vec4 color;

    void main()
    {
        vec4 texel = sampleTexture(albedoTex, pTextureCoordinate, u.textureRegion0);
        color = mix(texel, u.fogColor, pFog);
    }

#endif