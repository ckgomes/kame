#version 430
#include "base.glsl"

layout(location = 0) pipe vec3 p_color;

////////////////////////////////////////////////////////////////////////////////

#ifdef VERTEX_STAGE

    layout(binding = UNIFORM_BLOCK, std140)
    uniform DrawVertexBlock
    {
        mat4 model;
    } u;

    layout(location = 0, component = 0) in vec3 pos;
    layout(location = 1, component = 0) in vec3 normal;
    layout(location = 2, component = 0) in vec2 textureCoordinate;
    layout(location = 3, component = 0) in vec3 color;

    void main()
    {
        vec4 clip = g.viewProjection * u.model * vec4(pos, 1.);
        gl_Position = clip;
        p_color = color;
    }

#endif

////////////////////////////////////////////////////////////////////////////////

#ifdef FRAGMENT_STAGE

    layout(location = 0) out vec4 color;

    void main()
    {
        color = vec4(p_color, 1.);
    }

#endif