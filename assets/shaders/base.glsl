#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension GL_ARB_enhanced_layouts : enable

#ifdef VERTEX_STAGE
#define pipe out
#define UNIFORM_BLOCK 1

    layout(binding = 0, std140) uniform RenderPassVertexBlock
    {
        mat4 viewProjection;
        float near, far;
    } g;

#endif

#ifdef FRAGMENT_STAGE
#define pipe in
#define UNIFORM_BLOCK 3

    vec4 sampleTexture(sampler2D tex, vec2 uv, vec4 region)
    {
        uv = region.xy * uv + region.zw;
        return texture(tex, uv);
    }

#endif