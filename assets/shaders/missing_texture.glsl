#version 430
#include "base.glsl"

#define GRID_CELLS 3.

layout(location = 0) pipe vec2 pTextureCoordinate;

////////////////////////////////////////////////////////////////////////////////

#ifdef VERTEX_STAGE

    layout(binding = UNIFORM_BLOCK, std140)
    uniform DrawVertexBlock
    {
        mat4 model;
    } u;

    layout(location = 0, component = 0) in vec3 pos;
    layout(location = 1, component = 0) in vec3 normal;
    layout(location = 2, component = 0) in vec2 textureCoordinate;
    layout(location = 3, component = 0) in vec3 color;

    void main()
    {
        vec4 clip = g.viewProjection * u.model * vec4(pos, 1.);
        pTextureCoordinate = textureCoordinate;
        gl_Position = clip;
    }

#endif

////////////////////////////////////////////////////////////////////////////////

#ifdef FRAGMENT_STAGE

    layout(location = 0) out vec4 color;

    void main()
    {
        vec2 f = step(.5, fract(pTextureCoordinate * GRID_CELLS));
        color = mix(
            vec4(1., 0., 1., 1.),
            vec4(0., 1., 1., 1.),
            step(.5, abs(f.x - f.y)));
    }

#endif